/*
 * gf8_log.cpp
 *
 * Routines for 8-bit Galois fields using log tables.
 *
 */

#include "gf8_gather.h"
#include <stdio.h>
#include <string.h>
#include <immintrin.h>
#include <nmmintrin.h> //for _mm_blendv_epi8


/*
 * Basic Galois field routines
 */

gf_8_t 
gf8_gather_state::pow (gf_8_t a, uint32_t b) const {

    if (a == 0) {
        return (a);
    } else if (b == 0) {
        return (gf_8_t)1;
    } else {
        return (antilog ((gf_8_t)(((uint64_t)log(a) * ((uint64_t)b & 0xfffffULL))
                                  % (uint64_t)(GF8_FIELD_SIZE-1))));
    }
}

gf_8_t
gf8_gather_state::inv (gf_8_t a) const {

    if (a == 0) {
        return (0);
    } else if (a == 1) {
        return (1);
    } else {
        return (antilog_tbl[GF8_FIELD_SIZE - 1 - log_tbl[a]]);
    }
}

void pvec1(__m256i l, char* s){
  printf("%s: AVX Table: [LOW] <--->  [HIGH]\n",s);
  uint16_t *val1 = (uint16_t*) &l;
  printf("%04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x\n",
         val1[0], val1[1], val1[2], val1[3], val1[4], val1[5], val1[6], val1[7], 
         val1[8], val1[9],val1[10], val1[11], val1[12], val1[13], val1[14], val1[15]);
}

void
gf8_gather_state::mul_region (const gf_8_t * src, gf_8_t * dst, uint64_t bytes,
                           gf_8_t multiplier, bool accum) const {

    if (multiplier == (gf_8_t)0) {
        if (! accum) {
            memset (dst, 0, bytes);
        }
        return;
    }
    else if (multiplier == (gf_8_t)1) {
        if (! accum){
            memcpy (dst, src, bytes);
        }   
        else {
            uint64_t    chunks = bytes >> 5ULL;
            for (uint64_t i = 0; i < chunks; i++) {
                __m256i x = _mm256_loadu_si256 (((__m256i *)src) + i); 
                __m256i y = _mm256_loadu_si256 (((__m256i *)dst) + i); 
                _mm256_storeu_si256 (((__m256i *)dst) + i, _mm256_xor_si256(x,y));
            }   
        }   
        return;
    } 

  /*    
    uint64_t    chunks = bytes >> 4ULL;
    __m256i log_m = _mm256_set1_epi16(log(multiplier));
    __m256i res;
    __m256i amask = _mm256_set_epi8(0xff,0x0f,0xff,0x0e,0xff,0x0d,0xff,0x0c,
                                    0xff,0x0b,0xff,0x0a,0xff,0x09,0xff,0x08,
                                    0xff,0x07,0xff,0x06,0xff,0x05,0xff,0x04,
                                    0xff,0x03,0xff,0x02,0xff,0x01,0xff,0x00);
    __m128i a,b;
    __m128i zero = _mm_setzero_si128();
    //256 bit vector with constant value seperated every 32 bits

    if (!accum){
      for (uint64_t i = 0; i < chunks; ++i) {
        a = _mm_loadu_si128 (((__m128i *)src) + i); 
        uint8_t srr[16];
        uint8_t arr[16];
        uint8_t ray[16];
      
        memcpy(srr, &src[16*i],16);
        for (uint8_t j = 0; j < 16; ++j){
          arr[j] = log_tbl[srr[j]];
        }
        __m128i t = _mm_loadu_si128((__m128i *)arr);
        __m256i x = _mm256_inserti128_si256(_mm256_castsi128_si256(t),t,1);
        x = _mm256_shuffle_epi8(x,amask);
        res = _mm256_add_epi16(log_m, x);

        ray[0] = antilog_tbl[_mm256_extract_epi16(res,0)];
        ray[1] = antilog_tbl[_mm256_extract_epi16(res,1)];
        ray[2] = antilog_tbl[_mm256_extract_epi16(res,2)];
        ray[3] = antilog_tbl[_mm256_extract_epi16(res,3)];
        ray[4] = antilog_tbl[_mm256_extract_epi16(res,4)];
        ray[5] = antilog_tbl[_mm256_extract_epi16(res,5)];
        ray[6] = antilog_tbl[_mm256_extract_epi16(res,6)];
        ray[7] = antilog_tbl[_mm256_extract_epi16(res,7)];
        ray[8] = antilog_tbl[_mm256_extract_epi16(res,8)];
        ray[9] = antilog_tbl[_mm256_extract_epi16(res,9)];
        ray[10] = antilog_tbl[_mm256_extract_epi16(res,10)];
        ray[11] = antilog_tbl[_mm256_extract_epi16(res,11)];
        ray[12] = antilog_tbl[_mm256_extract_epi16(res,12)];
        ray[13] = antilog_tbl[_mm256_extract_epi16(res,13)];
        ray[14] = antilog_tbl[_mm256_extract_epi16(res,14)];
        ray[15] = antilog_tbl[_mm256_extract_epi16(res,15)];

        b = _mm_loadu_si128((__m128i *)ray);
        a = _mm_cmpeq_epi8(a,zero);
        b = _mm_andnot_si128(a,b);
        _mm_storeu_si128 (((__m128i *)dst)+i,b);
      }
    } else {
      for (uint64_t i = 0; i < chunks; ++i) {
        a = _mm_loadu_si128 (((__m128i *)src) + i); 
        uint8_t srr[16];
        uint8_t arr[16];
        uint8_t ray[16];
      
        memcpy(srr, &src[16*i],16);
        for (uint8_t j = 0; j < 16; ++j){
          arr[j] = log_tbl[srr[j]];
        }
        __m128i t = _mm_loadu_si128((__m128i *)arr);
        __m256i x = _mm256_inserti128_si256(_mm256_castsi128_si256(t),t,1);
        x = _mm256_shuffle_epi8(x,amask);
        res = _mm256_add_epi16(log_m, x);

        ray[0] = antilog_tbl[_mm256_extract_epi16(res,0)];
        ray[1] = antilog_tbl[_mm256_extract_epi16(res,1)];
        ray[2] = antilog_tbl[_mm256_extract_epi16(res,2)];
        ray[3] = antilog_tbl[_mm256_extract_epi16(res,3)];
        ray[4] = antilog_tbl[_mm256_extract_epi16(res,4)];
        ray[5] = antilog_tbl[_mm256_extract_epi16(res,5)];
        ray[6] = antilog_tbl[_mm256_extract_epi16(res,6)];
        ray[7] = antilog_tbl[_mm256_extract_epi16(res,7)];
        ray[8] = antilog_tbl[_mm256_extract_epi16(res,8)];
        ray[9] = antilog_tbl[_mm256_extract_epi16(res,9)];
        ray[10] = antilog_tbl[_mm256_extract_epi16(res,10)];
        ray[11] = antilog_tbl[_mm256_extract_epi16(res,11)];
        ray[12] = antilog_tbl[_mm256_extract_epi16(res,12)];
        ray[13] = antilog_tbl[_mm256_extract_epi16(res,13)];
        ray[14] = antilog_tbl[_mm256_extract_epi16(res,14)];
        ray[15] = antilog_tbl[_mm256_extract_epi16(res,15)];

        b = _mm_loadu_si128((__m128i *)ray);
        a = _mm_cmpeq_epi8(a,zero);
        b = _mm_andnot_si128(a,b);
        a = _mm_loadu_si128(((__m128i *)dst)+i);
        _mm_storeu_si128 (((__m128i *)dst)+i,_mm_xor_si128(a,b));
      }
    }
    return;
    */
 

    __m128i m1,m2,m3;
    __m128i zero = _mm_setzero_si128();
    //high to low set.
    __m128i mmask = _mm_set_epi64x(0,0xffffffffffffffff);

    __m256i v;
    // our control mask for pshufb to unpack values 32 bits apart
    __m256i cmask = _mm256_set_epi32(0xffffff07,0xffffff06,0xffffff05,0xffffff04,0xffffff03,0xffffff02,0xffffff01,0xffffff00);
    // our anti-control mask to pack values from 32 bits apart to 8 bits apart
    //changed 0xff to -1 because char undefined as signed, 255 unsigned
    __m256i amask = _mm256_set_epi8(-1,-1,-1,-1,-1,-1,-1,-1,
                                    12,8,4,0,-1,-1,-1,-1,
                                    -1,-1,-1,-1,-1,-1,-1,-1,
                                    -1,-1,-1,-1,12,8,4,0);
    //256 bit vector with constant value seperated every 32 bits
    __m256i log_m = _mm256_set1_epi32(log(multiplier));

    uint64_t    i;
    // we will load/operate over 128 bits at a time
    uint64_t    chunks = bytes >> 4ULL;


    if (accum) {
      for (i = 0; i < chunks; i++) {

        //load values from region into vector
        m3 = _mm_loadu_si128 (((__m128i *)src) + i); // load 128 bits into a vector
        m2 = _mm_and_si128(m3,mmask); //mask off high 64 bits to give us the low 64 bits

        // cant find _mm256_set_m128i(), which is defined as it is below
        // convert to 256 bit vector with v = [m2,m2], single pshufb will operate over both at once
        v = _mm256_insertf128_si256(_mm256_castsi128_si256(m2),m2,0x1);

        //convert / expand from 8 bits to 32 bits
        v = _mm256_shuffle_epi8(v,cmask);
        
        //gather // lookup into log table
        v = _mm256_i32gather_epi32((int32_t*)log_tbl,v,4);

        //add our multiplier to our looked up value
        v = _mm256_add_epi32(log_m,v);

        //gather // lookup into alog table 
        v = _mm256_i32gather_epi32((int32_t*)antilog_tbl,v,4);
        //again mask off 24 upper bits

        //convert back
        v = _mm256_shuffle_epi8(v,amask);
        
        //store our first 64 bit result back into m2
        //v has the first 32 bits of the first 128 bit lane set and the second 32 bits of the second lane set
        //when we xor here we will get a 128 bit vector with the first 64 bits set.
        m2 = _mm_xor_si128(_mm256_castsi256_si128(v),_mm256_extractf128_si256(v,1));

        //// Step 2: Do the next 64 bits we loaded  - repeat the exact same process //

        // shift our loaded src over 64 to now grab only that 64 bits
        m1 = _mm_srli_si128(m3,8);
        v = _mm256_insertf128_si256(_mm256_castsi128_si256(m1),m1,0x1); //convert to 256 bit vector

        //convert / expand from 8 bits to 32 bits
        v = _mm256_shuffle_epi8(v,cmask);
        
        //gather // lookup into log table
        v = _mm256_i32gather_epi32((int32_t*)log_tbl,v,4);

        //add
        v = _mm256_add_epi32(log_m,v);

        //gather // lookup into alog table
        v = _mm256_i32gather_epi32((int32_t*)antilog_tbl,v,4);

        //convert back
        v = _mm256_shuffle_epi8(v,amask);
 
        //store our second 64 bits back into m1
        m1 = _mm_xor_si128(_mm256_castsi256_si128(v),_mm256_extractf128_si256(v,1));
        //shift it right 64 bits to not overlap with m2
        m1 = _mm_slli_si128(m1,8); 
        //add m1 and m2 together
        m1 = _mm_xor_si128(m2,m1);
      
        //check if in the original loaded value, there were any 0's
        //if so we need to set them back to 0 as the alog table's
        //index for 0 is not 0.
        m3 = _mm_cmpeq_epi8(m3,zero);
        m1 = _mm_andnot_si128(m3,m1);

        //load in the dsts current value and add to the current result
        _mm_storeu_si128 (((__m128i *)dst) + i,
            _mm_xor_si128(m1,
              _mm_loadu_si128 (((__m128i *)dst) + i)));
      }
    } else {
      for (i = 0; i < chunks; i++) {
        //load values from region into vector
        m3 = _mm_loadu_si128 (((__m128i *)src) + i); // load 128 bits into a vector
        m2 = _mm_and_si128(m3,mmask); //mask off high 64 bits to give us the low 64 bits

        // cant find _mm256_set_m128i(), which is defined as it is below
        // convert to 256 bit vector with v = [m2,m2], single pshufb will operate over both at once
        v = _mm256_insertf128_si256(_mm256_castsi128_si256(m2),m2,0x1);

        //convert / expand from 8 bits to 32 bits
        v = _mm256_shuffle_epi8(v,cmask);
        
        //gather // lookup into log table
        v = _mm256_i32gather_epi32((int32_t*)log_tbl,v,4);

        //add our multiplier to our looked up value
        v = _mm256_add_epi32(log_m,v);

        //gather // lookup into alog table 
        v = _mm256_i32gather_epi32((int32_t*)antilog_tbl,v,4);
        //again mask off 24 upper bits

        //convert back
        v = _mm256_shuffle_epi8(v,amask);
        
        //store our first 64 bit result back into m2
        //v has the first 32 bits of the first 128 bit lane set and the second 32 bits of the second lane set
        //when we xor here we will get a 128 bit vector with the first 64 bits set.
        m2 = _mm_xor_si128(_mm256_castsi256_si128(v),_mm256_extractf128_si256(v,1));

        //// Step 2: Do the next 64 bits we loaded  - repeat the exact same process //

        // shift our loaded src over 64 to now grab only that 64 bits
        m1 = _mm_srli_si128(m3,8);
        v = _mm256_insertf128_si256(_mm256_castsi128_si256(m1),m1,0x1); //convert to 256 bit vector

        //convert / expand from 8 bits to 32 bits
        v = _mm256_shuffle_epi8(v,cmask);
        
        //gather // lookup into log table
        v = _mm256_i32gather_epi32((int32_t*)log_tbl,v,4);

        //add
        v = _mm256_add_epi32(log_m,v);

        //gather // lookup into alog table
        v = _mm256_i32gather_epi32((int32_t*)antilog_tbl,v,4);

        //convert back
        v = _mm256_shuffle_epi8(v,amask);
 
        //store our second 64 bits back into m1
        m1 = _mm_xor_si128(_mm256_castsi256_si128(v),_mm256_extractf128_si256(v,1));
        //shift it right 64 bits to not overlap with m2
        m1 = _mm_slli_si128(m1,8); 
        //add m1 and m2 together
        m1 = _mm_xor_si128(m2,m1);
      
        //check if in the original loaded value, there were any 0's
        //if so we need to set them back to 0 as the alog table's
        //index for 0 is not 0.
        m3 = _mm_cmpeq_epi8(m3,zero);
        m1 = _mm_andnot_si128(m3,m1);
      
        //store value back out
        _mm_storeu_si128 (((__m128i *)dst) + i, m1);
      }
    }
    for (i = chunks << 4ULL; i < bytes; ++i) {
      dst[i] = mul (src[i], multiplier) ^ (accum ? dst[i] : (gf_8_t)0);
    }
}

void
gf8_gather_state::mul_region_region (const gf_8_t * src1, const gf_8_t * src2,
                                  gf_8_t * dst, uint64_t bytes, bool accum) const
{

    if (accum) {
        for (unsigned i = 0; i < bytes / sizeof (gf_8_t); ++i) {
            dst[i] ^= mul (src1[i], src2[i]);
        }
    } else {
        for (unsigned i = 0; i < bytes / sizeof (gf_8_t); ++i) {
            dst[i] = mul (src1[i], src2[i]);
        }
    }
}

gf_8_t
gf8_gather_state::dotproduct (const gf_8_t * src1, const gf_8_t * src2,
                           uint64_t n_elements, uint64_t stride) const
{
    gf_8_t             dp = (gf_8_t)0;
    gf_8_t const *     s1 = src1;
    gf_8_t const *     s2 = src2;

    for (uint64_t i = 0; i < n_elements; ++i) {
        dp = add (dp, mul (*s1, *s2));
        s1 += stride;
        s2 += stride;
    }
    return dp;
}

//
// Since we're not using this for much, we want to make it more memory-efficient rather than
// speed-efficient.  We don't precompute the inverse table; we can just look up inverses
// using the log / antilog tables instead.
//
gf8_gather_state::gf8_gather_state (gf_8_t prim_poly, gf_8_t alpha){

    uint32_t    b, i;

    this->alpha = alpha;
    this->prim_poly = 0x100 | (uint32_t)prim_poly;

    for (b = this->alpha, i = 0; i < GF8_FIELD_SIZE - 1; i++) {
        log_tbl[b] = (gf_32_t)i;
        antilog_tbl[i] = (gf_32_t)b;
        antilog_tbl[i + GF8_FIELD_SIZE - 1] = (gf_32_t)b;
        b <<= 1;
        if (b & GF8_FIELD_SIZE) {
            b ^= this->prim_poly;
        }
    }
}

