// List of fields supported by gf_erasure
//

template struct gf_erasure<gf_8_t, gf8_shiftadd_state>;
template struct gf_erasure<gf_16_t, gf16_shiftadd_state>;
template struct gf_erasure<gf_32_t, gf32_shiftadd_state>;
template struct gf_erasure<gf_64_t, gf64_shiftadd_state>;

#ifdef  GF_LOG
template struct gf_erasure<gf_8_t, gf8_log_state>;
template struct gf_erasure<gf_16_t, gf16_log_state>;
#endif  // GF_LOG

#ifdef  GF_SHIFT2
template struct gf_erasure<gf_8_t, gf8_shift2_state>;
template struct gf_erasure<gf_16_t, gf16_shift2_state>;
#endif  // GF_SHIFT2

#ifdef  GF_SSSE3
template struct gf_erasure<gf_8_t, gf8_ssse3_state>;
template struct gf_erasure<gf_16_t, gf16_ssse3_state>;
#endif  // GF_SSSE3

#ifdef  GF_AVX2
template struct gf_erasure<gf_8_t, gf8_avx2_state>;
template struct gf_erasure<gf_16_t, gf16_avx2_state>;
template struct gf_erasure<gf_8_t, gf8_gather_state>;
template struct gf_erasure<gf_16_t, gf16_gather_state>;
#endif  // GF_AVX2

#ifdef  GF_CLMUL
template struct gf_erasure<gf_8_t, gf8_clmul_state>;
template struct gf_erasure<gf_16_t, gf16_clmul_state>;
template struct gf_erasure<gf_32_t, gf32_clmul_state>;
template struct gf_erasure<gf_64_t, gf64_clmul_state>;
#endif  // GF_CLMUL

#if defined(GF_NEON32) || defined(GF_NEON64)
template struct gf_erasure<gf_8_t, gf8_neon_state>;
template struct gf_erasure<gf_16_t, gf16_neon_state>;
template struct gf_erasure<gf_8_t, gf8_vmull_state>;
template struct gf_erasure<gf_16_t, gf16_vmull_state>;
template struct gf_erasure<gf_32_t, gf32_vmull_state>;
template struct gf_erasure<gf_64_t, gf64_vmull_state>;
#endif  // GF_NEON
