//only compile file once
#pragma     once

#include "gf8_ssse3.h"
#include "gf16_ssse3.h"

#include "gf8_clmul.h"
#include "gf16_clmul.h"
#include "gf32_clmul.h"
#include "gf64_clmul.h"

#include "gf8_avx2.h"
#include "gf8_gather.h"
#include "gf16_avx2.h"
#include "gf16_gather.h"

#include "gf8_neon.h"
#include "gf16_neon.h"
#include "gf8_vmull.h"
#include "gf16_vmull.h"
#include "gf32_vmull.h"
#include "gf64_vmull.h"

#include "gfw_shiftadd.h"
#include "gfw_shift2.h"
#include "gfw_log.h"

#include "gf_random.h"


// This is the generic template for handling GF operations
// depending on the gf_state passed in, operations will be
// used based on the struct (8,16, or sse based currently)
template <class gf_w, class gf_state>
struct
gf_w_state {

    gf_w_state (gf_w prim_poly, gf_w alpha = (gf_w)1)
        : state(prim_poly, alpha){}

    // + and - are XOR operations
    gf_w add (gf_w a, gf_w b) const {return ((gf_w)(a ^ b));}
    gf_w sub (gf_w a, gf_w b) const {return ((gf_w)(a ^ b));}

    // * and / depending on the gf_state
    gf_w mul (gf_w a, gf_w b) const {return state.mul (a, b);}
    gf_w div (gf_w a, gf_w b) const {return state.div (a, b);}
    gf_w inv (gf_w a) const {return state.inv (a);}
    gf_w pow (gf_w a, uint32_t b) const {return state.pow (a, b);}
    gf_w log (gf_w a) const {return state.log (a);}
    gf_w antilog (gf_w a) const {return state.antilog (a);}

    // mul_region: multiply region src by multiplier, and place the result in dst (XOR optional)
    void mul_region(const gf_w * src, gf_w * dst, uint64_t bytes, gf_w multiplier,
                    bool accum = false) const
    {
        state.mul_region (src, dst, bytes, multiplier, accum);
    }

    // mul_region_region: multiply each element in src1 by the corresponding element in src2,
    // and place the result in dst (XOR optional)
    void mul_region_region (const gf_w * src1, const gf_w * src2, gf_w * dst,
                            uint64_t bytes, bool accum = false) const
    {
        state.mul_region_region (src1, src2, dst, bytes, accum);
    }

    // Compute the dot product of the two input buffers.  The stride parameter determines the
    // distance between successive used elements.  Strides of 1 and 2 are fastest; other
    // strides might take longer to load from memory.
    gf_w dotproduct (const gf_w * src1, const gf_w * src2,
                     uint64_t n_elements, uint64_t stride = 1) const
    {
        return state.dotproduct (src1, src2, n_elements, stride);
    }

    uint64_t field_max() const {return state.field_max();}

    static const unsigned       TEST_INV = 0x1;
    static const unsigned       TEST_MUL_BY_1 = 0x2;
    static const unsigned       TEST_MUL_DIV = 0x4;
    static const unsigned       TEST_MUL_REGION_CONST = 0x8;
    static const unsigned       TEST_MUL_REGION_REGION = 0x10;
    static const unsigned       TEST_DOT_PRODUCT = 0x20;
    static const unsigned       TEST_POW = 0x40;
    unsigned    unit_test (const char * which_tests, uint64_t test_size, uint64_t bufsize);

    gf_state state;
};
