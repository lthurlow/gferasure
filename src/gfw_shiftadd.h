//
// gfw_shiftadd.h
// w-bit Galois field using basic shift-add (unoptimized).
//

#pragma     once

#include "gf_types.h"
#include "gf_random.h"

template <class gf_w_t>
struct
alignas(16)
gfw_shiftadd_state {

    // We can't provide predefined defaults for prim_poly or alpha because we
    // don't know the field size.  There's nothing else to initialize because
    // we need to do brute-force on just about everything.
    gfw_shiftadd_state (gf_w_t prim_poly_, gf_w_t alpha_ = (gf_w_t)1)
        : prim_poly(prim_poly_)
        , alpha (alpha_)
        {}

    // Basic arithmetic is all we need to support
    gf_w_t add (gf_w_t a, gf_w_t b) const {return ((gf_w_t)(a ^ b));}
    gf_w_t sub (gf_w_t a, gf_w_t b) const {return ((gf_w_t)(a ^ b));}
    gf_w_t inv (gf_w_t a) const;
    gf_w_t pow (gf_w_t a, uint64_t b) const;
    gf_w_t mul (gf_w_t a, gf_w_t b) const;
    gf_w_t div (gf_w_t a, gf_w_t b) const {return (mul (a, inv(b)));}

    void        mul_region (const gf_w_t * src, gf_w_t * dst, uint64_t bytes, gf_w_t m,
                            bool accum = false) const;
    void        mul_region_region (const gf_w_t * src1, const gf_w_t * src2, gf_w_t * dst,
                                   uint64_t bytes, bool accum = false) const;
    gf_w_t      dotproduct (const gf_w_t * src1, const gf_w_t * src2,
                            uint64_t n_elements, uint64_t stride = 1) const;

    uint64_t    bitwidth () const       {return (sizeof (gf_w_t) * 8ULL);}
    // Calculate this by getting bitwidth-1 set bits, and ORing with the leftmost bit set
    uint64_t    field_max () const      {return (((1ULL << (bitwidth() - 1)) - 1) |
                                                 (1ULL << (bitwidth() - 1)));}

 private:
    gf_w_t      prim_poly;
    gf_w_t      alpha;
    gf_w_t      mul_by_2 (gf_w_t a) const
    {
        bool reduce = !! (a & (gf_w_t)(1ULL << (bitwidth() - 1)));
        return ((a << (gf_w_t)1) ^ (reduce ? prim_poly : (gf_w_t)0));
    }
};

typedef gfw_shiftadd_state<gf_8_t>      gf8_shiftadd_state;
typedef gfw_shiftadd_state<gf_16_t>     gf16_shiftadd_state;
typedef gfw_shiftadd_state<gf_32_t>     gf32_shiftadd_state;
typedef gfw_shiftadd_state<gf_64_t>     gf64_shiftadd_state;

extern gf8_shiftadd_state               gf8_shiftadd_field;
extern gf16_shiftadd_state              gf16_shiftadd_field;
extern gf32_shiftadd_state              gf32_shiftadd_field;
extern gf64_shiftadd_state              gf64_shiftadd_field;
