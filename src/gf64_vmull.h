//
// gf64_vmull.h
//
// 64-bit Galois field using SSSE3.
//

#pragma     once

#include "gf_types.h"
#include "gfw_shiftadd.h"

#include "gf_random.h"

struct
alignas(16)
gf64_vmull_state {

    gf64_vmull_state (gf_64_t prim_poly_ = gf64_default_primpoly, gf_64_t alpha_ = 1)
        : prim_poly(prim_poly_)
        , shiftadd_state(prim_poly_, alpha_)
    {
    }


    gf_64_t  add (gf_64_t a, gf_64_t b)    const {return ((gf_64_t)(a ^ b));}
    gf_64_t  sub (gf_64_t a, gf_64_t b)    const {return ((gf_64_t)(a ^ b));}
    gf_64_t  inv (gf_64_t a)               const {return (shiftadd_state.inv (a));}
    gf_64_t  pow (gf_64_t a, gf_64_t b)    const {return (shiftadd_state.pow (a, b));}
    gf_64_t  mul (gf_64_t a, gf_64_t b)    const;
    gf_64_t  div (gf_64_t a, gf_64_t b)    const {return (mul (a,inv(b)));}

    void        mul_region (const gf_64_t * src, gf_64_t * dst,
                            uint64_t bytes, gf_64_t m, bool accum = false) const;

    void        mul_region_region (const gf_64_t * src1, const gf_64_t * src2, gf_64_t * dst,
                                 uint64_t bytes, bool accum = false) const;

    gf_64_t      dotproduct (const gf_64_t * src1, const gf_64_t * src2,
                            uint64_t n_elements, uint64_t stride = 1) const;

    bool        rs_encode (gf_64_t ** const data, unsigned n_data,
                           gf_64_t ** parity, unsigned n_parity, uint64_t length) const;

    uint64_t    field_max () const      {return GF64_FIELD_MAX;}

 private:
    uint16_t            prim_poly;
    gf64_shiftadd_state shiftadd_state;
};
