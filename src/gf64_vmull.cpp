/*
 * gf64_vmull.cpp
 *
 * Routines for 64-bit Galois fields using NEON.
 *
 */

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <arm_neon.h>

#include "gf64_vmull.h"

typedef poly16x8_t __v16p;
typedef poly8x8_t  __v8x8p;
typedef uint8x8_t  __v8x8u;
typedef uint8x16_t __v8u;
typedef uint16x8_t __v16u;
typedef uint64x2_t __v64u;

/*Camara et al.'s implementation: 
Fast Software Polynomial Multiplication on ARM processors Using the NEON Engine
*/
#ifdef GF_NEON32
static
inline
__v16u
__attribute__ ((optimize("O0")))
mul128(uint64x1_t a, uint64x1_t b){
  // q0 = t0q, d0 = t0l, d1 = t0h
  // q1 = t1q, d2 = t1l, d3 = t1h
  // q2 = t2q, d4 = t2l, d5 = t2h
  // q3 = t3q, d6 = t3l, d6 = t3h
  // q4 = rq,  d8 = rl,  d9 = rh
  // d10 = a
  // d11 = b
  // d12 = k16
  // d13 = k32
  // d14 = k48
  //register uint16x8_t t0q asm("q0");
  //register uint16x8_t t1q asm("q1");
  //register uint16x8_t t2q asm("q2");
  //register uint16x8_t t3q asm("q3");
  register uint16x8_t rq  asm("q4");
  register uint64x1_t ad  asm("d10") = a;
  register uint64x1_t bd  asm("d11") = b;
  register uint64x1_t k16 asm("d12") = vcreate_u64(0xffff);;
  register uint64x1_t k32 asm("d13") = vcreate_u64(0xffffffff);
  register uint64x1_t k48 asm("d14") = vcreate_u64(0xffffffffffff);
  asm (
    "vext.8 d0, d10, d10, $1\n\t"     //1
    "vmull.p8 q0, d0, d11\n\t"        //2
    "vext.8 d8, d11, d11, $1\n\t"     //3
    "vmull.p8 q4, d10, d8\n\t"        //4
    "vext.8 d2, d10, d10, $2\n\t"     //5
    "vmull.p8 q1, d2, d11\n\t"        //6
    "vext.8 d6, d11, d11, $2\n\t"     //7
    "vmull.p8 q3, d10, d6\n\t"        //8
    "vext.8 d4, d10, d10, $3\n\t"     //9
    "vmull.p8 q2, d4, d11\n\t"        //10
    "veorq q0, q0, q4\n\t"            //11
    "vext.8 d8, d11, d11, $3\n\t"     //12
    "vmull.p8 q4, d10, d8\n\t"        //13
    "veorq q1, q1, q3\n\t"            //14
    "vext.8 d6, d11, d11, $4\n\t"     //15
    "vmull.p8 q3, d10, d6\n\t"        //16
    "veor d0, d0, d1\n\t"             //17
    "vand d1, d1, d14\n\t"            //18
    "veor d2, d2, d3\n\t"             //19
    "vand d3, d3, d13\n\t"            //20
    "veorq q2, q2, q4\n\t"            //21
    "veor d0, d0, d1\n\t"             //22
    "veor d2, d2, d3\n\t"             //23
    "veor d4, d4, d5\n\t"             //24
    "vand d5, d5, d12\n\t"            //25
    "veor d6, d6, d7\n\t"             //26
    "vmov.i64 d7, $0\n\t"             //27
    "vext.8 q0, q0, q0, $15\n\t"      //28
    "veor d4, d4, d5\n\t"             //29
    "vext.8 q1, q1, q1, $14\n\t"      //30
    "vmull.p8 q4, d10, d11\n\t"       //31
    "vext.8 q2, q2, q2, $13\n\t"      //32
    "vext.8 q3, q3, q3, $12\n\t"      //33
    "veorq q0, q0, q1\n\t"            //34
    "veorq q2, q2, q3\n\t"            //35
    "veorq q4, q4, q0\n\t"            //36
    "veorq q4, q4, q2"                //37
    : /*output*/
    : "w" (k16), "w" (k32), "w" (k48), "w" (ad), "w" (bd)/*inputs*/
    : /*clobber*/
  );
  return rq;
}
#elif GF_NEON64
static
inline
__v16u
mul128(uint64x1_t a, uint64x1_t b){
  return vmull.p64(a,b);
}
#endif 

gf_64_t
gf64_vmull_state::mul(gf_64_t a64, gf_64_t b64) const {
  uint64x1_t a  = vcreate_u64(a64);
  uint64x1_t b  = vcreate_u64(b64);
  uint64x1_t pp = vcreate_u64((uint32_t)(prim_poly & 0xffffffffULL));
  uint64x1_t v;
  uint16x8_t w, result;

  result = mul128(a, b);
  v = vreinterpret_u64_u32(vset_lane_u32(0,vreinterpret_u32_u16(vget_high_u16(result)),0));
  w = mul128(pp,v);

  result = veorq_u16(result, w);
  v = vreinterpret_u64_u32(vset_lane_u32(0,vreinterpret_u32_u16(vget_high_u16(result)),1));
  w = mul128(pp,v);

  result = veorq_u16(result, w);
  return (gf_64_t)vget_low_u16(result);
}

void
gf64_vmull_state::mul_region (const gf_64_t * src, gf_64_t * dst,
                     uint64_t bytes, gf_64_t m, bool accum) const
{
  if (m == (gf_64_t)0) {
    if (! accum) {
      memset (dst, 0, bytes);
    }
    return;
  }
  else if (m == (gf_64_t)1) {
    if (! accum){
      memcpy (dst, src, bytes);
    }   
    else {
      uint64_t    chunks = bytes >> 3ULL;
      for (uint64_t i = 0; i < chunks; i+=2) {
        __v64u x = vld1q_u64(src+i);
        __v64u y = vld1q_u64(dst+i);
        vst1q_u64(dst + i, veorq_u64(x,y));
      }
    }
    return;
  }

  uint64_t    i;
  uint64_t    chunks = bytes >> 3ULL;
  uint64x1_t  mult = vcreate_u64(m);

  uint64x1_t x,y;
  if (accum) {
    for (i = 0; i < chunks; i++) {
      x = vld1_u64(src+i);
      y = vld1_u64(dst+i);
      vst1_u64(dst+i, veor_u64(mul(x,mult),y));
    }
  } else {
    for (i = 0; i < chunks; i++){
      x = vld1_u64(src+i);
      vst1_u64(dst + i, mul(x,mult));
    }
  }
}

void
gf64_vmull_state::mul_region_region (const gf_64_t * src1, 
                 const gf_64_t * src2, gf_64_t * dst,
                 uint64_t bytes, bool accum) const
{

  uint64_t    i;
  uint64_t    chunks = bytes >> 3ULL;

  if (accum) {
    uint64x1_t x,y,z;
    for (i = 0; i < chunks; i++) {
      x = vld1_u64(src1+i);
      y = vld1_u64(src2+i);
      z = vld1_u64(dst +i);
      vst1_u64(dst + i, veor_u64(mul(x,y),z));
    }
  } else {
    uint64x1_t x,y;
    for (i = 0; i < chunks; i++) {
      x = vld1_u64(src1+i);
      y = vld1_u64(src2+i);
      vst1_u64(dst + i, mul(x,y));
    }
  }
}

gf_64_t
gf64_vmull_state::dotproduct (const gf_64_t * src1, const gf_64_t * src2,
            uint64_t n_elements, uint64_t stride) const
{
    gf_64_t dp = (gf_64_t)0;
    for (uint64_t i = 0; i < n_elements; ++i) {
      dp = add (dp, mul (*src1, *src2));
      src1 += stride;
      src2 += stride;
    }
    return dp;
}
