#include <arm_neon.h>
#include <string.h>
#include <stdio.h>

#include "gf16_neon.h"

typedef uint16x8_t  __v128u;
typedef uint8x8x2_t __v128x2u;

__v128u gf16_blend_mask(__v128u a, __v128u mask){
  __v128u tmp = vreinterpretq_u16_s16(vshrq_n_s16( vreinterpretq_s16_u16(mask), 15));
  return(vandq_u16(tmp,a));
}

static 
inline
__v128x2u to_split(__v128u v){
  return uint8x8x2_t({ vget_low_u8(vreinterpretq_u8_u16(v)), vget_high_u8(vreinterpretq_u8_u16(v))});
}

static 
inline
__v128u to_whole(__v128x2u v){
  return vcombine_u16(vreinterpret_u16_u8(v.val[0]), vreinterpret_u16_u8(v.val[1]));
}

/*
static
inline
void 
setup_htable_64(gf_16_t m, uint32_t prim_poly, uint8x8x2_t *htbl, uint8x8x2_t *ltbl) {
  uint8x8_t lv[4];
  uint8x8_t hv[4];
  
  uint16x4_t tmp0, tmp1, tmp2, tmp3;
  tmp0 = vcreate_u16(0x0003000200010000);
  tmp1 = vcreate_u16(0x0007000600050004);
  tmp2 = vcreate_u16(0x000b000a00090008);
  tmp3 = vcreate_u16(0x000f000e000d000c);
  for (int i = 0; i<4;i++){
    lv[i] = vcombine_u16(vshl_n_u16(tmp0,4*i),vshl_n_u16(tmp1,4*i));
    hv[i] = vcombine_u16(vshl_n_u16(tmp2,4*i),vshl_n_u16(tmp3,4*i));
  }
 
  int left_bit = __builtin_clz ((uint32_t)m) - 16;
  int i,j;

  __v128u     poly = vdupq_n_u16((int32_t)(prim_poly & 0xffff));
  uint64_t    mul = (uint64_t)m * 0x0001000100010001ULL;
  __v128u     t1;

  for (i = 15; i >= left_bit; --i) {
    tmp0 = vcreate_u16(mul << (uint64_t)i);
    for (j = 0;j<4;++j){
      t1 = vcombine_u16(tmp0,tmp0);
      ltbl[j] = to_split(veorq_u16(to_whole(ltbl[j]), gf16_blend_mask(lv[j], t1)));
      htbl[j] = to_split(veorq_u16(to_whole(htbl[j]), gf16_blend_mask(hv[j], t1)));
      t1 = gf16_blend_mask(poly, lv[j]);
      lv[j] = veorq_u16(vaddq_u16(lv[j], lv[j]), t1);
      t1 = gf16_blend_mask(poly, hv[j]);
      hv[j] = veorq_u16(vaddq_u16(hv[j], hv[j]), t1);
    }
  }
}
*/



static
inline
void 
setup_htable(gf_16_t m, uint32_t prim_poly, __v128x2u *htbl, __v128x2u *ltbl) {
  __v128u lv[4];
  __v128u hv[4];
  
  uint16x4_t tmp0, tmp1, tmp2, tmp3;
  /*
  tmp0 = vcreate_u16(0x0003000200010000);
  tmp1 = vcreate_u16(0x0007000600050004);
  tmp2 = vcreate_u16(0x000b000a00090008);
  tmp3 = vcreate_u16(0x000f000e000d000c);
  */
  tmp0 = vcreate_u16(0x0003000200010000);
  tmp1 = vcreate_u16(0x0007000600050004);
  tmp2 = vcreate_u16(0x000b000a00090008);
  tmp3 = vcreate_u16(0x000f000e000d000c);
  for (int i = 0; i<4;i++){
    lv[i] = vcombine_u16(vshl_n_u16(tmp0,4*i),vshl_n_u16(tmp1,4*i));
    hv[i] = vcombine_u16(vshl_n_u16(tmp2,4*i),vshl_n_u16(tmp3,4*i));
  }
 
  int left_bit = __builtin_clz ((uint32_t)m) - 16;
  int i,j;

  __v128u     poly = vdupq_n_u16((int32_t)(prim_poly & 0xffff));
  uint64_t    mul = (uint64_t)m * 0x0001000100010001ULL;
  __v128u     t1;

  for (i = 15; i >= left_bit; --i) {
    tmp0 = vcreate_u16(mul << (uint64_t)i);
    for (j = 0;j<4;++j){
      t1 = vcombine_u16(tmp0,tmp0);
      ltbl[j] = to_split(veorq_u16(to_whole(ltbl[j]), gf16_blend_mask(lv[j], t1)));
      htbl[j] = to_split(veorq_u16(to_whole(htbl[j]), gf16_blend_mask(hv[j], t1)));
      t1 = gf16_blend_mask(poly, lv[j]);
      lv[j] = veorq_u16(vaddq_u16(lv[j], lv[j]), t1);
      t1 = gf16_blend_mask(poly, hv[j]);
      hv[j] = veorq_u16(vaddq_u16(hv[j], hv[j]), t1);
    }
  }
}


static inline
__v128u unpacklo(uint8x16_t v){return vmovl_u8(vget_low_u8(v));}
static inline
__v128u unpackhi(uint8x16_t v){return vmovl_u8(vget_high_u8(v));}


static
inline
void
vector_stdmap(__v128u &v0, __v128u &v1, uint8x16_t res_l, uint8x16_t res_h){
  __v128u tl = unpacklo(res_l);
  __v128u th = unpacklo(res_h);
  __v128u sl = unpackhi(res_l);
  __v128u sh = unpackhi(res_h);
  v0  = vsliq_n_u16(sl,sh,8);
  v1  = vsliq_n_u16(tl,th,8);
}

static
inline
void 
vector_altmap(__v128u & one, __v128u & two){
  __v128u mask = vdupq_n_u16(0x00ff);
  uint8x8_t ab_h = vmovn_u16(vshrq_n_u16(one, 8));
  uint8x8_t cd_h = vmovn_u16(vshrq_n_u16(two, 8));
  uint8x8_t ab_l = vmovn_u16(vandq_u16(one, mask));
  uint8x8_t cd_l = vmovn_u16(vandq_u16(two, mask));
  one = vreinterpretq_u16_u8(vcombine_u8(cd_h,ab_h));
  two = vreinterpretq_u16_u8(vcombine_u8(cd_l,ab_l));
}

static
inline
void 
valtmap(__v128x2u & one, __v128x2u & two){
  __v128u mask = vdupq_n_u16(0x00ff);
  __v128u ab_h = vshrq_n_u16(to_whole(one), 8);
  __v128u ab_l = vandq_u16  (to_whole(one), mask);
  __v128u cd_h = vshrq_n_u16(to_whole(two), 8);
  __v128u cd_l = vandq_u16  (to_whole(two), mask);
  one = to_split(vreinterpretq_u16_u8(vcombine_u8(vmovn_u16(cd_h), vmovn_u16(ab_h))));
  two = to_split(vreinterpretq_u16_u8(vcombine_u8(vmovn_u16(cd_l), vmovn_u16(ab_l))));
}

static
inline
void 
lookup_altmap(__v128x2u *one, __v128x2u *two){
  for(int i=0;i<4;++i){
    valtmap(one[i],two[i]);
  }
}


static
inline
uint8x16_t shuffle(__v128x2u a,uint8x16_t b){
  return vcombine_u8(vtbl2_u8(a, vget_low_u8(b)), vtbl2_u8(a, vget_high_u8(b)));
}

static
inline
__v128u
gf16_mult_const_4(uint16x8_t v0, uint8x8x2_t *htbl, uint8x8x2_t *ltbl, uint16x4_t loset) {

  uint8x8_t v1,v2;
  v1 = vmovn_u16(v0);
  v2 = vshrn_n_u16(v0, 8);

  uint8x8_t res_l = vtbl2_u8(ltbl[2], vand_u8(v1,vreinterpret_u8_u16(loset)));
  uint8x8_t res_h = vtbl2_u8(htbl[2], vand_u8(v1,vreinterpret_u8_u16(loset)));

  v1 = vshr_n_u8(v1, 4);

  res_l = veor_u8(res_l, vtbl2_u8(ltbl[0], vand_u8(v2,vreinterpret_u8_u16(loset))));
  res_h = veor_u8(res_h, vtbl2_u8(htbl[0], vand_u8(v2,vreinterpret_u8_u16(loset))));

  v2 = vshr_n_u8(v2, 4);

  res_l = veor_u8(res_l, vtbl2_u8(ltbl[3],v2));
  res_h = veor_u8(res_h, vtbl2_u8(htbl[3],v2));
  res_l = veor_u8(res_l, vtbl2_u8(ltbl[1],v1));
  res_h = veor_u8(res_h, vtbl2_u8(htbl[1],v1));
  
  __v128u res  = vmovl_u8(res_l);
  return vorrq_u16(res, vshll_n_u8(res_h, 8));
}


static
inline
void 
gf16_mult_const_16(__v128u &v0, __v128u &v1, 
                              __v128x2u *htbl, __v128x2u *ltbl, __v128u loset) {

  uint8x16_t  vv0,vv1;
  vector_altmap(v0,v1);

  vv0 = vreinterpretq_u8_u16(vandq_u16(loset, v0));
  vv1 = vreinterpretq_u8_u16(vandq_u16(loset, v1));

  uint8x16_t res_l =      shuffle(ltbl[2],vv0);
  uint8x16_t res_h =      shuffle(htbl[2],vv0);
  res_l = veorq_u8(res_l, shuffle(ltbl[0],vv1));
  res_h = veorq_u8(res_h, shuffle(htbl[0],vv1));

  vv0 = vreinterpretq_u8_u16(vandq_u16(loset, vshrq_n_u16(v0,4)));
  vv1 = vreinterpretq_u8_u16(vandq_u16(loset, vshrq_n_u16(v1,4)));

  res_l = veorq_u8(res_l, shuffle(ltbl[3],vv0));
  res_h = veorq_u8(res_h, shuffle(htbl[3],vv0));
  res_l = veorq_u8(res_l, shuffle(ltbl[1],vv1));
  res_h = veorq_u8(res_h, shuffle(htbl[1],vv1));

  vector_stdmap(v0,v1,res_l,res_h);
}

void
gf16_neon_state::mul_region( const gf_16_t * src, gf_16_t * dst, uint64_t bytes,
                             gf_16_t multiplier, bool accum) const {
  if (multiplier == (gf_16_t)0) {
    if (! accum) {
      memset (dst, 0, bytes);
    }
    return;
  }
  else if (multiplier == (gf_16_t)1) {
    if (! accum){
      memcpy (dst, src, bytes);
    }   
    else {
      uint64_t    chunks = bytes >> 4ULL;
      for (uint64_t i = 0; i < chunks; i++) {
        __v128u x = vld1q_u16(src+i*8);
        __v128u y = vld1q_u16(dst+i*8);
        vst1q_u16(dst + i*8, veorq_u16(x,y));
      }
    }
    return;
  }   

  uint64_t    i;
  uint64_t    chunks = bytes >> 4ULL;
  __v128u     loset = vreinterpretq_u16_u8(vdupq_n_u8(0x0f));
  __v128u     v0,v1;
  __v128x2u   htbl[4];
  __v128x2u   ltbl[4];
  __v128x2u   zero = to_split(vdupq_n_u16(0));
  for(int j=0;j<4;++j){
    htbl[j] = zero;
    ltbl[j] = zero;
  }
  /*
  uint64_t    chunks = bytes >> 3ULL;
  uint16x4_t  loset = vreinterpret_u16_u8(vdup_n_u8(0x0f));
  uint16x8_t   v0;
  uint8x8x2_t htbl[4];
  uint8x8x2_t ltbl[4];
  uint8x8x2_t zero = {vget_low_u8(vdupq_n_u8(0)),vget_high_u8(vdupq_n_u8(0))};
  for(int j=0;j<4;++j){
    htbl[j] = zero;
    ltbl[j] = zero;
  }
  */
  setup_htable(multiplier, prim_poly, htbl, ltbl);
  lookup_altmap(htbl,ltbl);
  //setup_htable_64(multiplier, prim_poly, htbl, ltbl);

  // if the number of chunks is odd
  if(chunks&1){
    --chunks;
  }

  if (accum) {
    for (i = 0; i < chunks; i+=2) {
      v0 = vld1q_u16(src+i*8);
      v1 = vld1q_u16(src+(i+1)*8);

      gf16_mult_const_16(v0, v1, htbl, ltbl, loset);

      v0 = veorq_u16(vld1q_u16(dst + i*8), v0);
      v1 = veorq_u16(vld1q_u16(dst + (i+1)*8), v1);

      vst1q_u16(dst + i*8, v0);
      vst1q_u16(dst + (i+1)*8, v1);
    }
  } else {
    /*
    for (i = 0; i < chunks; i++) {
      v0 = vld1q_u16(src+i*8);
      v0 = gf16_mult_const_4(v0, htbl, ltbl, loset);
      vst1q_u16(dst + i*8, v0);
    }
    */
    for (i = 0; i < chunks; i+=2) {
      v0 = vld1q_u16(src+i*8);
      v1 = vld1q_u16(src+(i+1)*8);

      gf16_mult_const_16(v0, v1, htbl, ltbl, loset);

      vst1q_u16(dst + i*8, v0);
      vst1q_u16(dst + (i+1)*8, v1);
    }
  }
  for (i = chunks << 3ULL; i < bytes/2; ++i) {
    dst[i] = mul (src[i], multiplier) ^ (accum ? dst[i] : (gf_16_t)0);
  }
}


static 
inline
void 
mul_vec_by_vec_rnd(__v128u &r, __v128u v1, __v128u & v2, __v128u poly, const int pos){
    __v128u t1;
    t1=vshlq_n_u16(v1, pos);
    r=veorq_u16(r, gf16_blend_mask(v2, t1));
    t1=gf16_blend_mask(poly, v2);
    v2=veorq_u16(vaddq_u16(v2, v2), t1);
}


static
inline
__v128u mul_vec_by_vec(__v128u v1, __v128u v2, __v128u pp){
    __v128u r = vdupq_n_u16((uint16_t)0);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 15);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 14);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 13);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 12);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 11);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 10);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 9);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 8);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 7);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 6);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 5);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 4);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 3);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 2);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 1);
    r=veorq_u16(r, gf16_blend_mask(v2, v1));
    return r;
}




void
gf16_neon_state::mul_region_region (const gf_16_t * buf1, const gf_16_t * buf2, gf_16_t * dst,
                                    uint64_t bytes, bool accum) const { 
    uint64_t    i;
    uint64_t    chunks = bytes >> 4ULL;
    __v128u       v1, v2, r, pp;

    pp = vdupq_n_u16((uint16_t)prim_poly);
    if (accum) {
        for (i = 0; i < chunks; ++i) {
            v1 = vld1q_u16(buf1 + 8*i);
            v2 = vld1q_u16(buf2 + 8*i);
            r = veorq_u16 (vld1q_u16(dst + 8*i),
                                                mul_vec_by_vec (v1, v2, pp));
            vst1q_u16 (dst + 8*i, r);
        }
        for (i = (chunks * 8); i < bytes/2; ++i) {
            dst[i] = add (dst[i], mul (buf1[i], buf2[i]));
        }
    } else {
        for (i = 0; i < chunks; ++i) {
            v1 = vld1q_u16 (buf1 + 8*i);
            v2 = vld1q_u16 (buf2 + 8*i);
            r = mul_vec_by_vec (v1, v2, pp);
            vst1q_u16 (dst + 8*i, r);
        }
        for (i = (chunks * 8); i < bytes/2; ++i) {
            dst[i] = mul (buf1[i], buf2[i]);
        }
    }
}

gf_16_t
gf16_neon_state::dotproduct (const gf_16_t * src1, const gf_16_t * src2,
                             uint64_t n_elements, uint64_t stride) const{
    gf_16_t     result = (gf_16_t)0;
    uint64_t    i;
    uint64_t    chunks;
    __v128u     r;
    __v128u     accumulator = vdupq_n_u16((uint16_t)0);
    __v128u     pp = vdupq_n_u16 ((uint16_t)prim_poly);

    if (stride == 1) {
        chunks = n_elements >> 3ULL;
        for (i = 0; i < chunks; ++i) {
            r = mul_vec_by_vec (vld1q_u16 (src1 + 8*i),
                                vld1q_u16 (src2 + 8*i), pp);
            accumulator = veorq_u16 (r, accumulator);
        }
        for (i = chunks * 8; i < n_elements; ++i) {
            result ^= mul (src1[i], src2[i]);
        }
      result^=vgetq_lane_u16(accumulator,0);
      result^=vgetq_lane_u16(accumulator,1);
      result^=vgetq_lane_u16(accumulator,2);
      result^=vgetq_lane_u16(accumulator,3);
      result^=vgetq_lane_u16(accumulator,4);
      result^=vgetq_lane_u16(accumulator,5);
      result^=vgetq_lane_u16(accumulator,6);
      result^=vgetq_lane_u16(accumulator,7);

    } else if (stride == 2) {
    }
    return result;
}
