//
// gfw_log.h
// w-bit Galois field using basic shift-add (unoptimized).
//

#pragma     once

#include <typeinfo>
#include <assert.h>

#include "gf_types.h"
#include "gf_random.h"

template <class gf_w_t>
struct
alignas(16)
gfw_log_state {

    gfw_log_state (gf_w_t prim_poly_, gf_w_t alpha_ = (gf_w_t)1){
        this->alpha = alpha_;
        this->prim_poly = prim_poly_;
        assert(sizeof(gf_w_t)<= sizeof(gf_16_t));
        uint32_t b = this->alpha;
        uint32_t i = 1;
        bool field8 = (typeid(gf_w_t) == typeid(gf_8_t));
        //remove the first iteration so compiler doesnt compile about i+field_max
        log_tbl[b] = 0;
        antilog_tbl[0] = (gf_w_t)b;
        antilog_tbl[field_max()] = (gf_w_t)b;
        b <<= 1;
        for (; i < field_max(); ++i){
            log_tbl[b] = (gf_w_t)i;
            antilog_tbl[i] = (gf_w_t)b;
            antilog_tbl[i + field_max()] = (gf_w_t)b;
            b <<= 1;
            if (b & (1ULL << bitwidth())){
                b ^= prim_poly_;
                if (field8){
                  b ^= 0x100;
                } else {
                  b ^= 0x10000;
                }
            }
        }
    }

    // Basic arithmetic is all we need to support
    gf_w_t add (gf_w_t a, gf_w_t b) const {return ((gf_w_t)(a ^ b));}
    gf_w_t sub (gf_w_t a, gf_w_t b) const {return ((gf_w_t)(a ^ b));}
    gf_w_t inv (gf_w_t a) const;
    gf_w_t pow (gf_w_t a, uint64_t b) const;
    gf_w_t mul (gf_w_t a, gf_w_t b) const;
    gf_w_t div (gf_w_t a, gf_w_t b) const {return (mul (a, inv(b)));}
    gf_w_t log (gf_w_t a) const {return (log_tbl[a]);}
    gf_w_t antilog (gf_w_t a) const {return (antilog_tbl[a]);}

    void        mul_region (const gf_w_t * src, gf_w_t * dst, uint64_t bytes,
                    gf_w_t m, bool accum = false) const;
    void        mul_region_region (const gf_w_t * src1, const gf_w_t * src2, 
                    gf_w_t * dst, uint64_t bytes, bool accum = false) const;
    gf_w_t      dotproduct (const gf_w_t * src1, const gf_w_t * src2,
                            uint64_t n_elements, uint64_t stride = 1) const;

    uint64_t    bitwidth () const       {return (sizeof (gf_w_t) * 8ULL);}
    uint64_t    field_size () const     {return (1ULL << bitwidth());}
    uint64_t    field_max () const      {return (field_size() - 1);}

 private:
    gf_w_t log_tbl[(1ULL << (sizeof (gf_w_t) * 8ULL))];
    gf_w_t antilog_tbl[(1ULL << (sizeof (gf_w_t) * 8ULL)) * 2];
    gf_w_t prim_poly;
    gf_w_t alpha;
};

typedef gfw_log_state<gf_8_t>      gf8_log_state;
typedef gfw_log_state<gf_16_t>     gf16_log_state;

extern gf8_log_state               gf8_log_field;
extern gf16_log_state              gf16_log_field;
