//
// gf_erasure.h
//
// Routines for erasure coding.
//
// The class is set up when instantiated by supplying the number of data inputs and the
// desired number of ECC symbols.
//
// Supported methods are:
//
// generate_ecc (const gf_w * in[], gf_w * out[], uint64_t n_bytes)
//      Generates codeword symbols and places them into the buffers pointed to by out.
//      If you don't want to compute and store one or more symbols, set the corresponding
//      pointers in out to NULL.  Both input and output buffers must be in the correct order.
//      IMPORTANT: in must n_data slots, and all must be valid pointers.
//      IMPORTANT: out must have n_data + n_ecc slots.  Systematic codes use an identity
//                 matrix for the first n_data elements of out[].  If you don't want the
//                 routine to copy data from in[] to out[] for these elements, set the
//                 pointers to NULL.
//
// config_rebuild
//      
#pragma once

#include "gf_w.h"
#include "gf_matrix.h"

static const unsigned       ECC_CAUCHY_SYSTEMATIC = 1;
static const unsigned       ECC_CAUCHY = 2;
static const unsigned       ECC_VANDERMONDE = 3;
static const unsigned       ECC_PQ = 4;

template<class gf_w, class gf_state>
struct gf_erasure {
    gf_erasure (unsigned n_data_, unsigned n_ecc_, unsigned ecc_type_ = ECC_CAUCHY_SYSTEMATIC)
        : ecc_type(ecc_type_)
        , n_data(n_data_)
        , n_ecc(n_ecc_)
        , generator (n_data + n_ecc, n_data)
        , internal_recover (n_data, n_data)
        , partial (n_data, n_data)
        , scratch (n_data, n_data)
    {
        setup ();
    }

    void        generate (const gf_w * data[], gf_w * result[], uint64_t n_bytes) const;
    bool        config_rebuild (const gf_w in_ids[],
                                gf_matrix<gf_w, gf_state> * recover = (gf_matrix<gf_w, gf_state> *)0);
    bool        rebuild (const gf_w * in[], gf_w * out[], uint64_t n_bytes,
                         const gf_matrix<gf_w, gf_state> * recover = (gf_matrix<gf_w, gf_state> *)0) const;
private:
    unsigned                    ecc_type;
    unsigned                    n_data;
    unsigned                    n_ecc;
    gf_matrix<gf_w, gf_state>   generator;
    gf_matrix<gf_w, gf_state>   internal_recover;
    gf_matrix<gf_w, gf_state>   partial;
    gf_matrix<gf_w, gf_state>   scratch;
    void                        setup ();
};
