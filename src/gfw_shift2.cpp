/*
 * gfw_shift2.cpp
 *
 * Routines for n-bit Galois fields using simple shift and add.  These should be
 * independent of field width, assuming that there's a function that returns element
 * size in bits.
 *
 */

#include <string.h>
#include "gfw_shift2.h"

#include <stdio.h>


template<class gf_w_t>
gf_w_t
gfw_shift2_state<gf_w_t>::mul (gf_w_t a, gf_w_t b) const
{
    gf_w_t accum = (b & (gf_w_t)1) ? a : (gf_w_t)0;
    accum = (b & (gf_w_t)1) ? a : (gf_w_t)0;
    a = mul_by_2 (a);
    accum = add (accum, (b & (gf_w_t)2) ? a : 0);
    b >>= 2;
    for (unsigned i = 2; i < bitwidth(); i+=2) {
      if ((b&(gf_w_t)3)==(gf_w_t)1){
        a = mul_by_2 (a);
        accum = add (accum, (b & (gf_w_t)1) ? a : 0);
        a = ((a>>(bitwidth()-1)==1) ? a<<1^prim_poly : a<<1);
        b >>= 2;
      } else {
        mul_by_4(a,b,accum);
      } 
    }
    return accum;
}

template<class gf_w_t>
gf_w_t
gfw_shift2_state<gf_w_t>::pow (gf_w_t a, uint64_t b) const
{
    gf_w_t      accum = (b & 1) ? a : (gf_w_t)1;
    gf_w_t      powers = a;

    b >>= 1;
    while (b != 0) {
        powers = mul (powers, powers);
        if (b & 1) {
            accum = mul (accum, powers);
        }
        b >>= 1;
    }
    return accum;
}

template<class gf_w_t>
gf_w_t
gfw_shift2_state<gf_w_t>::inv (gf_w_t a) const
{
    return (a < 2) ? a : pow (a, (0xffffffffffffffffULL >> (64ULL - bitwidth())) ^ 0x1);
}

template<class gf_w_t>
void
gfw_shift2_state<gf_w_t>::mul_region (const gf_w_t * src, gf_w_t * dst, uint64_t bytes,
                                        gf_w_t multiplier, bool accum) const
{
    /*Faster without the ton of branches slightly higher average, reduced std dev*/
    /*
    if (multiplier == (gf_w_t)0) {
        if (! accum) {
            memset (dst, 0, bytes);
        }
        return;
    }
    else if (multiplier == (gf_w_t)1) {
        if (! accum){
            memcpy (dst, src, bytes);
        }
        else {
            uint64_t len = bytes;
            if (bitwidth() == (uint64_t)16) len = len >> 1;
            else if (bitwidth() == (uint64_t)32) len = len >> 2;
            else if (bitwidth() == (uint64_t)64) len = len >> 3;
            for (uint64_t i = 0; i < len; i++) {
                dst[i] ^= src[i];
            }
        }
        return;
    }
    */

    if (accum) {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            dst[i] = add (dst[i], mul (multiplier, src[i]));
        }
    } else {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            dst[i] = mul (multiplier, src[i]);
        }
    }
}

template<class gf_w_t>
void
gfw_shift2_state<gf_w_t>::mul_region_region ( const gf_w_t * src1, const gf_w_t * src2,
                                          gf_w_t * dst, uint64_t bytes, bool accum) const {

    if (accum) {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            dst[i] = add (dst[i], mul (src1[i], src2[i]));
        }
    } else {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            dst[i] = mul (src1[i], src2[i]);
        }
    }
}

template<class gf_w_t>
gf_w_t
gfw_shift2_state<gf_w_t>::dotproduct (const gf_w_t * src1, const gf_w_t * src2,
                                  uint64_t n_elements, uint64_t stride) const
{
    gf_w_t             dp = (gf_w_t)0;

    for (uint64_t i = 0; i < n_elements; ++i) {
        dp = add (dp, mul (*src1, *src2));
        src1 += stride;
        src2 += stride;
    }
    return dp;
}

template struct gfw_shift2_state<gf_8_t>;
template struct gfw_shift2_state<gf_16_t>;
template struct gfw_shift2_state<gf_32_t>;
template struct gfw_shift2_state<gf_64_t>;

gf8_shift2_state      gf8_shift2_field  (gf8_default_primpoly, (gf_8_t)1);
gf16_shift2_state     gf16_shift2_field (gf16_default_primpoly, (gf_8_t)1);
gf32_shift2_state     gf32_shift2_field (gf32_default_primpoly, (gf_8_t)1);
gf64_shift2_state     gf64_shift2_field (gf64_default_primpoly, (gf_8_t)1);
