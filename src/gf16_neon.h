#pragma     once

#include "gf_types.h"
#include "gfw_log.h"
#include "gf_random.h"

struct 
alignas(16)
gf16_neon_state {

    gf16_neon_state (gf_16_t prim_poly_ = gf16_default_primpoly, gf_16_t alpha_ = 1)
        : prim_poly (0x10000 | prim_poly_)
        , log_state (prim_poly_, alpha_)
    {
    }

    // single GF operands
    gf_16_t log     (gf_16_t a) const {return (log_state.log (a));}
    gf_16_t antilog (gf_16_t a) const {return (log_state.antilog (a));}
    gf_16_t inv     (gf_16_t a) const {return (log_state.inv (a));}
    gf_16_t pow     (gf_16_t a, uint32_t b) const {return (log_state.pow (a, b));}

    //GF arithmetic
    gf_16_t add (gf_16_t a, gf_16_t b) const {return ((gf_16_t)(a ^ b));}
    gf_16_t sub (gf_16_t a, gf_16_t b) const {return ((gf_16_t)(a ^ b));}
    gf_16_t mul (gf_16_t a, gf_16_t b) const {return (log_state.mul (a, b));}
    gf_16_t div (gf_16_t a, gf_16_t b) const {return (log_state.div (a, b));}

    void mul_region (const gf_16_t * src, gf_16_t * dst, uint64_t bytes, gf_16_t m,
                    bool accum = false) const;
    void mul_region_region (const gf_16_t * src1, const gf_16_t * src2, gf_16_t * dst,
                            uint64_t bytes, bool accum = false) const;
    gf_16_t       dotproduct (const gf_16_t * src1, const gf_16_t * src2,
                              uint64_t n_elements, uint64_t stride = 1) const;
    bool          rs_encode (gf_16_t ** const data, unsigned n_data,
                             gf_16_t ** parity, unsigned n_parity, uint64_t length) const;

    uint64_t    field_size () const     {return GF16_FIELD_SIZE;}
    uint64_t    field_max () const      {return (field_size() - 1);}


 private:
    uint32_t            prim_poly;
    gf_16_t             reduce_tbl[16];
    gf16_log_state      log_state;
};
