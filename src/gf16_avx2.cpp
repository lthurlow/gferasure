#include <string.h>
#include <immintrin.h>
#include <nmmintrin.h> //for _mm_blendv_epi8

#include "gf16_avx2.h"

#define _mm256_set_m128i(hi,lo) _mm256_insertf128_si256(_mm256_castsi128_si256(lo),(hi),1)

/*256 ethan blend*/
static
inline
__m256i 
gf16_blendv(__m256i l, __m256i mask){
  __m256i temp = _mm256_srai_epi16(mask, 15);
  return(_mm256_and_si256(temp,l));
}

/*GF16 AVX ALTERNATE MAPPING MULTIPLY*/
// lbtl and htbl are arrays, each with 2, 256 bit vectors
// the process is that we will take 2 128 bit vectors for the first 128 and second 128
// we will then do the shuffle for the multiple.  Ths means we have to do a total of 8 shuffles
// for each of the 128 bit vectors in both ltbl and htbl


static
inline 
void gf16_avx_mult_alt_16_alt(__m256i & v0, __m256i & v1, __m256i *htbl, __m256i *ltbl, __m256i loset) {
  //altmap words
  __m256i mask = _mm256_set1_epi16(0xff);
  __m256i ab_h = _mm256_srli_epi16(v0,8);
  __m256i ab_l = _mm256_and_si256(v0,mask);
  __m256i cd_h = _mm256_srli_epi16(v1,8);
  __m256i cd_l = _mm256_and_si256(v1,mask);
  v0 = _mm256_packus_epi16(cd_h,ab_h);
  v1 = _mm256_packus_epi16(cd_l,ab_l);

  __m256i lres, hres,bit_loc;

  bit_loc = _mm256_and_si256(loset, v0);
  lres = _mm256_shuffle_epi8(ltbl[2],bit_loc);
  hres = _mm256_shuffle_epi8(htbl[2],bit_loc);

  bit_loc = _mm256_and_si256(loset, _mm256_srli_epi64(v0,4));
  lres = _mm256_xor_si256(lres,_mm256_shuffle_epi8(ltbl[3],bit_loc));
  hres = _mm256_xor_si256(hres,_mm256_shuffle_epi8(htbl[3],bit_loc));

  bit_loc = _mm256_and_si256(loset,v1);
  lres = _mm256_xor_si256(lres,_mm256_shuffle_epi8(ltbl[0],bit_loc));
  hres = _mm256_xor_si256(hres,_mm256_shuffle_epi8(htbl[0],bit_loc));

  bit_loc = _mm256_and_si256(loset, _mm256_srli_epi64(v1,4));
  lres = _mm256_xor_si256(lres,_mm256_shuffle_epi8(ltbl[1],bit_loc));
  hres = _mm256_xor_si256(hres,_mm256_shuffle_epi8(htbl[1],bit_loc));
  
  v0=_mm256_unpackhi_epi8(lres,hres);
  v1=_mm256_unpacklo_epi8(lres,hres);
}


//convert vectors to ALT mapping using AVX
static
inline
void
to_avx_altmap(__m256i & one, __m256i & two){
  __m256i mask = _mm256_set1_epi16(0xff);
  __m256i ab_h = _mm256_srli_epi16(one,8);
  __m256i ab_l = _mm256_and_si256(one,mask);
  __m256i cd_h = _mm256_srli_epi16(two,8);
  __m256i cd_l = _mm256_and_si256(two,mask);
  one = _mm256_packus_epi16(cd_h,ab_h);
  two = _mm256_packus_epi16(cd_l,ab_l);
}


// this function will set up our tables.  there are two ways to set up the tables, one
// is to have the first table go 000x and the second table go 00x0.  The second apporach
// is to have the first half the table be 000x and the second half the table to be 00x0
// where in the first case x is 0-f and the second case x is 0-7.  This implementation is 
// using the second approach, storing the low values 0-7 in the lv and the high values in hv.
void gf16_avx_htable(uint16_t m, uint32_t prim_poly, __m256i & htbl, __m256i & ltbl) {

  __m256i lv[2];
  __m256i hv[2];
  lv[0] = _mm256_set_epi64x (
                             0x0070006000500040LL, 0x0030002000100000LL,
                             0x0007000600050004LL, 0x0003000200010000LL);
  lv[1] = _mm256_set_epi64x (
                             0x7000600050004000LL, 0x3000200010000000LL,
                             0x0700060005000400LL, 0x0300020001000000LL);
  hv[0] = _mm256_set_epi64x (
                             0x00f000e000d000c0LL, 0x00b000a000900080LL,
                             0x000f000e000d000cLL, 0x000b000a00090008LL);
  hv[1] = _mm256_set_epi64x (
                             0xf000e000d000c000LL, 0xb000a00090008000LL,
                             0x0f000e000d000c00LL, 0x0b000a0009000800LL);
  
  int left_bit = __builtin_clz ((uint32_t)m) - 16;
  int i,j;

  __m256i     poly = _mm256_set1_epi16((int32_t)(prim_poly & 0xffff));
  uint64_t    mul = (uint64_t)m * 0x0001000100010001ULL;
  __m256i     t1;

  //set both inputs to be 0
  (&ltbl)[0] = _mm256_setzero_si256 ();
  (&htbl)[0] = _mm256_setzero_si256 ();
  (&ltbl)[1] = _mm256_setzero_si256 ();
  (&htbl)[1] = _mm256_setzero_si256 ();
  
  for (i = 15; i >= left_bit; --i) {
    for (j=0; j<2;++j){
      t1 = _mm256_set1_epi64x (mul << (uint64_t)i);
      (&ltbl)[j] = _mm256_xor_si256((&ltbl)[j], gf16_blendv(lv[j], t1));
      (&htbl)[j] = _mm256_xor_si256((&htbl)[j], gf16_blendv(hv[j], t1));
      t1 = gf16_blendv(poly, lv[j]);
      lv[j] = _mm256_xor_si256(_mm256_add_epi16(lv[j], lv[j]), t1);
      t1 = gf16_blendv(poly, hv[j]);
      hv[j] = _mm256_xor_si256(_mm256_add_epi16(hv[j], hv[j]), t1);
    }
  }
}

/* This function will take our essentially 2 avx2 tables and convert them into 4 
 * The layout of the tables will mimic gf8 where we use 2x tables, with each table
 * containing duplicated information across 128 bit boundaries.
 * input = [A | B]
 * output = [A | A] [B | B]
*/

static
inline
void convert_tables(__m256i *htbl, __m256i *ltbl){
  ltbl[3]=_mm256_set_m128i(_mm256_extractf128_si256(ltbl[1],1),_mm256_extractf128_si256(ltbl[1],1));
  ltbl[2]=_mm256_set_m128i(_mm256_extractf128_si256(ltbl[1],0),_mm256_extractf128_si256(ltbl[1],0));
  ltbl[1]=_mm256_set_m128i(_mm256_extractf128_si256(ltbl[0],1),_mm256_extractf128_si256(ltbl[0],1));
  ltbl[0]=_mm256_set_m128i(_mm256_extractf128_si256(ltbl[0],0),_mm256_extractf128_si256(ltbl[0],0));
  
  htbl[3]=_mm256_set_m128i(_mm256_extractf128_si256(htbl[1],1),_mm256_extractf128_si256(htbl[1],1));
  htbl[2]=_mm256_set_m128i(_mm256_extractf128_si256(htbl[1],0),_mm256_extractf128_si256(htbl[1],0));
  htbl[1]=_mm256_set_m128i(_mm256_extractf128_si256(htbl[0],1),_mm256_extractf128_si256(htbl[0],1));
  htbl[0]=_mm256_set_m128i(_mm256_extractf128_si256(htbl[0],0),_mm256_extractf128_si256(htbl[0],0));
}

//mul region takes in two regions of memory.  It multiplies the first region by the
//multiplier, and stores the result in the second region of memory.  This function
//is used for multiplying many values by a single value at once.
//this version uses alternate mapping, to change this, do not use the to_avx_altmap
//as well as the alt_mult function.

void
gf16_avx2_state::mul_region (const gf_16_t * src, gf_16_t * dst, uint64_t bytes,
                            gf_16_t multiplier, bool accum) const {
    //if the multiplier is 0, memset 0 is quicker
    if (multiplier == (gf_16_t)0) {
        if (! accum) {
            memset (dst, 0, bytes);
        }
        return;
    }
    else if (multiplier == (gf_16_t)1) {
        if (! accum){
            memcpy (dst, src, bytes);
        }   
        else {
            uint64_t    chunks = bytes >> 5ULL;
            for (uint64_t i = 0; i < chunks; i++) {
                __m256i x = _mm256_loadu_si256 (((__m256i *)src) + i); 
                __m256i y = _mm256_loadu_si256 (((__m256i *)dst) + i); 
                _mm256_storeu_si256 (((__m256i *)dst) + i, _mm256_xor_si256(x,y));
            }   
        }   
        return;
    } 

    __m256i v0;
    __m256i v1;
    __m256i htbl[4];
    __m256i ltbl[4];

    uint64_t    i;
    uint64_t    chunks = bytes >> 5ULL;

    //set up multiplication tables for the multiplier
    gf16_avx_htable(multiplier, prim_poly, *htbl, *ltbl);
    //set up alternate mapping tables
    to_avx_altmap(htbl[0],ltbl[0]);
    to_avx_altmap(htbl[1],ltbl[1]);

    convert_tables(htbl, ltbl);

    __m256i loset = _mm256_set1_epi8(0x0f);
 
    if(chunks&1){
        --chunks;
    }

    // go through the region and multiply, if accumulator set, add in.
    if (accum) {
        for (i = 0; i < chunks; i+=2) {
            v0 = _mm256_loadu_si256 (((__m256i *)src) + i);
            v1 = _mm256_loadu_si256 (((__m256i *)src) + (i+1));
            gf16_avx_mult_alt_16_alt(v0, v1, htbl, ltbl, loset);
            v0 = _mm256_xor_si256 (_mm256_loadu_si256 (((__m256i *)dst) +i),
                                   v0);
            v1 = _mm256_xor_si256 (_mm256_loadu_si256 (((__m256i *)dst) +(i+1)),
                                   v1);
            _mm256_storeu_si256 (((__m256i *)dst) + i, v0);
            _mm256_storeu_si256 (((__m256i *)dst) + (i+1), v1);
        }
    } else {
        for (i = 0; i < chunks; i+=2) {
            v0 = _mm256_loadu_si256 (((__m256i *)src) + i);
            v1 = _mm256_loadu_si256 (((__m256i *)src) +(i+1));
            gf16_avx_mult_alt_16_alt(v0, v1, htbl, ltbl,loset);
            _mm256_storeu_si256 (((__m256i *)dst) + i, v0);
            _mm256_storeu_si256 (((__m256i *)dst) + (i+1), v1);
        }
    }
    for (i = chunks << 4ULL; i < bytes/2; ++i) {
        dst[i] = mul (src[i], multiplier) ^ (accum ? dst[i] : (gf_16_t)0);
    }
}

//add in v2 if 15-pos bit in v1 is set
static
inline
void
mul_vec_by_vec_rnd (__m256i & r, __m256i v1, __m256i & v2, __m256i poly, int pos)
{
    __m256i     t1;
    // Shift mask bit to high-order
    t1 = _mm256_slli_epi32 (v1, pos);
    // Add into accumulator (using XOR) if bit in multiplicand is non-zero
    r = _mm256_xor_si256(r, gf16_blendv(v2, t1));
    // Multiply by 2
    t1 = gf16_blendv(poly, v2);
    v2 = _mm256_xor_si256 (_mm256_add_epi16(v2, v2), t1);
}

//multiply two 16x16 vectors by one another
static
inline
__m256i
mul_vec_by_vec (__m256i v1, __m256i v2, __m256i pp)
{
    __m256i     r;
    r = _mm256_setzero_si256 ();

    for (unsigned i = 15; i > 0; --i) {
        mul_vec_by_vec_rnd (r, v1, v2, pp, i);
    }
    // Handle the last round
    r = _mm256_xor_si256 (r, gf16_blendv(v2, v1));
    return r;
}

//mul region by region takes in 3 regions of memory.  The first two regions are multiplied by
//eachother and are stored in the third region if accum isn't set, added into third region if it is set.
void
gf16_avx2_state::mul_region_region (const gf_16_t * buf1, const gf_16_t * buf2, gf_16_t * dst,
                                   uint64_t bytes, bool accum) const
{
    uint64_t    i;
    uint64_t    chunks = bytes >> 5ULL;

    __m256i     v1, v2, r, pp;

    //set primative polynomial
    pp = _mm256_set1_epi16((int32_t)(prim_poly & 0xffff));
    //multiply regions together.
    if (accum) {
        for (i = 0; i < chunks; ++i) {
            v1 = _mm256_loadu_si256 (((__m256i *)buf1) + i);
            v2 = _mm256_loadu_si256 (((__m256i *)buf2) + i);
            r = _mm256_xor_si256 (_mm256_loadu_si256 (((__m256i *)dst) + i),
                                                mul_vec_by_vec (v1, v2, pp));
            _mm256_storeu_si256 (((__m256i *)dst) + i, r);
        }
        for (i = (chunks<<4 ); i < bytes/2; ++i) {
            dst[i] = add (dst[i], mul (buf1[i], buf2[i]));
        }
    } else {
        for (i = 0; i < chunks; ++i) {
            v1 = _mm256_loadu_si256 (((__m256i *)buf1) + i);
            v2 = _mm256_loadu_si256 (((__m256i *)buf2) + i);
            r = mul_vec_by_vec (v1, v2, pp);
            _mm256_storeu_si256 (((__m256i *)dst) + i, r);
        }
        for (i = (chunks<<4 ); i < bytes/2; ++i) {
            dst[i] =  mul (buf1[i], buf2[i]);
        }
    }
}

/*
 * Dot product also multipliest two regions, however unlike multiply region
 * the result is a single GF16, as dot product will multiple each of the
 * two regions together and add the products together.
*/

gf_16_t
gf16_avx2_state::dotproduct (const gf_16_t * src1, const gf_16_t * src2,
                            uint64_t n_elements, uint64_t stride) const
{
    gf_16_t     result = (gf_16_t)0;
    uint64_t    i;
    uint64_t    chunks;
    __m256i     r;
    __m256i     accumulator = _mm256_setzero_si256 ();
    __m256i     pp = _mm256_set1_epi16 ((uint32_t)prim_poly);

    if (stride == 1) {
        chunks = n_elements >> 4ULL;
        for (i = 0; i < chunks; ++i) {
            r = mul_vec_by_vec (_mm256_loadu_si256 ((__m256i *)src1 + i),
                                _mm256_loadu_si256 ((__m256i *)src2 + i), pp);
            accumulator = _mm256_xor_si256 (r, accumulator);
        }
        for (i = chunks * sizeof (__m256i); i < n_elements; ++i) {
            result ^= mul (src1[i], src2[i]);
        }
        result ^= _mm256_extract_epi16 (accumulator, 0); 
        result ^= _mm256_extract_epi16 (accumulator, 1); 
        result ^= _mm256_extract_epi16 (accumulator, 2); 
        result ^= _mm256_extract_epi16 (accumulator, 3); 
        result ^= _mm256_extract_epi16 (accumulator, 4); 
        result ^= _mm256_extract_epi16 (accumulator, 5); 
        result ^= _mm256_extract_epi16 (accumulator, 6); 
        result ^= _mm256_extract_epi16 (accumulator, 7); 
        result ^= _mm256_extract_epi16 (accumulator, 8); 
        result ^= _mm256_extract_epi16 (accumulator, 9); 
        result ^= _mm256_extract_epi16 (accumulator, 10); 
        result ^= _mm256_extract_epi16 (accumulator, 11); 
        result ^= _mm256_extract_epi16 (accumulator, 12); 
        result ^= _mm256_extract_epi16 (accumulator, 13); 
        result ^= _mm256_extract_epi16 (accumulator, 14); 
        result ^= _mm256_extract_epi16 (accumulator, 15); 
    } else if (stride == 2) {
/*
        uint64_t        chunks = n_elements >> 2ULL;
        __m128i         r;
        __m128i         accumulator = _mm_setzero_si128 ();
        __m128i         pp = _mm_set1_epi16 ((uint32_t)prim_poly);

        for (i = 0; i < chunks; ++i) {
            r = mul_vec_by_vec (_mm_loadu_si128 ((__m128i *)src1 + i),
                                _mm_loadu_si128 ((__m128i *)src2 + i), pp);
            accumulator = _mm_xor_si128 (r, accumulator);
        }
        for (i = chunks * sizeof (__m128i); i < n_elements; ++i) {
            result ^= mul (src1[i], src2[i]);
        }
        result ^= _mm_extract_epi16 (accumulator, 0); 
        result ^= _mm_extract_epi16 (accumulator, 1); 
        result ^= _mm_extract_epi16 (accumulator, 2); 
        result ^= _mm_extract_epi16 (accumulator, 3); 
        result ^= _mm_extract_epi16 (accumulator, 4); 
        result ^= _mm_extract_epi16 (accumulator, 5); 
        result ^= _mm_extract_epi16 (accumulator, 6); 
        result ^= _mm_extract_epi16 (accumulator, 7); 
*/
    }
    return result;
}

inline
__m128i
multiply_vec_by_2 (__m128i v, __m128i reducer, __m128i mask)
{
    return _mm_xor_si128 (_mm_shuffle_epi8 (reducer, _mm_srli_epi64 (_mm_and_si128 (v, mask), 7)),
                          _mm_add_epi8 (v, v));
}
