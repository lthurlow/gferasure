//
// Test Harness for gf erasure, by using secret splitting
//

//#include <string.h> // is not used at all
#include <stdio.h> // for printf and stdout
#include <stdlib.h> // for strtol and exit
#include <getopt.h> // included for user input
#include <sys/time.h> // for the gettimeofday
#include <string.h>
#include <ctype.h>

#include "gf_secretsplit.h" // included to test secret split
#include "gf_random.h"

struct timer {
    double      start_time;
    double      split_time;
    void        start ()
    {
        start_time = split_time = gettime_d ();
    }

    double      split ()
    {
        double  new_t = gettime_d ();
        double  elapsed = new_t - split_time;
        split_time = new_t;
        return elapsed;
    }
    double      stop ()
    {
        double  new_t = gettime_d ();
        return new_t - start_time;
    }
    static double       gettime_d () {
        struct  timeval tv;
        gettimeofday (&tv, NULL);
        return (double)tv.tv_sec + (double)tv.tv_usec * 1e-6;
    }
};



//struct for storing user options
struct option long_options[] = {
    {"shares",     required_argument, 0, 's' },
    {"min_shares", required_argument, 0, 'm' },
    {"bytes",      required_argument, 0, 'b' },
    {"trials",     required_argument, 0, 't' },
    {"gf",         required_argument, 0, 'g' },
    {"random_ids", no_argument,       0, 'r' },
    {"timed",      required_argument, 0, 'T' },
    {0,            0,                 0, 0   }
};

//contains all info for user when program is not run properly
struct help_msgs {
    const char *        opts;
    const char *        msg;
} my_help_msgs[] = {
    {"-s, --shares", "Number of shares to split (default=24)"},
    {"-m, --min_shares", "Minimum number of shares to rebuild (default=2+shares/2"},
    {"-b, --bytes", "Size of secret to split / rebuild (default=128)"},
    {"-t, --trials", "Number of trials to run (default=1000)"},
    {"-r, --random_ids", "Use random share IDs (default is sequential)"},
    {"-l, --limit_ids", "Use a limited number of IDs"},
    {"-g, --gf", "Use a given Galois field (default=gf8_log):\n\t\tgf8_shiftadd gf8_log gf8_ssse3 gf8_avx2\n\t\tgf16_shiftadd gf16_log gf16_ssse3 gf16_avx2 gf32_ssse3 gf64_ssse3\n"},
    {"-T, --timed", "Time test to run (m = region/constant, r=region/region, d=dot product"},
};

//function to be called on error, iterates through each option in above struct
void
print_help(const char * prog, bool do_exit){

    fprintf (stderr, "Usage: %s [options]\n", prog);
    for (unsigned i = 0; i < sizeof(my_help_msgs)/sizeof (my_help_msgs[0]); ++i){
        fprintf (stderr,"\t%-20s: %s\n", 
                 my_help_msgs[i].opts, my_help_msgs[i].msg);
    }
    if (do_exit){
        exit (1);
    }
}


template<typename gf_w, typename gf_state>
double
time_trial (gf_w * buf1, gf_w * buf2, gf_w * buf3, uint64_t len,
            const gf_state & state, char which_test, unsigned n_trials)
{
    gf_w                dotprod = (gf_w)0;
    struct timer        tmr;
    double elapsed = 0.0;

    tmr.start ();
    for (unsigned i = 0; i < n_trials; ++i) {
        // Assign new values so we force execution of the test
        buf2[0] = gf_random_val<gf_w> ();
        switch (which_test) {
        case 'm':
            state.mul_region (buf1, buf3, len, buf2[0]);
            break;
        case 'r':
            state.mul_region_region (buf1, buf2, buf3, len);
            break;
        case 'd':
            dotprod ^= state.dotproduct (buf1, buf2, len / sizeof (gf_w));
            break;
        default:
            break;
        }
    }
    elapsed += tmr.stop();
    return (elapsed);
}

int main (int argc, char * argv[]){

    unsigned    n_shares = 24;
    unsigned    n_min = 2+n_shares/2;
    unsigned    n_bytes = 128;
    unsigned    n_trials = 1000;
    unsigned    n_random_ids = 0;
    int         c;
    char        timed_test = '\0';
    double      compute_time = 0.0;
    const char * gf_str = "gf8_log";

    // read in user inputs, store in appropriate variables
    while (1) {
        c = getopt_long (argc, argv, "s:m:b:t:l:X:T:g:rh?", long_options, NULL);

        if (c == -1) {
            break;
        }

        switch (c) {
        case 's':
            n_shares = strtol (optarg, NULL, 10);
            break;
        case 'm':
            n_min = strtol (optarg, NULL, 10);
            break;
        case 'b':
            n_bytes = strtol (optarg, NULL, 10);
            break;
        case 't':
            n_trials = strtol (optarg, NULL, 10);
            break;
        case 'r':
            n_random_ids = n_shares;
            break;
        case 'g':
            gf_str = optarg;
        case 'l':
            n_random_ids = strtol (optarg, NULL, 10);
            break;
        case 'T':
            timed_test = tolower (optarg[0]);
            if (! strchr ("mrd", timed_test)) {
                print_help (argv[0], true);
            }
            break;
        case 'h':
        case '?':
            print_help (argv[0], true);
            break;
        default:
            fprintf (stderr, "%s: option %c not recognized!\n", argv[0], c);
            print_help (argv[0], true);
            break;
        }
    }

    if (n_trials < 1 || n_trials > 100000) {
        fprintf (stderr, "%s: trials (%d) must be 1-100000\n", argv[0], n_trials);
        print_help (argv[0], true);
    }

    if (timed_test == '\0') {
        if (n_shares > 0) {
            if (n_shares < 4 || n_shares > 200) {
                fprintf (stderr, "%s: shares (%d) must be between 4 and 200 (inclusive)\n",
                         argv[0], n_shares);
                print_help (argv[0], true);
            }

            if (n_min == 0) {
                n_min = (n_shares / 2) + 2;
            } else if (n_min > n_shares || n_min < 2) {
                fprintf (stderr, 
                         "%s: minimum shares (%d) must be between 2 and n_shares (inclusive)\n",
                         argv[0], n_min);
                print_help (argv[0], true);
            }

            if (n_random_ids > n_shares) {
                fprintf (stderr, 
                         "%s: number of random IDs (%d) must be no greater than n_shares (%d)\n",
                         argv[0], n_random_ids, n_shares);
                print_help (argv[0], true);
            }

            if (n_bytes < 8 || n_bytes > 256 || n_bytes % 8 != 0) {
                fprintf (stderr, "%s: bytes (%d) must be 8-256 and evenly divisible by 8\n",
                         argv[0], n_bytes);
                print_help (argv[0], true);
            }
        } else {
            if (n_bytes % 16 != 0 || n_bytes > 1024 * 1024 * 32) {
                fprintf (stderr, "%s: bytes (%d) must be less than 32MB and evenly divisible by 16\n",
                         argv[0], n_bytes);
                print_help (argv[0], true);
            }
        }


        unsigned result;
        struct timer tmr;
        double elapsed = 0.0;
        if (! strcmp (gf_str, "gf8_shiftadd")) {
            split_secret<gf_8_t, gf8_shiftadd_state> sec_sa8(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_sa8.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
        } else if (! strcmp (gf_str, "gf8_log")) {
            split_secret<gf_8_t, gf8_log_state> sec_log8(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_log8.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
        } else if (! strcmp (gf_str, "gf16_shiftadd")) {
            split_secret<gf_16_t, gf16_shiftadd_state> sec_sa16(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_sa16.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
        } else if (! strcmp (gf_str, "gf16_log")) {
            split_secret<gf_16_t, gf16_log_state> sec_log16(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_log16.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
        } else if (! strcmp (gf_str, "gf32_shiftadd")) {
            split_secret<gf_32_t, gf32_shiftadd_state> sec_sa32(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_sa32.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
        } else if (! strcmp (gf_str, "gf64_shiftadd")) {
            split_secret<gf_64_t, gf64_shiftadd_state> sec_sa64(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_sa64.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();

#ifdef  GF_SSSE3
        } else if (! strcmp (gf_str, "gf8_ssse3")) {
            split_secret<gf_8_t, gf8_ssse3_state> sec_ssse8(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_ssse8.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
        } else if (! strcmp (gf_str, "gf16_ssse3")) {
            split_secret<gf_16_t, gf16_ssse3_state> sec_ssse16(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_ssse16.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
#endif
#ifdef  GF_CLMUL
        } else if (! strcmp (gf_str, "gf8_clmul")) {
            split_secret<gf_8_t, gf8_clmul_state> sec_clmul8(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_clmul8.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
        } else if (! strcmp (gf_str, "gf16_clmul")) {
            split_secret<gf_16_t, gf16_clmul_state> sec_clmul16(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_clmul16.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
        } else if (! strcmp (gf_str, "gf32_clmul")) {
            split_secret<gf_32_t, gf32_clmul_state> sec_clmul32(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_clmul32.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
        } else if (! strcmp (gf_str, "gf64_clmul")) {
            split_secret<gf_64_t, gf64_clmul_state> sec_clmul64(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_clmul64.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
#endif  // GF_SSSE3

#ifdef  GF_AVX2
        } else if (! strcmp (gf_str, "gf8_avx2")) {
            split_secret<gf_8_t, gf8_avx2_state> sec_avx8(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_avx8.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
        } else if (! strcmp (gf_str, "gf16_avx2")) {
            split_secret<gf_16_t, gf16_avx2_state> sec_avx16(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_avx16.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
#endif
#if defined(GF_NEON32) || defined(GF_NEON64)
        } else if (! strcmp (gf_str, "gf8_neon")) {
            split_secret<gf_8_t, gf8_neon_state> sec_neon8(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_neon8.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
        } else if (! strcmp (gf_str, "gf16_neon")) {
            split_secret<gf_16_t, gf16_neon_state> sec_neon16(n_bytes, n_shares, n_min);
            tmr.start();
            result = sec_neon16.sanity_check (n_trials, n_random_ids);
            elapsed = tmr.stop();
#endif
        } else {
            printf ("Unrecognized field: %s\n", gf_str);
            exit (1);
        }
        printf ("Sanity check on %s (shares=%d min=%d bytes=%d trials=%d) returned %d - %s!\n",
                gf_str, n_shares, n_min, n_bytes, n_trials, result,
                n_trials == result ? "success" : "failure");
        printf ("Elapsed time: %.3f seconds (%.1f us/trial)\n", elapsed,
                elapsed * 1.0e6 / (double)n_trials);
    }

    if (timed_test != '\0') {
        gf_32_t *       buf1 = new gf_32_t[n_bytes / sizeof (gf_32_t)];
        gf_32_t *       buf2 = new gf_32_t[n_bytes / sizeof (gf_32_t)];
        gf_32_t *       buf3 = new gf_32_t[n_bytes / sizeof (gf_32_t)];
        for (unsigned i = 0; i < n_bytes / sizeof (gf_32_t); ++i) {
            buf1[i] = gf_random_val<gf_32_t> ();
            buf2[i] = gf_random_val<gf_32_t> ();
        }

        if (! strcmp (gf_str, "gf8_shiftadd")) {
            gf8_shiftadd_state  field ((gf_8_t)0035, (gf_8_t)1);
            compute_time = time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf16_shiftadd")) {
            gf16_shiftadd_state field ((gf_16_t)0x100b, (gf_16_t)1);
            compute_time = time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf32_shiftadd")) {
            gf32_shiftadd_state field ((gf_32_t)0x000000c5, (gf_32_t)1);
            compute_time = time_trial ((gf_32_t *)buf1, (gf_32_t *)buf2, (gf_32_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf64_shiftadd")) {
            gf64_shiftadd_state field ((gf_64_t)0x000000000000001b, (gf_64_t)1);
            compute_time = time_trial ((gf_64_t *)buf1, (gf_64_t *)buf2, (gf_64_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
#ifdef  GF_SHIFT2
        } else if (! strcmp (gf_str, "gf8_shift2")) {
            gf8_shift2_state  field ((gf_8_t)0035, (gf_8_t)1);
            compute_time = time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf16_shift2")) {
            gf16_shift2_state field ((gf_16_t)0x100b, (gf_16_t)1);
            compute_time = time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf32_shift2")) {
            gf32_shift2_state field ((gf_32_t)0x000000c5, (gf_32_t)1);
            compute_time = time_trial ((gf_32_t *)buf1, (gf_32_t *)buf2, (gf_32_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf64_shift2")) {
            gf64_shift2_state field ((gf_64_t)0x000000000000001b, (gf_64_t)1);
            compute_time = time_trial ((gf_64_t *)buf1, (gf_64_t *)buf2, (gf_64_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
#endif
#ifdef  GF_LOG
        } else if (! strcmp (gf_str, "gf8_log")) {
            gf8_log_state       field ((gf_8_t)0035, (gf_8_t)1);
            compute_time = time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf16_log")) {
            gf16_log_state      field ((gf_16_t)0x100b, (gf_16_t)1);
            compute_time = time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
#endif
#ifdef  GF_SSSE3
        } else if (! strcmp (gf_str, "gf8_ssse3")) {
            gf8_ssse3_state     field ((gf_8_t)0035, (gf_8_t)1);
            compute_time = time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf16_ssse3")) {
            gf16_ssse3_state    field ((gf_16_t)0x100b, (gf_16_t)1);
            compute_time = time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
#endif  // GF_SSSE3
#ifdef  GF_CLMUL
        } else if (! strcmp (gf_str, "gf8_clmul")) {
            gf8_clmul_state     field ((gf_8_t)0035, (gf_8_t)1);
            compute_time = time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf16_clmul")) {
            gf16_clmul_state    field ((gf_16_t)0x002d, (gf_16_t)1);
            compute_time = time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf32_clmul")) {
            gf32_clmul_state field ((gf_32_t)0x000000c5, (gf_32_t)1);
            compute_time = time_trial ((gf_32_t *)buf1, (gf_32_t *)buf2, (gf_32_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf64_clmul")) {
            gf64_clmul_state field ((gf_64_t)0x000000000000001b, (gf_64_t)1);
            compute_time = time_trial ((gf_64_t *)buf1, (gf_64_t *)buf2, (gf_64_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
#endif  // GF_CLMUL
#ifdef  GF_AVX2
        } else if (! strcmp (gf_str, "gf8_avx2")) {
            gf8_avx2_state      field ((gf_8_t)0035, (gf_8_t)1);
            compute_time = time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf8_gather")) {
            gf8_gather_state      field ((gf_8_t)0035, (gf_8_t)1);
            compute_time = time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf16_avx2")) {
            gf16_avx2_state     field ((gf_16_t)0x100b, (gf_16_t)1);
            compute_time = time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf16_gather")) {
            gf16_gather_state      field ((gf_16_t)0x100b, (gf_16_t)1);
            compute_time = time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
#endif
#if defined(GF_NEON32) || defined(GF_NEON64)
        } else if (! strcmp (gf_str, "gf8_neon")) {
            gf8_neon_state      field ((gf_8_t)0035, (gf_8_t)1);
            compute_time = time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf16_neon")) {
            gf16_neon_state     field ((gf_16_t)0x100b, (gf_16_t)1);
            compute_time = time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf8_vmull")) {
            gf8_vmull_state     field ((gf_8_t)0035, (gf_8_t)1);
            compute_time = time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf16_vmull")) {
            gf16_vmull_state     field ((gf_16_t)0x100b, (gf_16_t)1);
            compute_time = time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf32_vmull")) {
            gf32_vmull_state    field ((gf_32_t)0x000000c5, (gf_32_t)1);
            compute_time = time_trial ((gf_32_t *)buf1, (gf_32_t *)buf2, (gf_32_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
        } else if (! strcmp (gf_str, "gf64_vmull")) {
            gf64_vmull_state    field ((gf_64_t)0x000000000000001b, (gf_64_t)1);
            compute_time = time_trial ((gf_64_t *)buf1, (gf_64_t *)buf2, (gf_64_t *)buf3,
                                       n_bytes, field, timed_test, n_trials);
#endif
        } else {
            printf ("Unrecognized field: %s\n", gf_str);
            exit (1);
        }
        printf ("%s: test %c took %.8f seconds for %d trials of %d bytes (%.2f MB/s)\n",
                gf_str, timed_test, compute_time, n_trials, n_bytes,
                //((double)n_bytes) / ((compute_time/(double)n_trials) * 1024.0 * 1024.0));
                ((double)n_trials * (double)n_bytes) / (compute_time * 1024.0 * 1024.0));
    }
}
