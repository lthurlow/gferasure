//
// Application to use GF libraries
//

#include <string.h> //memcpy, memset operations
//#include <stdio.h> not used
#include "gf_secretsplit.h"

template<typename gf_w, typename gf_state>
split_secret<gf_w, gf_state>::split_secret (unsigned len_, unsigned n_shares_, unsigned n_min_)

  : len((len_ + sizeof (gf_w) - 1) / sizeof (gf_w))
  , secret_len(len_)
  , n_shares(n_shares_)
  , n_min(n_min_)
  , n_valid_shares (0U)
  , secret_valid (false)
  , generator (n_shares_, n_min_)
  , partial (n_min_, n_min_)
  , inv (n_min_, n_min_)
  , single_row (1, n_min_)
{
  //not sure what the gf_w for n_shares + n_min is needed
  unsigned space_needed = (sizeof (secret_share_t) * n_shares +
                           sizeof (gf_w *) * n_min +
                           sizeof (gf_w) * (n_shares + n_min) * len +
                           sizeof (uint64_t)
                          );

  // allocate space for new data structure
  region = new uint64_t[space_needed / sizeof (uint64_t)];
  shares = (secret_share_t *)region;
  orig = (gf_w **)(shares + n_shares);
  orig[0] = (gf_w *)(orig + n_min);
  for (unsigned i = 1; i < n_min; ++i) {
    orig[i] = orig[i-1] + len;
  }
  shares[0].data = (uint8_t *)(orig[n_min-1] + len);
  shares[0].id = 0;
  for (unsigned i = 1; i < n_shares; ++i) {
    shares[i].data = shares[i-1].data + len * sizeof (gf_w);
    shares[i].id = 0;
  }
}

//deconstructor for split_secret, remove memory allocation
template<typename gf_w, typename gf_state> 
split_secret<gf_w, gf_state>::~split_secret (){
  delete region;
}

template<typename gf_w, typename gf_state>
int
split_secret<gf_w, gf_state>::create_shares (void * secret_, unsigned * _share_ids){

    // Zero the last word of the first slot, and then overwrite with secret
    orig[0][len-1] = (gf_w)0;
    memcpy (orig[0], secret_, secret_len);
    // Fill the rest of orig with random data
    for (unsigned slot = 1; slot < n_min; ++slot){
        for (unsigned i = 0; i < len; ++i){
            orig[slot][i] = gf_random_val<gf_w>();
        }
    }
    // If we're passed a NULL pointer for share IDs or the first share ID is 0,
    // use the share IDs sequential starting from 1.
    if (_share_ids == (unsigned *)0 || _share_ids[0] == 0U){
        for (unsigned i = 0; i < n_shares; ++i){
            shares[i].id = i + 1;
        }
    } else {
        unsigned        here;
        unsigned        ctr;

        for (ctr = 0; ctr < n_shares && _share_ids[ctr] != 0; ++ctr){
            if ((uint64_t)_share_ids[ctr] > generator.field.field_max()) {
                return 0;
            }
            for (here = 0; here < ctr; ++here){
                if (_share_ids[ctr] < shares[here].id){
                    break;
                }
            }
            // We can't have duplicate share IDs!
            if (shares[here].id == _share_ids[ctr]){
                return 0;
            }
            for (unsigned j = ctr; j > here; --j){
                shares[j].id = shares[j-1].id;
            }
            shares[here].id = _share_ids[ctr];
        }

        if (ctr < n_shares) {
            unsigned offset = n_shares - ctr - 1;
            for (unsigned i = ctr; i > 0; --i){
                shares[i + offset].id = shares[i - 1].id;
            }

            unsigned cur_share = 1;
            unsigned cur_hi_ndx = n_shares - ctr;
            unsigned cur_lo_ndx = 0;
            for (unsigned i = 0; i < n_shares - ctr; ++i){
                while (cur_hi_ndx < n_shares && cur_share >= shares[cur_hi_ndx].id){
                    cur_share = shares[cur_hi_ndx].id + 1;
                    shares[cur_lo_ndx++].id = shares[cur_hi_ndx++].id;
                }
                shares[cur_lo_ndx++].id = cur_share;
                cur_share += 1;
            }
        }
    }
    n_valid_shares = n_shares;

    gf_w        share_ids[n_shares];
    gf_w *      share_data[n_shares];
    for (unsigned i = 0; i < n_shares; ++i){
        share_ids[i] = (gf_w)shares[i].id;
        share_data[i] = (gf_w *)shares[i].data;
    }

    generator.set_vandermonde (share_ids);

    // Generate all of the shares
    generator.apply ((const gf_w **)orig, share_data, len * sizeof (gf_w));

    secret_valid = true;
    return 1;
}

template<typename gf_w, typename gf_state>
unsigned
split_secret<gf_w, gf_state>::find_by_id (unsigned id) const {

    for (unsigned i = 0; i < n_valid_shares; ++i){
        if (id == shares[i].id){
            return i;
        }
    }
    return n_valid_shares;
}

template<typename gf_w, typename gf_state>
int
split_secret<gf_w, gf_state>::delete_share (unsigned id){

    unsigned pos = find_by_id (id);

    if (pos == n_valid_shares){
        return 0;
    }

    n_valid_shares -= 1;
    for (unsigned i = pos; i < n_valid_shares; ++i){
        shares[i] = shares[i+1];
    }
    return 1;
}

static 
inline 
void 
shift_shares_forward(secret_share_t * shares, unsigned start, unsigned end){
    // printf ("Shifting shares forward from %d to %d\n", start, end);
    secret_share_t      t = shares[end];
    for (unsigned i = end; i > start; --i) {
        shares[i] = shares[i-1];
    }
    shares[start] = t;
}

template<typename gf_w, typename gf_state>
bool
split_secret<gf_w, gf_state>::set_share (const secret_share_t * new_share){

    unsigned pos;

    for (pos=0; pos < n_valid_shares && new_share->id >= shares[pos].id;++pos){
        if (new_share->id == shares[pos].id){
            return(
                   !memcmp(new_share->data, shares[pos].data, len * sizeof (gf_w)));
        }
    }
    if (n_valid_shares == n_shares) {
        return false;
    }
    // If we have a new number but no room for it, we've failed!
    //printf ("Setting share %04d at slot %d\n", new_share->id, pos);
    shift_shares_forward (shares, pos, n_valid_shares);

    shares[pos].id = new_share->id;
    memcpy (shares[pos].data, new_share->data, len * sizeof (gf_w));
    n_valid_shares += 1;
    return 1;
}

template<typename gf_w, typename gf_state>
bool
split_secret<gf_w, gf_state>::get_share (secret_share_t * to_rebuild){

    unsigned pos;
    gf_w basis[1];
    gf_w * rebuilt_data[1];

    for (pos = 0; pos < n_valid_shares && to_rebuild->id >= shares[pos].id; ++pos) {
        if (to_rebuild->id == shares[pos].id) {
            memcpy (to_rebuild->data, shares[pos].data, sizeof (gf_w) * len);
            return true;
        }
    }

    // Didn't find the share, but nowhere to put it!
    if (n_valid_shares >= n_shares) {
        return false;
    }

    //printf ("Getting share %04d into slot %d\n", to_rebuild->id, pos);
    shift_shares_forward (shares, pos, n_valid_shares);
    shares[pos].id = to_rebuild->id;

    // This returns quickly if the secret is already found.
    find_secret ();
    if (! secret_valid) {
        return false;
    }

    // Recompute the single share we were asked for.
    basis[0] = shares[pos].id;
    rebuilt_data[0] = (gf_w *)shares[pos].data;
    single_row.set_vandermonde (basis);
    single_row.apply (orig, rebuilt_data, len * sizeof (gf_w));
    n_valid_shares += 1;

    memcpy (to_rebuild->data, shares[pos].data, len * sizeof (gf_w));
    return 1;
}

template<typename gf_w, typename gf_state> 
unsigned
split_secret<gf_w, gf_state>::get_all_valid_ids (unsigned * ids) const {

    if (ids != (unsigned *)0) {
        for (unsigned i = 0; i < n_valid_shares; ++i) {
            ids[i] = shares[i].id;
        }
    }
    return n_valid_shares;
}

template<typename gf_w, typename gf_state>
unsigned 
split_secret<gf_w, gf_state>::get_all_valid_shares (secret_share_t * out) const {

    for (unsigned i = 0; i < n_valid_shares; ++i) {
        out[i].id = shares[i].id;
        memcpy (out[i].data, shares[i].data, sizeof (gf_w) * len);
    }
    return n_valid_shares;
}

template<typename gf_w, typename gf_state>
int
split_secret<gf_w, gf_state>::find_secret (void){

    gf_w* share_data[n_min];
    gf_w  share_ids[n_min];

    if (secret_valid) {
        return 1;
    }
    if (n_valid_shares < n_min) {
        return 0;
    }

    for (unsigned i = 0; i < n_min; ++i) {
        share_data[i] = (gf_w *)shares[i].data;
        share_ids[i]  = shares[i].id;
    }

    partial.set_vandermonde (share_ids);
    partial.invert (&inv);
    inv.apply (share_data, orig, len * sizeof (gf_w));
    secret_valid = true;
    return 1;
}

template<typename gf_w, typename gf_state>
void
split_secret<gf_w, gf_state>::reset (void){

    for (unsigned i = 0; i < n_min; ++i) {
        memset (orig[i], 0, len * sizeof (gf_w));
    }
    for (unsigned i = 0; i < n_shares; ++i) {
        memset (shares[i].data, 0, len * sizeof (gf_w));
        shares[i].id = 0;
    }
    secret_valid = false;
    n_valid_shares = 0;
}

// base function which will run secret splitting.
template<typename gf_w, typename gf_state>
int
split_secret<gf_w, gf_state>::sanity_check(unsigned n_trials, unsigned n_random_ids){

    secret_share_t      my_shares[65536];
    unsigned            id_list[n_shares + 1];
    secret_share_t      single_share;
    uint8_t             dataspace[n_shares + 2][len * sizeof (gf_w)];
    unsigned            used[n_shares];
    unsigned            cur, x;
    gf_w                my_secret[len];

    if (secret_valid) {
        // If we have a valid secret, assume that we don't want to destroy it, which this
        // sanity check would do.
        return -1;
    }

    for (unsigned i = 0; i < n_shares; ++i) {
        my_shares[i].data = dataspace[i];
    }

    for (unsigned i = 0; i < len; ++i) {
        my_secret[i] = gf_random_val<gf_w>();
    }

    if (n_random_ids > 0) {
        if (n_random_ids >= n_shares) {
            n_random_ids = n_shares;
        }
        unsigned i, j;
        for (i = 0; i < n_random_ids; ++i) {
            do {
                id_list[i] = 1 + gf_random_u64() % (generator.field.field_max() - 1);
                for (j = 0; j < i; ++j) {
                    if (id_list[i] == id_list[j]) {
                        break;
                    }
                }
            } while (i != j);
        }
        id_list[i] = 0;
        create_shares (my_secret, id_list);
    } else {
        create_shares (my_secret);
    }
    get_all_valid_shares (my_shares);

    for (unsigned trial = 0; trial < n_trials; ++trial) {
        reset ();
        for (unsigned i = 0; i < n_shares; ++i) {
            used[i] = i;
        }

        for (unsigned i = 0; i < n_min; ++i) {
            x = i + (gf_random_u64() % (n_shares - i));
            cur = used[x];
            used[x] = used[i];
            used[i] = cur;
            set_share (&my_shares[cur]);
        }

        if (! find_secret ()) {
            return trial;
        }

        if (!! memcmp (my_secret, secret(), sizeof (my_secret))) {
            return trial;
        }
        for (unsigned i = 0; i < n_shares; ++i) {
            single_share.id = my_shares[i].id;
            single_share.data = dataspace[n_shares];
            memset (single_share.data, 0, len * sizeof (gf_w));
            if (! get_share (&single_share) ||
                !! memcmp(my_shares[i].data, single_share.data, len * sizeof (gf_w))){
                return trial;
            }
        }
    }

    return n_trials;
}

template struct split_secret<gf_8_t, gf8_shiftadd_state>;
template struct split_secret<gf_16_t, gf16_shiftadd_state>;
template struct split_secret<gf_32_t, gf32_shiftadd_state>;
template struct split_secret<gf_64_t, gf64_shiftadd_state>;

#ifdef  GF_LOG
template struct split_secret<gf_8_t, gf8_log_state>;
template struct split_secret<gf_16_t, gf16_log_state>;
#endif

#ifdef  GF_SSSE3
template struct split_secret<gf_8_t, gf8_ssse3_state>;
template struct split_secret<gf_16_t, gf16_ssse3_state>;
#endif  // GF_SSSE3

#ifdef  GF_CLMUL
template struct split_secret<gf_8_t, gf8_clmul_state>;
template struct split_secret<gf_16_t, gf16_clmul_state>;
template struct split_secret<gf_32_t, gf32_clmul_state>;
template struct split_secret<gf_64_t, gf64_clmul_state>;
#endif

#ifdef  GF_AVX2
template struct split_secret<gf_8_t, gf8_avx2_state>;
template struct split_secret<gf_16_t, gf16_avx2_state>;
#endif  // GF_AVX2

#if  defined(GF_NEON32) || defined(GF_NEON64)
template struct split_secret<gf_8_t, gf8_neon_state>;
template struct split_secret<gf_16_t, gf16_neon_state>;
#endif  // GF_NEON
