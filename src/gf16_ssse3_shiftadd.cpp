/*
 * gf16_ssse3_shiftadd.cpp
 *
 * Routines for 16-bit Galois fields using shift-add under SSSE3.
 *
 */

#include "gf16_ssse3_shiftadd.h"
#include <string.h>
#include <nmmintrin.h>

gf_16_t
gf16_ssse3_shiftadd_state::pow (gf_16_t a, uint32_t b) const {

  if (a == 0) {
    return (a);
  } else if (b == 0) {
    return (gf_16_t)1;
  } else {
    return (antilog ((gf_16_t)(
        ((uint64_t)log(a) * ((uint64_t)b & 0xfffffULL)) 
        % (uint64_t)(GF8_FIELD_SIZE-1))));
  }
}

// see gf8_log for comments
gf_16_t
gf16_ssse3_shiftadd_state::inv (gf_16_t a) const {

  if (a == 0) {
    return (0);
  } else if (a == 1) {
    return (1);
  } else {
    return (antilog_tbl[GF8_FIELD_SIZE-1-log_tbl[a]]);
  }
}


static
inline
void
mul_vec_by_vec_rnd (__m128i & r, __m128i v1, __m128i & v2, __m128i poly, __m128i zero, int pos)
{
    __m128i     t1;

    // Shift mask bit to high-order
    t1 = _mm_slli_epi16 (v1, pos);
    // Add into accumulator (using XOR) if bit in multiplicand is non-zero
    r = _mm_xor_si128 (r, _mm_blend_epi16 (zero, v2, t1));
    // Multiply by 2
    t1 = _mm_blend_epi16 (zero, poly, v2);
    v2 = _mm_xor_si128 (_mm_add_epi16 (v2, v2), t1);
}

static
inline
__m128i
mul_vecs_by_vecs (__m128i v1, __m128i v1,  __m128i v2, __m128i pp)
{
    __m128i     r, zero;

    r = _mm_setzero_si128 ();
    zero = _mm_setzero_si128 ();

    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 15);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 14);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 13);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 12);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 11);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 10);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 9);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 8);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 7);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 6);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 5);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 4);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 3);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 2);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 1);
    // Handle the last round
    r = _mm_xor_si128 (r, _mm_blend_epi16 (zero, v2, v1));

    return r;
}

void
gf16_ssse3_shiftadd_state::mul_region( gf_16_t * const src, gf_16_t * dst, uint64_t bytes,
                                       gf_16_t multiplier, bool accum) const {

    if (multiplier == (gf_16_t)0) {
        if (! accum) {
            memset (dst, 0, bytes);
        }
        return;
    }

    __m128i     mul = _mm_set1_epi16 ((int16_t)multiplier);
    __m128i     pp = _mm_set1_epi16 ((int16_t)prim_poly);
    __m128i     v;
    uint64_t    i;
    uint64_t    chunks = bytes >> 4ULL;

    if (accum) {
        for (i = 0; i < chunks; ++i) {
            // load a 128 bit value, it does not need to be alinged _mm_loadu_si128
            v = _mm_loadu_si128 (((__m128i *)src) + i);
            v = _mm_xor_si128 (_mm_loadu_si128 (((__m128i *)dst) + i),
                               mul_vec_by_vec (v, mul, pp));
            //store the value back into dst
            _mm_storeu_si128 (((__m128i *)dst) + i, v);
        }
    } else {
        for (i = 0; i < chunks; ++i) {
            v = _mm_loadu_si128 (((__m128i *)src) + i);
            v = mul_vec_by_vec (v, mul, pp);
            _mm_storeu_si128 (((__m128i *)dst) + i, v);
        }
    }

    for (i = chunks << 4ULL; i < bytes; i += 2) {
        dst[i] = mul (src[i], multiplier) ^ (accum ? dst[i] : (gf_16_t)0);
    }
}

void
gf16_ssse3_shiftadd_state::mul_region_region (gf_16_t * const buf1, gf_16_t * const buf2,
                                              gf_16_t * dst, uint64_t bytes, bool accum) const
{
    uint64_t    i;
    uint64_t    chunks = bytes >> 4ULL;
    __m128i     v1, v2, r, pp;

    pp = _mm_set1_epi16 ((int16_t)prim_poly);
    if (accum) {
        for (i = 0; i < chunks; ++i) {
            v1 = _mm_loadu_si128 (((__m128i *)buf1) + i);
            v2 = _mm_loadu_si128 (((__m128i *)buf2) + i);
            r = _mm_xor_si128 (_mm_loadu_si128 (((__m128i *)dst) + i),
                               mul_vec_by_vec (v1, v2, pp));
            _mm_storeu_si128 (((__m128i *)dst) + i, r);
        }
        for (i = (chunks * 16); i < bytes; ++i) {
            dst[i] = add (dst[i], mul (buf1[i], buf2[i]));
        }
    } else {
        for (i = 0; i < chunks; ++i) {
            v1 = _mm_loadu_si128 (((__m128i *)buf1) + i);
            v2 = _mm_loadu_si128 (((__m128i *)buf2) + i);
            r = mul_vec_by_vec (v1, v2, pp);
            _mm_storeu_si128 (((__m128i *)dst) + i, r);
        }
        if (!! (bytes & (sizeof (__m128i) - 1))) {
            // Handle the last 16 bytes.  Probably faster to just do 16 using unaligned access
            v1 = _mm_loadu_si128 ((__m128i *)(buf1 + bytes - sizeof (__m128i)));
            v2 = _mm_loadu_si128 ((__m128i *)(buf2 + bytes - sizeof (__m128i)));
            r = mul_vec_by_vec (v1, v2, pp);
            _mm_storeu_si128 ((__m128i *)(buf1 + bytes - sizeof (__m128i)), r);
        }
    }

}
//
// Since we're not using this for much, we want to make it more memory-efficient rather than
// speed-efficient.  We don't precompute the inverse table; we can just look up inverses
// using the log / antilog tables instead.
//
gf16_ssse3_shiftadd_state::gf16_ssse3_shiftadd_state (gf_16_t prim_poly, gf_16_t alpha) {

    uint32_t    b, i;

    this->alpha = alpha;
    this->prim_poly = 0x100 | (uint32_t)prim_poly;

    for (b = this->alpha, i = 0; i < GF16_FIELD_SIZE - 1; i++) {
        log_tbl[b] = (gf_16_t)i;
        antilog_tbl[i] = (gf_16_t)b;
        antilog_tbl[i + GF16_FIELD_SIZE - 1] = (gf_16_t)b;
        b <<= 1;
        if (b & GF16_FIELD_SIZE) {
            b ^= this->prim_poly;
        }
    }
}

// see gf8_log for comments
int
gf16_ssse3_shiftadd_state::sanity_check (int max_stage) const {

    uint32_t    i, j;
    gf_16_t      r, r1, r2;
    int         stage;
    int         counts[field_size()];
    gf_16_t      buf[0x40000];
    gf_16_t      *buf1, *buf2, *buf3;
    uint64_t    bufsize = field_size() * sizeof (gf_16_t);

    /* Check that antilog (log (x)) == x
     * Note that we have to start at 1, since log (0) is undefined.
     */
    if (max_stage >= 1) {
        stage = 1;
        for (i = 1; i < field_size(); i++) {
            r = antilog (log ((gf_16_t)i));
            if (r != (gf_16_t)i) {
                return (stage);
            }
        }
    }

    /* Check that log (antilog (x)) == x
     * Note that we have to end at 65534, since antilog (65535) is undefined.
     */
    if (max_stage >= 2) {
        stage = 2;
        for (i = 0; i < field_size()-1; i++) {
            r = log (antilog ((gf_16_t)i));
            if (r != (gf_16_t)i) {
                return (stage);
            }
        }
    }

    /* Check that inverses really are */
    stage = 3;
    for (i = 1; i < field_size(); i++) {
        r1 = mul (inv ((gf_16_t)i), (gf_16_t)i);
        if (r1 != (gf_16_t)1) {
            return (stage);
        }
    }

    /* Check multiplicative identity */
    stage = 4;
    for (i = 0; i < field_size(); i++) {
        r1 = mul ((gf_16_t)i, (gf_16_t)1);
        if (r1 != (gf_16_t)i) {
            return (stage);
        }
    }

    // Check region multiplication
    stage = 5;
    buf1 = buf;
    buf2 = buf1 + field_size();
    buf3 = buf2 + field_size();
    for (uint32_t multiplier = 0; multiplier < field_size(); multiplier += 1) {
        for (i = 0; i < field_size(); ++i) {
            buf1[i] = gf_random_val<gf_16_t> ();
            buf2[i] = mul (buf1[i], (gf_16_t)multiplier);
            buf3[i] = (gf_16_t)0;
        }
        mul_region (buf1, buf3, bufsize, (gf_16_t)multiplier);
        if (!! memcmp (buf2, buf3, bufsize)) {
            return (stage);
        }
    }

    // Check region-region multiplication
    stage = 6;
    int         rr_size = 8 * field_size ();
    buf1 = buf;
    buf2 = buf1 + rr_size;
    buf3 = buf2 + rr_size;
    for (i = 0; i < rr_size; ++i) {
        buf1[i] = gf_random_val<gf_16_t> ();
        buf2[i] = gf_random_val<gf_16_t> ();
        buf3[i] = (gf_16_t)0;
    }
    mul_region_region (buf1, buf2, buf3, rr_size);
    for (i = 0; i < rr_size; ++i) {
        if (mul(buf1[i], buf2[i]) != buf3[i]) {
            return (stage);
        }
    }

    stage = 7;
    buf1 = buf;
    buf2 = buf1 + field_size();
    buf3 = buf2 + field_size();
    for (uint32_t multiplier = 0; multiplier < field_size(); multiplier += 1) {
        for (i = 0; i < field_size(); ++i) {
            buf1[i] = gf_random_val<gf_16_t> ();
            buf2[i] = mul (buf1[i], (gf_16_t)multiplier) ^ (gf_16_t)((i * 33) % field_size());
            buf3[i] = (gf_16_t)0;
        }
        mul_region (buf1, buf3, bufsize, (gf_16_t)multiplier, true);
        for (i = 0; i < field_size(); ++i) {
            if ((buf2[i] ^ buf3[i]) != (gf_16_t)((i * 33) % field_size())) {
                return (stage);
            }
        }
    }


    /* Check that divide and multiply work */
    if (max_stage >= 50) {
        stage = 50;
        for (i = 1; i < field_size(); i++) {
            for (j = 1; j < field_size(); j++) {
                r1 = div ((gf_16_t)i, (gf_16_t)j);
                r2 = mul ((gf_16_t)i, inv ((gf_16_t)j));
                if (r1 != r2) {
                    return (stage);
                }
            }
        }
    }

    // Check that multiplications produce unique results.
    if (max_stage >= 60) {
        stage = 60;
        for (i = 1; i < field_size(); i++) {
            for (j = 0; j < field_size(); j++) {
                counts[j] = 0;
            }
            for (j = 0; j < field_size(); j++) {
                r1 = mul ((gf_16_t)i, (gf_16_t)j);
                counts[r1] += 1;
                r2 = mul ((gf_16_t)j, (gf_16_t)i);
                if (r1 != r2) {
                    return (stage);
                }
            }
            for (j = 0; j < field_size(); j++) {
                if (counts[j] != 1) {
                    return (stage);
                }
            }
        }
    }
    return (0);
}

