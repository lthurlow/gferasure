/*
 * gf8_ssse3.cpp
 *
 * Routines for 8-bit Galois fields using SSSE3 tables.
 * for additional comments for non-SIMD instructions review gf8_log.cpp comments
 *
 */

#include "gf8_ssse3.h"
#include <string.h>
#include <nmmintrin.h> //for _mm_blendv_epi8

/*
 * Sub-function to mul_vec_by_vec, for each round, multiply
*/
static
inline
void
mul_vec_by_vec_rnd (__m128i & r, __m128i v1, __m128i & v2, __m128i poly, __m128i zero, int pos)
{
    __m128i     t1;

    // Shift mask bit to high-order
    t1 = _mm_slli_epi32 (v1, pos);
    // Add into accumulator (using XOR) if bit in multiplicand is non-zero
    r = _mm_xor_si128 (r, _mm_blendv_epi8 (zero, v2, t1));
    // Multiply by 2
    t1 = _mm_blendv_epi8 (zero, poly, v2);
    v2 = _mm_xor_si128 (_mm_add_epi8 (v2, v2), t1);
}

/*
 * Multiply two vectors by eachother
*/

static
inline
__m128i
mul_vec_by_vec (__m128i v1, __m128i v2, __m128i pp)
{
    __m128i     r, zero;

    r = _mm_setzero_si128 ();
    zero = _mm_setzero_si128 ();

    for (unsigned i = 7; i > 0; --i) {
        mul_vec_by_vec_rnd (r, v1, v2, pp, zero, i);
    }
    // Handle the last round
    r = _mm_xor_si128 (r, _mm_blendv_epi8 (zero, v2, v1));

    return r;
}

/*
 * Set up the multiplication table.
*/

static
inline
void
set_up_htable (gf_8_t m, uint32_t pp,  __m128i & htbl, __m128i & ltbl) {

    // set 2 64 bit values _mm_set_epi64x(__int64 b, __int64 a)
    __m128i     lv = _mm_set_epi64x (0x0f0e0d0c0b0a0908LL, 0x0706050403020100LL);
    // shifts the 2 64 bit values by count _mm_slli_epi64(__m128i a, int count)
    __m128i     hv = _mm_slli_epi64 (lv, 4);
    // set 16 signed 8 bit integers _mm_set1_epi8(char b)
    // so this will but the low bits of the prim poly in poly
    __m128i     poly = _mm_set1_epi8 ((int8_t) (pp & 0xff));
    // sets the 128 bits to zero _mm_setzero_si128()
    __m128i     zero = _mm_setzero_si128 ();
    uint64_t    mul = (uint64_t)m * 0x0101010101010101ULL;
    __m128i     t1;


    // __builtin_clz works on 32 bit integers, but we're only using 8 bits!
    int         left_bit = __builtin_clz ((uint32_t)m) - 24;
    int         i;

    //set both inputs to be 0
    ltbl = _mm_setzero_si128 ();
    htbl = _mm_setzero_si128 ();

    for (i = 7; i > left_bit; --i) {
        // Shift mask bit to high-order for each 8-bit value
        t1 = _mm_set1_epi64x (mul << (uint64_t)i);
        // Add into accumulator (using XOR) if bit in multiplicand is non-zero
        ltbl = _mm_xor_si128 (ltbl, _mm_blendv_epi8 (zero, lv, t1));
        htbl = _mm_xor_si128 (htbl, _mm_blendv_epi8 (zero, hv, t1));
        // Multiply lv and hv by 2 in the Galois field */
        // _mm_blendv_epi8 Selects integer bytes from two using variable mask

        // multiply by 2
        t1 = _mm_blendv_epi8 (zero, poly, lv);
        lv = _mm_xor_si128 (_mm_add_epi8 (lv, lv), t1);
        // multiply by 2
        t1 = _mm_blendv_epi8 (zero, poly, hv);
        hv = _mm_xor_si128 (_mm_add_epi8 (hv, hv), t1);
    }
    // Take care of adding in the last value
    // Shift mask bit to high-order for each 8-bit value
    t1 = _mm_set1_epi64x (mul << (uint64_t)i);
    ltbl = _mm_xor_si128 (ltbl, _mm_blendv_epi8 (zero, lv, t1));
    htbl = _mm_xor_si128 (htbl, _mm_blendv_epi8 (zero, hv, t1));
}

/*
 * Multiplication for a vector by a constant.
*/

static
inline 
__m128i gf8_mult_const_16(__m128i v, __m128i htbl, __m128i ltbl, __m128i loset) {

  __m128i r_lo, r_hi, hi, lo;
  // compute the bitwise and between 2 128b values _mm_and_si128
  lo = _mm_and_si128(loset, v);
  // shift right _mm_srli_epi64, then and with loset
  hi = _mm_and_si128(loset, _mm_srli_epi64(v, 4));
  // shuffle the contects of ltbl according to lo _mm_shuffle_epi8
  r_lo = _mm_shuffle_epi8(ltbl, lo);
  r_hi = _mm_shuffle_epi8(htbl, hi);
  //return xor of lo and hi bit vectors
  return (_mm_xor_si128(r_lo, r_hi));
}


/*
 * Multiply Region is used to multiply a region (set of values) by a single
 * multiplier.  The result is then stored the second memmory region.
 * For testing the 128 bit version, change v to a 128 bit vector.
 * For testing the other multiplication methods, comment out _alt, and
 * uncomment the desired method.
*/

void
gf8_ssse3_state::mul_region (const gf_8_t * src, gf_8_t * dst, uint64_t bytes,
                             gf_8_t multiplier, bool accum) const
{

  if (multiplier == (gf_8_t)0) {
    if (! accum) {
      memset (dst, 0, bytes);
    }
    return;
  }
  else if (multiplier == (gf_8_t)1) {
      if (! accum){
        memcpy (dst, src, bytes);
      }   
      else {
        uint64_t    chunks = bytes >> 4ULL;
        for (uint64_t i = 0; i < chunks; i++) {
          __m128i x = _mm_loadu_si128 (((__m128i *)src) + i);
          __m128i y = _mm_loadu_si128 (((__m128i *)dst) + i);
          _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128(x,y));
        }
      }
      return;
  }   



  __m128i     htbl, ltbl, v;

  uint64_t    i;
  uint64_t    chunks = bytes >> 4ULL;

  set_up_htable (multiplier, prim_poly, htbl, ltbl);

  __m128i     loset = _mm_set1_epi8(0x0f);


  if (accum) {
    for (i = 0; i < chunks; ++i) {
      // load a 128 bit value, it does not need to be alinged _mm_loadu_si128
      v = _mm_loadu_si128 (((__m128i *)src) + i);
      v = _mm_xor_si128 (_mm_loadu_si128 (((__m128i *)dst) + i),
                         gf8_mult_const_16 (v, htbl, ltbl, loset));
      //store the value back into dst
      _mm_storeu_si128 (((__m128i *)dst) + i, v);
    }
  } else {
    for (i = 0; i < chunks; ++i) {
      v = _mm_loadu_si128 (((__m128i *)src) + i);
      v = gf8_mult_const_16(v, htbl, ltbl, loset);
      _mm_storeu_si128 (((__m128i *)dst) + i, v);
    }
  }

  for (i = chunks << 4ULL; i < bytes; ++i) {
    dst[i] = mul (src[i], multiplier) ^ (accum ? dst[i] : (gf_8_t)0);
  }
}


/*
 * Multiplying one region by another, takes in the two regions to multiply each other
 * by and stores the result in the third memmory buffer.
*/

void
gf8_ssse3_state::mul_region_region (const gf_8_t * buf1, const gf_8_t * buf2, gf_8_t * dst,
                                    uint64_t bytes, bool accum) const
{
    uint64_t    i;
    uint64_t    chunks = bytes >> 4ULL;
    __m128i     v1, v2, r, pp;

    pp = _mm_set1_epi8 ((int8_t)prim_poly);
    if (accum) {
        for (i = 0; i < chunks; ++i) {
            v1 = _mm_loadu_si128 (((__m128i *)buf1) + i);
            v2 = _mm_loadu_si128 (((__m128i *)buf2) + i);
            r = _mm_xor_si128 (_mm_loadu_si128 (((__m128i *)dst) + i),
                                                mul_vec_by_vec (v1, v2, pp));
            _mm_storeu_si128 (((__m128i *)dst) + i, r);
        }
        for (i = (chunks * 16); i < bytes; ++i) {
            dst[i] = add (dst[i], mul (buf1[i], buf2[i]));
        }
    } else {
        for (i = 0; i < chunks; ++i) {
            v1 = _mm_loadu_si128 (((__m128i *)buf1) + i);
            v2 = _mm_loadu_si128 (((__m128i *)buf2) + i);
            r = mul_vec_by_vec (v1, v2, pp);
            _mm_storeu_si128 (((__m128i *)dst) + i, r);
        }
        if (!! (bytes & (sizeof (__m128i) - 1))) {
            // Handle the last 16 bytes.  Probably faster to just do 16 using unaligned access
            v1 = _mm_loadu_si128 ((__m128i *)(buf1 + bytes - sizeof (__m128i)));
            v2 = _mm_loadu_si128 ((__m128i *)(buf2 + bytes - sizeof (__m128i)));
            r = mul_vec_by_vec (v1, v2, pp);
            _mm_storeu_si128 ((__m128i *)(dst + bytes - sizeof (__m128i)), r);
        }
    }

}

gf_8_t
gf8_ssse3_state::dotproduct (const gf_8_t * src1, const gf_8_t * src2,
                             uint64_t n_elements, uint64_t stride) const
{
    gf_8_t      result = (gf_8_t)0;
    uint64_t    i;
    uint64_t    chunks;
    __m128i     r;
    __m128i     accumulator = _mm_setzero_si128 ();
    __m128i     pp = _mm_set1_epi8 ((int8_t)prim_poly);
//take out if statement, chunks=n_elements>>(5-stride)??
    if (stride == 1) {
        chunks = n_elements >> 4ULL;
        for (i = 0; i < chunks; ++i) {
            r = mul_vec_by_vec (_mm_loadu_si128 ((__m128i *)src1 + i),
                                _mm_loadu_si128 ((__m128i *)src2 + i), pp);
            accumulator = _mm_xor_si128 (r, accumulator);
        }
        for (i = chunks * sizeof (__m128i); i < n_elements; ++i) {
            result ^= mul (src1[i], src2[i]);
        }
        result ^= _mm_extract_epi8 (accumulator, 0); 
        result ^= _mm_extract_epi8 (accumulator, 1); 
        result ^= _mm_extract_epi8 (accumulator, 2); 
        result ^= _mm_extract_epi8 (accumulator, 3); 
        result ^= _mm_extract_epi8 (accumulator, 4); 
        result ^= _mm_extract_epi8 (accumulator, 5); 
        result ^= _mm_extract_epi8 (accumulator, 6); 
        result ^= _mm_extract_epi8 (accumulator, 7); 
        result ^= _mm_extract_epi8 (accumulator, 8); 
        result ^= _mm_extract_epi8 (accumulator, 9); 
        result ^= _mm_extract_epi8 (accumulator, 10);
        result ^= _mm_extract_epi8 (accumulator, 11);
        result ^= _mm_extract_epi8 (accumulator, 12);
        result ^= _mm_extract_epi8 (accumulator, 13);
        result ^= _mm_extract_epi8 (accumulator, 14);
        result ^= _mm_extract_epi8 (accumulator, 15);
    } else if (stride == 2) {
        uint64_t        chunks = n_elements >> 3ULL;
/*
        __m128i         r;
        __m128i         accumulator = _mm_setzero_si128 ();
        __m128i         pp = _mm_set1_epi8 ((int8_t)prim_poly);
*/
        for (i = 0; i < chunks; ++i) {
            r = mul_vec_by_vec (_mm_loadu_si128 ((__m128i *)src1 + i),
                                _mm_loadu_si128 ((__m128i *)src2 + i), pp);
            accumulator = _mm_xor_si128 (r, accumulator);
        }
        for (i = chunks * sizeof (__m128i); i < n_elements; ++i) {
            result ^= mul (src1[i], src2[i]);
        }
        result ^= _mm_extract_epi8 (accumulator, 0); 
        result ^= _mm_extract_epi8 (accumulator, 1); 
        result ^= _mm_extract_epi8 (accumulator, 2); 
        result ^= _mm_extract_epi8 (accumulator, 3); 
        result ^= _mm_extract_epi8 (accumulator, 4); 
        result ^= _mm_extract_epi8 (accumulator, 5); 
        result ^= _mm_extract_epi8 (accumulator, 6); 
        result ^= _mm_extract_epi8 (accumulator, 7); 
        result ^= _mm_extract_epi8 (accumulator, 8); 
        result ^= _mm_extract_epi8 (accumulator, 9); 
        result ^= _mm_extract_epi8 (accumulator, 10);
        result ^= _mm_extract_epi8 (accumulator, 11);
        result ^= _mm_extract_epi8 (accumulator, 12);
        result ^= _mm_extract_epi8 (accumulator, 13);
        result ^= _mm_extract_epi8 (accumulator, 14);
        result ^= _mm_extract_epi8 (accumulator, 15);
    }

    return result;
}

inline
__m128i
multiply_vec_by_2 (__m128i v, __m128i reducer, __m128i mask)
{
    return _mm_xor_si128 (_mm_shuffle_epi8 (reducer, _mm_srli_epi64 (_mm_and_si128 (v, mask), 7)),
                          _mm_add_epi8 (v, v));
}
