/*
 * gf16_ssse3.cpp
 *
 * Routines for 16-bit Galois fields using SSSE3 tables.
 * Additional comments can be found in gf16_log.cpp for non-simd functions
 * 
 */

#include "gf16_ssse3.h"
#include <string.h>
#include <immintrin.h>
#include <nmmintrin.h> //for _mm_blendv_epi8

/*
 * Custom 16 bit blend function (not present in intel).
 * takes the same 8 bit region twice to construct a 16 bit region.
 * these 16 bit regions, will then both choose yes or no in the blendv
 * to select the correct 16 bit element.
*/
static
inline
__m128i 
gf16_blend_mask(__m128i l, __m128i mask){
  __m128i temp = _mm_srai_epi16(mask, 15);
  return(_mm_and_si128(temp,l));
}


/*
 * This is the other type of table, where 0000-0007 | 0000-0070 |
 * so that values < 7 are in lv, values higher are in hv.  The main
 * difference with both these table methods is how you operate over the data
*/
static
inline
void 
set_up_htable_0007(gf_16_t m, uint32_t prim_poly, __m128i * htbl, __m128i * ltbl) {

  __m128i lv[4];
  __m128i hv[4];
  lv[0] = _mm_set_epi64x (0x0007000600050004LL, 0x0003000200010000LL);
  lv[1] = _mm_set_epi64x (0x0070006000500040LL, 0x0030002000100000LL);
  lv[2] = _mm_set_epi64x (0x0700060005000400LL, 0x0300020001000000LL);
  lv[3] = _mm_set_epi64x (0x7000600050004000LL, 0x3000200010000000LL);

  hv[0] = _mm_set_epi64x (0x000f000e000d000cLL, 0x000b000a00090008LL);
  hv[1] = _mm_set_epi64x (0x00f000e000d000c0LL, 0x00b000a000900080LL);
  hv[2] = _mm_set_epi64x (0x0f000e000d000c00LL, 0x0b000a0009000800LL);
  hv[3] = _mm_set_epi64x (0xf000e000d000c000LL, 0xb000a00090008000LL);

  int left_bit = __builtin_clz ((uint32_t)m) - 16;
  int i;

  __m128i     poly = _mm_set1_epi16((int32_t)(prim_poly & 0xffff));
  uint64_t    mul = (uint64_t)m * 0x0001000100010001ULL;
  __m128i     t1;

  for (i = 15; i >= left_bit; --i) {
    t1 = _mm_set1_epi64x (mul << (uint64_t)i);
    (ltbl)[0] = _mm_xor_si128((ltbl)[0], gf16_blend_mask(lv[0], t1));
    (htbl)[0] = _mm_xor_si128((htbl)[0], gf16_blend_mask(hv[0], t1));
    t1 = gf16_blend_mask(poly, lv[0]);
    lv[0] = _mm_xor_si128(_mm_add_epi16(lv[0], lv[0]), t1);
    t1 = gf16_blend_mask(poly, hv[0]);
    hv[0] = _mm_xor_si128(_mm_add_epi16(hv[0], hv[0]), t1);

    t1 = _mm_set1_epi64x (mul << (uint64_t)i);
    (ltbl)[1] = _mm_xor_si128((ltbl)[1], gf16_blend_mask(lv[1], t1));
    (htbl)[1] = _mm_xor_si128((htbl)[1], gf16_blend_mask(hv[1], t1));
    t1 = gf16_blend_mask(poly, lv[1]);
    lv[1] = _mm_xor_si128(_mm_add_epi16(lv[1], lv[1]), t1);
    t1 = gf16_blend_mask(poly, hv[1]);
    hv[1] = _mm_xor_si128(_mm_add_epi16(hv[1], hv[1]), t1);

    t1 = _mm_set1_epi64x (mul << (uint64_t)i);
    (ltbl)[2] = _mm_xor_si128((ltbl)[2], gf16_blend_mask(lv[2], t1));
    (htbl)[2] = _mm_xor_si128((htbl)[2], gf16_blend_mask(hv[2], t1));
    t1 = gf16_blend_mask(poly, lv[2]);
    lv[2] = _mm_xor_si128(_mm_add_epi16(lv[2], lv[2]), t1);
    t1 = gf16_blend_mask(poly, hv[2]);
    hv[2] = _mm_xor_si128(_mm_add_epi16(hv[2], hv[2]), t1);

    t1 = _mm_set1_epi64x (mul << (uint64_t)i);
    (ltbl)[3] = _mm_xor_si128((ltbl)[3], gf16_blend_mask(lv[3], t1));
    (htbl)[3] = _mm_xor_si128((htbl)[3], gf16_blend_mask(hv[3], t1));
    t1 = gf16_blend_mask(poly, lv[3]);
    lv[3] = _mm_xor_si128(_mm_add_epi16(lv[3], lv[3]), t1);
    t1 = gf16_blend_mask(poly, hv[3]);
    hv[3] = _mm_xor_si128(_mm_add_epi16(hv[3], hv[3]), t1);

  }
}

/*
 * This is the altmap function originally published in:
 * Screaming Fast Galois Field Arithmetic Using Intel SIMD Instructions
 * The alternate mapping is a much more efficient table for multiplication
 * because it fits better with the intel instructions for blendv and shuffle_epi8
 * additionally it fully utiliziates shuffle_epi8 for 16 lookups simutaneously
*/

static
inline
void
to_altmap(__m128i & one, __m128i & two){
  __m128i mask = _mm_set1_epi16(0xff);
  __m128i ab_h = _mm_srli_epi16(one,8);
  __m128i ab_l = _mm_and_si128(one,mask);
  __m128i cd_h = _mm_srli_epi16(two,8);
  __m128i cd_l = _mm_and_si128(two,mask);
  one = _mm_packus_epi16(cd_h,ab_h);
  two = _mm_packus_epi16(cd_l,ab_l);
}

/*
 * This function will just call to_alt map 4 times for both the high and low tables
*/

static
inline
void
to_altmap_4(__m128i *one, __m128i *two){
  to_altmap(one[0],two[0]);
  to_altmap(one[1],two[1]);
  to_altmap(one[2],two[2]);
  to_altmap(one[3],two[3]);
}


/*
static
inline
void
to_stdmap(__m128i & high, __m128i & low){
  __m128i temp = _mm_unpackhi_epi8(low, high);
  low = _mm_unpacklo_epi8(low,high);
  high = temp;
}

static
inline
void
to_stdmap_4(__m128i *one, __m128i *two){
  to_stdmap(one[0],two[0]);
  to_stdmap(one[1],two[1]);
  to_stdmap(one[2],two[2]);
  to_stdmap(one[3],two[3]);
}
*/


/*
 * This is the Alternate mapping multiply, compared to the gf16 multiplies above
 * it is much more compact, uses less instructions.  There is also no need for
 * a blendv because of how the data is setup, the shuffle is operating over only
 * the data needed,
*/

static
inline 
void 
gf16_mult_alt(__m128i & v0, __m128i & v1, __m128i *htbl, __m128i *ltbl, __m128i loset) {
  //altmap
  __m128i mask = _mm_set1_epi16(0xff);
  __m128i ab_h = _mm_srli_epi16(v0,8);
  __m128i ab_l = _mm_and_si128(v0,mask);
  __m128i cd_h = _mm_srli_epi16(v1,8);
  __m128i cd_l = _mm_and_si128(v1,mask);
  v0 = _mm_packus_epi16(cd_h,ab_h);
  v1 = _mm_packus_epi16(cd_l,ab_l);

  __m128i bit_loc = _mm_and_si128(loset, v0);
  __m128i res_l = _mm_shuffle_epi8(ltbl[2],bit_loc);
  __m128i res_h = _mm_shuffle_epi8(htbl[2],bit_loc);

  bit_loc = _mm_and_si128(loset, _mm_srli_epi64(v0,4));
  res_l = _mm_xor_si128(res_l, _mm_shuffle_epi8(ltbl[3],bit_loc));
  res_h = _mm_xor_si128(res_h, _mm_shuffle_epi8(htbl[3],bit_loc));

  bit_loc = _mm_and_si128(loset,v1);
  res_l = _mm_xor_si128(res_l, _mm_shuffle_epi8(ltbl[0],bit_loc));
  res_h = _mm_xor_si128(res_h, _mm_shuffle_epi8(htbl[0],bit_loc));

  bit_loc = _mm_and_si128(loset, _mm_srli_epi64(v1,4));
  res_l = _mm_xor_si128(res_l, _mm_shuffle_epi8(ltbl[1],bit_loc));
  res_h = _mm_xor_si128(res_h, _mm_shuffle_epi8(htbl[1],bit_loc));

  v0=_mm_unpackhi_epi8(res_l, res_h);
  v1=_mm_unpacklo_epi8(res_l, res_h);
}

/*
 * Multiply Region function takes in two regions of memmory, the first region
 * is the multiplicand, to be be multiplied by the multiplier and stored in the 
 * second memmory region.  This function is to be used for multiplying multiple
 * variables by a single multiplier.  In particular this function uses the 0007 
 * htable and alternate mapping, to change to normal mapping, comment out the 
 * to_altmap function as well as the mult_alt, then uncomment out the mult_const
*/



void
gf16_ssse3_state::mul_region (const gf_16_t * src, gf_16_t * dst, uint64_t bytes,
                              gf_16_t multiplier, bool accum) const {

  if (multiplier == (gf_16_t)0) {
    if (! accum) {
      memset (dst, 0, bytes);
    }
    return;
  }
  else if (multiplier == (gf_16_t)1) {
    if (! accum){
      memcpy (dst, src, bytes);
    }   
    else {
      uint64_t    chunks = bytes >> 4ULL;
      for (uint64_t i = 0; i < chunks; i++) {
        __m128i x = _mm_loadu_si128 (((__m128i *)src) + i);
        __m128i y = _mm_loadu_si128 (((__m128i *)dst) + i);
        _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128(x,y));
      }
    }
    return;
  }   


  uint64_t    i;
  uint64_t    chunks = bytes >> 4ULL;
  __m128i loset = _mm_set1_epi8(0x0f);
  __m128i v0,v1;
  __m128i htbl[4];
  __m128i ltbl[4];

  htbl[0] = _mm_setzero_si128();
  ltbl[0] = _mm_setzero_si128();
  htbl[1] = _mm_setzero_si128();
  ltbl[1] = _mm_setzero_si128();
  htbl[2] = _mm_setzero_si128();
  ltbl[2] = _mm_setzero_si128();
  htbl[3] = _mm_setzero_si128();
  ltbl[3] = _mm_setzero_si128();


  set_up_htable_0007(multiplier, prim_poly, htbl, ltbl);
  to_altmap_4(htbl, ltbl);


  // if the number of chunks is odd
  if(chunks&1){
    --chunks;
  }

  // going to multiply 256 bits at a time
  if (accum) {
    for (i = 0; i < chunks; i+=2) {
      // load a 128 bit value, it does not need to be alinged _mm_loadu_si128
      v0 = _mm_loadu_si128 (((__m128i *)src) + i);
      v1 = _mm_loadu_si128 (((__m128i *)src) + (i+1));

      gf16_mult_alt(v0, v1, htbl, ltbl, loset);
      //accumulate into dst
      v0 = _mm_xor_si128 (_mm_loadu_si128 (((__m128i *)dst) +i), v0);
      v1 = _mm_xor_si128 (_mm_loadu_si128 (((__m128i *)dst) +(i+1)), v1);

      _mm_storeu_si128 (((__m128i *)dst) + i, v0);
      _mm_storeu_si128 (((__m128i *)dst) + (i+1), v1);

    }
  } else {
    for (i = 0; i < chunks; i+=2) {
       v0 = _mm_loadu_si128 (((__m128i *)src) + i);
       v1 = _mm_loadu_si128 (((__m128i *)src) + (i+1));

       gf16_mult_alt(v0, v1, htbl, ltbl,loset);

      _mm_storeu_si128 (((__m128i *)dst) + i, v0);
      _mm_storeu_si128(((__m128i *) dst)+(i+1), v1);
    }
  }

  for (i = chunks << 3ULL; i < bytes/2; ++i) {
    dst[i] = mul (src[i], multiplier) ^ (accum ? dst[i] : (gf_16_t)0);
  }
}

static
inline
void
mul_vec_by_vec_rnd (__m128i & r, __m128i v1, __m128i & v2, __m128i poly,int pos)
{
    __m128i     t1;

    // Shift mask bit to high-order
    t1 = _mm_slli_epi32 (v1, pos);
    // Add into accumulator (using XOR) if bit in multiplicand is non-zero
    r = _mm_xor_si128(r, gf16_blend_mask(v2, t1));
    // Multiply by 2
    t1 = gf16_blend_mask(poly, v2);
    v2 = _mm_xor_si128 (_mm_add_epi16(v2, v2), t1);
}

static
inline
__m128i
mul_vec_by_vec (__m128i v1, __m128i v2, __m128i pp)
{
    __m128i     r;

    r = _mm_setzero_si128 ();

    for (unsigned i = 15; i > 0; --i) {
        mul_vec_by_vec_rnd (r, v1, v2, pp, i);
    }
    // Handle the last round
    r = _mm_xor_si128 (r, gf16_blend_mask(v2, v1));

    return r;
}

/*
 * Multiply region by region function is used for multiplying two regions by each
 * other and storing the result in a third region.
*/

void
gf16_ssse3_state::mul_region_region (const gf_16_t * buf1, const gf_16_t * buf2, gf_16_t * dst,
                                    uint64_t bytes, bool accum) const
{
    uint64_t    i;
    uint64_t    chunks = bytes >> 4ULL;

    __m128i     v1, v2, r, pp;

    //pp = _mm_set1_epi8 ((int8_t)prim_poly);
    pp = _mm_set1_epi16((int32_t)(prim_poly & 0xffff));
    if (accum) {
        for (i = 0; i < chunks; ++i) {
            v1 = _mm_loadu_si128 (((__m128i *)buf1) + i);
            v2 = _mm_loadu_si128 (((__m128i *)buf2) + i);
            r = _mm_xor_si128 (_mm_loadu_si128 (((__m128i *)dst) + i),
                                                mul_vec_by_vec (v1, v2, pp));
            _mm_storeu_si128 (((__m128i *)dst) + i, r);
        }
        for (i = (chunks * 8); i < bytes/2; ++i) {
            dst[i] = add (dst[i], mul (buf1[i], buf2[i]));
        }
    } else {
        for (i = 0; i < chunks; ++i) {
            v1 = _mm_loadu_si128 (((__m128i *)buf1) + i);
            v2 = _mm_loadu_si128 (((__m128i *)buf2) + i);
            r = mul_vec_by_vec (v1, v2, pp);
            _mm_storeu_si128 (((__m128i *)dst) + i, r);
        }
        for (i = (chunks * 8); i < bytes/2; ++i) {
            dst[i] =  mul (buf1[i], buf2[i]);
        }

    }

}

/*
 * Dot product also multipliest two regions, however unlike multiply region
 * the result is a single GF16, as dot product will multiple each of the
 * two regions together and add the products together.
*/

gf_16_t
gf16_ssse3_state::dotproduct (const gf_16_t * src1, const gf_16_t * src2,
                              uint64_t n_elements, uint64_t stride) const
{
    gf_16_t     result = (gf_16_t)0;
    uint64_t    i;
    uint64_t    chunks;
    __m128i     r;
    __m128i     accumulator = _mm_setzero_si128 ();
    __m128i     pp = _mm_set1_epi16 ((uint32_t)prim_poly);

    if (stride == 1) {
        chunks = n_elements >> 3ULL;
        for (i = 0; i < chunks; ++i) {
            r = mul_vec_by_vec (_mm_loadu_si128 ((__m128i *)src1 + i),
                                _mm_loadu_si128 ((__m128i *)src2 + i), pp);
            accumulator = _mm_xor_si128 (r, accumulator);
        }
        for (i = chunks * 8; i < n_elements; ++i) {
            result ^= mul (src1[i], src2[i]);
        }
        result ^= _mm_extract_epi16 (accumulator, 0); 
        result ^= _mm_extract_epi16 (accumulator, 1); 
        result ^= _mm_extract_epi16 (accumulator, 2); 
        result ^= _mm_extract_epi16 (accumulator, 3); 
        result ^= _mm_extract_epi16 (accumulator, 4); 
        result ^= _mm_extract_epi16 (accumulator, 5); 
        result ^= _mm_extract_epi16 (accumulator, 6); 
        result ^= _mm_extract_epi16 (accumulator, 7); 
    } else if (stride == 2) {
        uint64_t        chunks = n_elements >> 2ULL;
        __m128i         r;
        __m128i         accumulator = _mm_setzero_si128 ();
        __m128i         pp = _mm_set1_epi16 ((uint32_t)prim_poly);

        for (i = 0; i < chunks; ++i) {
            r = mul_vec_by_vec (_mm_loadu_si128 ((__m128i *)src1 + i),
                                _mm_loadu_si128 ((__m128i *)src2 + i), pp);
            accumulator = _mm_xor_si128 (r, accumulator);
        }
        for (i = chunks * sizeof (__m128i); i < n_elements; ++i) {
            result ^= mul (src1[i], src2[i]);
        }

        result ^= _mm_extract_epi16 (accumulator, 0); 
        result ^= _mm_extract_epi16 (accumulator, 1); 
        result ^= _mm_extract_epi16 (accumulator, 2); 
        result ^= _mm_extract_epi16 (accumulator, 3); 
        result ^= _mm_extract_epi16 (accumulator, 4); 
        result ^= _mm_extract_epi16 (accumulator, 5); 
        result ^= _mm_extract_epi16 (accumulator, 6); 
        result ^= _mm_extract_epi16 (accumulator, 7); 
    }

    return result;
}


//FIXME: not modified for gf16
inline
__m128i
multiply_vec_by_2 (__m128i v, __m128i reducer, __m128i mask)
{
    return _mm_xor_si128 (_mm_shuffle_epi8 (reducer, _mm_srli_epi64 (_mm_and_si128 (v, mask), 7)),
                          _mm_add_epi8 (v, v));
}
