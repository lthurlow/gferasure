// Fields instantiated for gf_matrix.
//
// For each field, this includes both the gf_matrix instance and the default field.
//

// Always instantiate shiftadd fields
template struct gf_matrix<gf_8_t, gf8_shiftadd_state>;
template struct gf_matrix<gf_16_t, gf16_shiftadd_state>;
template struct gf_matrix<gf_32_t, gf32_shiftadd_state>;
template struct gf_matrix<gf_64_t, gf64_shiftadd_state>;
template<> gf_w_state<gf_8_t,  gf8_shiftadd_state> gf_matrix<gf_8_t,gf8_shiftadd_state>::default_field((gf_8_t)0x1d, 1);
template<> gf_w_state<gf_16_t, gf16_shiftadd_state> gf_matrix<gf_16_t,gf16_shiftadd_state>::default_field((gf_16_t)0x100b, 1);
template<> gf_w_state<gf_32_t, gf32_shiftadd_state> gf_matrix<gf_32_t,gf32_shiftadd_state>::default_field((gf_32_t)0x000000c5, 1);
template<> gf_w_state<gf_64_t, gf64_shiftadd_state> gf_matrix<gf_64_t,gf64_shiftadd_state>::default_field((gf_64_t)0x000000000000001b, 1);

#ifdef  GF_LOG
template<> gf_w_state<gf_8_t,  gf8_log_state> gf_matrix<gf_8_t,gf8_log_state>::default_field((gf_8_t)0x1d, (gf_8_t)1);
template<> gf_w_state<gf_16_t, gf16_log_state> gf_matrix<gf_16_t,gf16_log_state>::default_field((gf_16_t)0x100b, (gf_16_t)1);
template struct gf_matrix<gf_8_t, gf8_log_state>;
template struct gf_matrix<gf_16_t, gf16_log_state>;
#endif  // GF_LOG

#ifdef  GF_SSSE3
template struct gf_matrix<gf_8_t, gf8_ssse3_state>;
template struct gf_matrix<gf_16_t, gf16_ssse3_state>;
template<> gf_w_state<gf_8_t, gf8_ssse3_state> gf_matrix<gf_8_t,gf8_ssse3_state>::default_field((gf_8_t)0x1d, (gf_8_t)1);
template<> gf_w_state <gf_16_t,gf16_ssse3_state> gf_matrix<gf_16_t,gf16_ssse3_state>::default_field((gf_16_t)0x100b, (gf_16_t)1);
#endif  // GF_SSSE3

#ifdef  GF_AVX2
template struct gf_matrix<gf_8_t, gf8_avx2_state>;
template struct gf_matrix<gf_16_t, gf16_avx2_state>;
template struct gf_matrix<gf_8_t, gf8_gather_state>;
template struct gf_matrix<gf_16_t, gf16_gather_state>;
template<> gf_w_state<gf_8_t, gf8_avx2_state> gf_matrix<gf_8_t,gf8_avx2_state>::default_field((gf_8_t)0x1d, (gf_8_t)1);
template<> gf_w_state <gf_16_t,gf16_avx2_state> gf_matrix<gf_16_t,gf16_avx2_state>::default_field((gf_16_t)0x100b, (gf_16_t)1);
template<> gf_w_state<gf_8_t, gf8_gather_state> gf_matrix<gf_8_t,gf8_gather_state>::default_field((gf_8_t)0x1d, (gf_8_t)1);
template<> gf_w_state <gf_16_t,gf16_gather_state> gf_matrix<gf_16_t,gf16_gather_state>::default_field((gf_16_t)0x100b, (gf_16_t)1);
#endif  // GF_AVX2

#ifdef  GF_CLMUL
template struct gf_matrix<gf_8_t, gf8_clmul_state>;
template struct gf_matrix<gf_16_t, gf16_clmul_state>;
template struct gf_matrix<gf_32_t, gf32_clmul_state>;
template struct gf_matrix<gf_64_t, gf64_clmul_state>;
template<> gf_w_state<gf_8_t, gf8_clmul_state> gf_matrix<gf_8_t,gf8_clmul_state>::default_field((gf_8_t)0x1d, 1);
template<> gf_w_state<gf_16_t, gf16_clmul_state> gf_matrix<gf_16_t,gf16_clmul_state>::default_field((gf_16_t)0x002d, 1);
template<> gf_w_state<gf_32_t, gf32_clmul_state> gf_matrix<gf_32_t,gf32_clmul_state>::default_field((gf_32_t)0x000000c5, 1);
template<> gf_w_state<gf_64_t, gf64_clmul_state> gf_matrix<gf_64_t,gf64_clmul_state>::default_field((gf_64_t)0x000000000000001b, 1);
#endif  // GF_CLMUL

#if defined(GF_NEON32) || defined(GF_NEON64)
template struct gf_matrix<gf_8_t,  gf8_neon_state>;
template struct gf_matrix<gf_16_t, gf16_neon_state>;
template struct gf_matrix<gf_8_t,  gf8_vmull_state>;
template struct gf_matrix<gf_16_t, gf16_vmull_state>;
template struct gf_matrix<gf_32_t, gf32_vmull_state>;
template struct gf_matrix<gf_64_t, gf64_vmull_state>;
template<> gf_w_state<gf_8_t, gf8_neon_state> gf_matrix<gf_8_t,gf8_neon_state>::default_field((gf_8_t)0x1d, (gf_8_t)1);
template<> gf_w_state <gf_16_t,gf16_neon_state> gf_matrix<gf_16_t,gf16_neon_state>::default_field((gf_16_t)0x100b, (gf_16_t)1);
template<> gf_w_state<gf_8_t, gf8_vmull_state> gf_matrix<gf_8_t,gf8_vmull_state>::default_field((gf_8_t)0x1d, 1);
template<> gf_w_state<gf_16_t, gf16_vmull_state> gf_matrix<gf_16_t,gf16_vmull_state>::default_field((gf_16_t)0x100b, 1);
template<> gf_w_state<gf_32_t, gf32_vmull_state> gf_matrix<gf_32_t,gf32_vmull_state>::default_field((gf_32_t)0x000000c5, 1);
template<> gf_w_state<gf_64_t, gf64_vmull_state> gf_matrix<gf_64_t,gf64_vmull_state>::default_field((gf_64_t)0x000000000000001b, 1);
#endif  // GF_NEON
