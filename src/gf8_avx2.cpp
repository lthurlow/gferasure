/*
 * gf8_avx.cpp
 *
 * Routines for 8-bit Galois fields using AVX instructions.
 * for additional comments for non-SIMD instructions review gf8_log.cpp comments
 *
*/
 

#include <string.h>

#include <immintrin.h>
#include <nmmintrin.h> //for _mm_blendv_epi8
#include <avxintrin.h>

#include "gf8_avx2.h"

//not defined for linux, may be defined for osx
#define _mm256_set_m128i(hi,lo) _mm256_insertf128_si256(_mm256_castsi128_si256(lo),(hi),1)

/*
 * AVX Constant multiplier, the alt here does *not* represent the alternate mapping
 * just an alternate method for multiplying 32 values by a constant.
 * This method will take the 256 bit multiplication table, take the first 128 bits
 * create a new vector T with that first 128 bits for both halves, then lookup.
 * Do the same for the second 128 bits, and xor the first and second results.
 * htable = A | B (A,B = 128 bits)
 * T = A|A 
 * T = B|B
 * so that we operate over only 128 bits at a time to allow us to use the AVX shuffle
 * which segments by 128 bits, because there is no shuffle_epi16 to index over full 256 bits
*/

static
inline 
__m256i gf8_avx_mul1(__m256i v, __m256i htbl, __m256i loset) {
  __m256i T, subp;
  T = _mm256_set_m128i(_mm256_extractf128_si256(htbl,0),_mm256_extractf128_si256(htbl,0));
  subp = _mm256_shuffle_epi8(T,_mm256_and_si256(loset,v));
  T = _mm256_set_m128i(_mm256_extractf128_si256(htbl,1),_mm256_extractf128_si256(htbl,1));
  v = _mm256_and_si256(loset,_mm256_srli_epi16(v,4));
  return _mm256_xor_si256(subp, _mm256_shuffle_epi8(T,v));
}

static
inline
void
set_up_avx_htable (gf_8_t m, uint32_t pp,  __m256i & htbl) {
    __m256i     lhv = _mm256_set_epi64x (
                                        0xf0e0d0c0b0a09080LL, 0x7060504030201000LL,
                                        0x0f0e0d0c0b0a0908LL, 0x0706050403020100LL
                                       );
    __m256i     poly = _mm256_set1_epi8 ((int8_t) (pp & 0xff));
    __m256i     zero = _mm256_setzero_si256 ();
    uint64_t    mul = (uint64_t)m * 0x0101010101010101ULL;
    __m256i     t1;

    int         left_bit = __builtin_clz ((uint32_t)m) - 24;
    int         i;

    htbl = _mm256_setzero_si256 ();

    for (i = 7; i > left_bit; --i) {
        t1 = _mm256_set1_epi64x (mul << (uint64_t)i);
        htbl = _mm256_xor_si256 (htbl, _mm256_blendv_epi8 (zero, lhv, t1));
        // multiply by 2
        t1 = _mm256_blendv_epi8 (zero, poly, lhv);
        lhv = _mm256_xor_si256 (_mm256_add_epi8 (lhv, lhv), t1);
    }
    t1 = _mm256_set1_epi64x (mul << (uint64_t)i);
    htbl = _mm256_xor_si256 (htbl, _mm256_blendv_epi8 (zero, lhv, t1));
}

/*
 * Multiply Region is used to multiply a region (set of values) by a single
 * multiplier.  The result is then stored the second memmory region.
*/

void
gf8_avx2_state::mul_region (const gf_8_t * src, gf_8_t * dst, uint64_t bytes,
                             gf_8_t multiplier, bool accum) const {

  if (multiplier == (gf_8_t)0) {
    if (! accum) {
      memset (dst, 0, bytes);
    }
    return;
  }
  else if (multiplier == (gf_8_t)1) {
    if (! accum){
      memcpy (dst, src, bytes);
    }   
    else {
      uint64_t    chunks = bytes >> 5ULL;
      for (uint64_t i = 0; i < chunks; i++) {
        __m256i x = _mm256_loadu_si256 (((__m256i *)src) + i); 
        __m256i y = _mm256_loadu_si256 (((__m256i *)dst) + i); 
        _mm256_storeu_si256 (((__m256i *)dst) + i, _mm256_xor_si256(x,y));
      }   
    }   
    return;
  }  

  __m256i     v;
  __m256i     htbl; //AVX2 version
  //__m128i     ltbl; //AVX2 version
  //__m128i     htbl; //AVX2 version

  uint64_t    i;
  uint64_t    chunks = bytes >> 5ULL;


  set_up_avx_htable (multiplier, prim_poly, htbl);
  //set_up_avx_htable2 (multiplier, prim_poly, ltbl,htbl);

  __m256i     loset = _mm256_set1_epi8(0x0f);

  if (accum) {
    for (i = 0; i < chunks; ++i) {
      // load a 256 bit value, it does not need to be alinged _mm256_loadu_si256
      v = _mm256_loadu_si256 (((__m256i *)src) + i);
      v = _mm256_xor_si256 (_mm256_loadu_si256 (((__m256i *)dst) + i),
                         gf8_avx_mul1(v, htbl, loset)); 
                         //gf8_avx_mul2(v, ltbl, htbl, loset)); 
      //store the value back into dst
      _mm256_storeu_si256 (((__m256i *)dst) + i, v);
    }
  } else {
    for (i = 0; i < chunks; ++i) {
      v = _mm256_loadu_si256 (((__m256i *)src) + i);
      v = gf8_avx_mul1(v, htbl, loset); 
      //v = gf8_avx_mul2(v, ltbl,htbl, loset); 
      _mm256_storeu_si256 (((__m256i *)dst) + i, v);
    }
  }
  for (i = chunks << 5ULL; i < bytes; ++i) {
    dst[i] = mul (src[i], multiplier) ^ (accum ? dst[i] : (gf_8_t)0);
  }
}

//Modified for AVX
static
inline
void
mul_vec_by_vec_rnd (__m256i & r, __m256i v1, __m256i & v2, __m256i poly, __m256i zero, int pos)
{
    __m256i     t1;
    // Shift mask bit to high-order
    t1 = _mm256_slli_epi32 (v1, pos);
    // Add into accumulator (using XOR) if bit in multiplicand is non-zero
    r = _mm256_xor_si256 (r, _mm256_blendv_epi8 (zero, v2, t1));
    // Multiply by 2
    t1 = _mm256_blendv_epi8 (zero, poly, v2);
    v2 = _mm256_xor_si256 (_mm256_add_epi8 (v2, v2), t1);
}

//Modified for AVX
static
inline
__m256i
mul_vec_by_vec (__m256i v1, __m256i v2, __m256i pp)
{
    __m256i     r, zero;

    r = _mm256_setzero_si256 ();
    zero = _mm256_setzero_si256 ();

    for (unsigned i = 7; i > 0; --i) {
        mul_vec_by_vec_rnd (r, v1, v2, pp, zero, i);
    }
    // Handle the last round
    r = _mm256_xor_si256 (r, _mm256_blendv_epi8 (zero, v2, v1));

    return r;
}



/*
 * Multiplying one region by another, takes in the two regions to multiply each other
 * by and adds the result to the third memmory buffer if accum is set, and simply stores it if not. 
*/

void
gf8_avx2_state::mul_region_region (const gf_8_t * buf1, const gf_8_t * buf2, gf_8_t * dst,
                                    uint64_t bytes, bool accum) const
{
    uint64_t    i;
    uint64_t    chunks = bytes >> 5ULL;
    __m256i     v1, v2, r, pp;

    pp = _mm256_set1_epi8 ((int8_t)prim_poly);
    if (accum) {
        for (i = 0; i < chunks; ++i) {
            v1 = _mm256_loadu_si256 (((__m256i *)buf1) + i);
            v2 = _mm256_loadu_si256 (((__m256i *)buf2) + i);
            r = _mm256_xor_si256 (_mm256_loadu_si256 (((__m256i *)dst) + i),
                                                mul_vec_by_vec (v1, v2, pp));
            _mm256_storeu_si256 (((__m256i *)dst) + i, r);
        }
        for (i = (chunks * 32); i < bytes; ++i) {
            dst[i] = add (dst[i], mul (buf1[i], buf2[i]));
        }
    } else {
        for (i = 0; i < chunks; ++i) {
            v1 = _mm256_loadu_si256 (((__m256i *)buf1) + i);
            v2 = _mm256_loadu_si256 (((__m256i *)buf2) + i);
            r = mul_vec_by_vec (v1, v2, pp);
            _mm256_storeu_si256 (((__m256i *)dst) + i, r);
        }
        if (!! (bytes & (sizeof (__m256i) - 1))) {
            // Handle the last 32 bytes.  Probably faster to just do 32 using unaligned access
            v1 = _mm256_loadu_si256 ((__m256i *)(buf1 + bytes - sizeof (__m256i)));
            v2 = _mm256_loadu_si256 ((__m256i *)(buf2 + bytes - sizeof (__m256i)));
            r = mul_vec_by_vec (v1, v2, pp);
            _mm256_storeu_si256 ((__m256i *)(dst + bytes - sizeof (__m256i)), r);
        }
    }
}

/*
 * Dot product also multipliest two regions, however unlike multiply region
 * the result is a single GF8, as dot product will multiple each of the
 * two regions together and add the products together.
*/

gf_8_t
gf8_avx2_state::dotproduct (const gf_8_t * src1, const gf_8_t * src2,
                             uint64_t n_elements, uint64_t stride) const
{
    gf_8_t      result = (gf_8_t)0;
    uint64_t    i;
    uint64_t    chunks;
    __m256i     r;
    __m256i     accumulator = _mm256_setzero_si256 ();
    __m256i     pp = _mm256_set1_epi8 ((int8_t)prim_poly);

    if (stride == 1) {
        chunks = n_elements >> 5ULL;
        for (i = 0; i < chunks; ++i) {
            r = mul_vec_by_vec (_mm256_loadu_si256 ((__m256i *)src1 + i),
                                _mm256_loadu_si256 ((__m256i *)src2 + i), pp);
            accumulator = _mm256_xor_si256 (r, accumulator);
        }
        for (i = chunks * sizeof (__m256i); i < n_elements; ++i) {
            result ^= mul (src1[i], src2[i]);
        }
        result ^= _mm256_extract_epi8 (accumulator, 0); 
        result ^= _mm256_extract_epi8 (accumulator, 1); 
        result ^= _mm256_extract_epi8 (accumulator, 2); 
        result ^= _mm256_extract_epi8 (accumulator, 3); 
        result ^= _mm256_extract_epi8 (accumulator, 4); 
        result ^= _mm256_extract_epi8 (accumulator, 5); 
        result ^= _mm256_extract_epi8 (accumulator, 6); 
        result ^= _mm256_extract_epi8 (accumulator, 7); 
        result ^= _mm256_extract_epi8 (accumulator, 8); 
        result ^= _mm256_extract_epi8 (accumulator, 9); 
        result ^= _mm256_extract_epi8 (accumulator, 10);
        result ^= _mm256_extract_epi8 (accumulator, 11);
        result ^= _mm256_extract_epi8 (accumulator, 12);
        result ^= _mm256_extract_epi8 (accumulator, 13);
        result ^= _mm256_extract_epi8 (accumulator, 14);
        result ^= _mm256_extract_epi8 (accumulator, 15);
        result ^= _mm256_extract_epi8 (accumulator, 16); 
        result ^= _mm256_extract_epi8 (accumulator, 17); 
        result ^= _mm256_extract_epi8 (accumulator, 18); 
        result ^= _mm256_extract_epi8 (accumulator, 19); 
        result ^= _mm256_extract_epi8 (accumulator, 20); 
        result ^= _mm256_extract_epi8 (accumulator, 21); 
        result ^= _mm256_extract_epi8 (accumulator, 22); 
        result ^= _mm256_extract_epi8 (accumulator, 23); 
        result ^= _mm256_extract_epi8 (accumulator, 24); 
        result ^= _mm256_extract_epi8 (accumulator, 25); 
        result ^= _mm256_extract_epi8 (accumulator, 26);
        result ^= _mm256_extract_epi8 (accumulator, 27);
        result ^= _mm256_extract_epi8 (accumulator, 28);
        result ^= _mm256_extract_epi8 (accumulator, 29);
        result ^= _mm256_extract_epi8 (accumulator, 30);
        result ^= _mm256_extract_epi8 (accumulator, 31);
    } else if (stride == 2) {
/*
        uint64_t        chunks = n_elements >> 3ULL;
        __m128i         r;
        __m128i         accumulator = _mm_setzero_si128 ();
        __m128i         pp = _mm_set1_epi8 ((int8_t)prim_poly);
*/
/*
        for (i = 0; i < chunks; ++i) {
            r = mul_vec_by_vec (_mm_loadu_si128 ((__m128i *)src1 + i),
                                _mm_loadu_si128 ((__m128i *)src2 + i), pp);
            accumulator = _mm_xor_si128 (r, accumulator);
        }
        for (i = chunks * sizeof (__m128i); i < n_elements; ++i) {
            result ^= mul (src1[i], src2[i]);
        }
        result ^= _mm_extract_epi8 (accumulator, 0); 
        result ^= _mm_extract_epi8 (accumulator, 1); 
        result ^= _mm_extract_epi8 (accumulator, 2); 
        result ^= _mm_extract_epi8 (accumulator, 3); 
        result ^= _mm_extract_epi8 (accumulator, 4); 
        result ^= _mm_extract_epi8 (accumulator, 5); 
        result ^= _mm_extract_epi8 (accumulator, 6); 
        result ^= _mm_extract_epi8 (accumulator, 7); 
        result ^= _mm_extract_epi8 (accumulator, 8); 
        result ^= _mm_extract_epi8 (accumulator, 9); 
        result ^= _mm_extract_epi8 (accumulator, 10);
        result ^= _mm_extract_epi8 (accumulator, 11);
        result ^= _mm_extract_epi8 (accumulator, 12);
        result ^= _mm_extract_epi8 (accumulator, 13);
        result ^= _mm_extract_epi8 (accumulator, 14);
        result ^= _mm_extract_epi8 (accumulator, 15);
*/
    }

    return result;
}

//FIXME: Not modified for AVX
inline
__m128i
multiply_vec_by_2 (__m128i v, __m128i reducer, __m128i mask)
{
    return _mm_xor_si128 (_mm_shuffle_epi8 (reducer, _mm_srli_epi64 (_mm_and_si128 (v, mask), 7)),
                          _mm_add_epi8 (v, v));
}
