//
// gf_unittest.cpp
//
// Run unit tests on finite field.  For most fields, this can't be done exhaustively,
// so we just use randomness to do it.

#include <inttypes.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include "gf_w.h"
#include "gf_random.h"
#include "gf_erasure.h"

int rcode;
const char *    prog_name;

template<class gf_w, class gf_state>
unsigned
erasure_code_test_one (unsigned n_data, unsigned n_ecc, uint64_t n_bytes, unsigned n_tests,
                       unsigned ecc_type = ECC_CAUCHY_SYSTEMATIC)
{
    uint64_t                    n_words = n_bytes / sizeof (gf_w);
    gf_erasure<gf_w, gf_state>  kernel (n_data, n_ecc, ecc_type);
    gf_w *                      data[n_data];
    gf_w *                      stored[n_data + n_ecc];
    gf_w *                      results[n_data];
    gf_w *                      in_bufs[n_data + n_ecc];
    gf_w *                      out_bufs[n_data + n_ecc];
    gf_w                        rebuild_symbols[n_data + n_ecc];
    gf_w *                      space = new gf_w[n_words * (3 * n_data + n_ecc)];
    unsigned                    i, j;

    for (i = 0; i < n_data; ++i, space += n_words) {
        data[i] = space;
        gf_random_fill (data[i], n_bytes);
    }

    // Allocate space for stored information.  This can be up to n_data + n_ecc because
    // some codes are non-systematic.
    for (i = 0; i < n_data + n_ecc; ++i, space += n_words) {
        stored[i] = space;
        out_bufs[i] = space;
    }
    // Allocate space for computed data for rebuild tests.
    for (i = 0; i < n_data; ++i, space += n_words) {
        results[i] = space;
    }
    // Generate the coded data
    kernel.generate ((const gf_w **)data, out_bufs, n_bytes);

    for (i = 0; i < n_tests; ++i) {
        gf_w    z, tmp;
        for (j = 0; j < n_data + n_ecc; ++j) {
            rebuild_symbols[j] = (gf_w)j;
        }
        // Randomly pick symbols to use for regeneration
        for (j = 0; j < n_data; ++j) {
            z = gf_random_u64 () % (n_data + n_ecc - j);
            tmp = rebuild_symbols[j];
            rebuild_symbols[j] = rebuild_symbols[z];
            rebuild_symbols[z] = tmp;
        }
        for (j = 0; j < n_data; ++j) {
            in_bufs[j] = stored[rebuild_symbols[j]];
        }

        kernel.config_rebuild (rebuild_symbols);
        kernel.rebuild ((const gf_w **)in_bufs, results, n_bytes);
        for (j = 0; j < n_data; ++j) {
            if (memcmp (results[j], data[j], n_bytes)) {
                return (i);
            }
        }
    }
    return (n_tests);
}

// We should exhaustively check, but we can't do that for very large fields because it's too
// slow.  Thus, for field sizes of 2^16 or lower, we're exhaustive.  Otherwise, we're not.
template<class gf_w, class gf_state>
unsigned
gf_w_state<gf_w, gf_state>::unit_test (const char * which_tests,
                                       uint64_t test_size, uint64_t bufsize)
{
    uint64_t    i, j;
    bool        subset = test_size <= state.field_max();
    gf_w        v, m, r1, r2, r3;
    gf_w        buf0[bufsize / sizeof (gf_w)];
    gf_w        buf1[bufsize / sizeof (gf_w)];
    gf_w        buf2[bufsize / sizeof (gf_w)];
    gf_w        buf3[bufsize / sizeof (gf_w)];
    gf_w        buf4[bufsize / sizeof (gf_w)];
    uint64_t    n_tests = subset ? test_size : state.field_max() + 1;

    // Check that inverses really are
    if (strchr (which_tests, 'i')) {
        printf("\t\tInverses: ");
        fflush (stdout);
        for (i = 0; i < n_tests; ++i) {
            if (subset) {
                v = gf_random_val<gf_w>();
            } else {
                v = i;
            }
            v = (! v) ? 1 : v;
            r1 = mul (inv (v), v);
            if (r1 != (gf_w)1) {
                printf(" fail\n");
                rcode = -10;
                return (TEST_INV);
            }
        }
        printf(" pass\n");
    }

    // Check multiplication by 1
    if (strchr (which_tests, '1')) {
        printf("\t\tIdentity Multiplication: ");
        fflush (stdout);
        for (i = 0; i < n_tests; ++i) {
            if (subset) {
                v = gf_random_val<gf_w>();
            } else {
                v = i;
            }
            r1 = mul (v, (gf_w)1);
            if (r1 != v) {
                printf(" fail\n");
                rcode = -11;
                return (TEST_MUL_BY_1);
            }
        }
        printf(" pass\n");
    }

    // Check that multiplication and division work.
    if (strchr (which_tests, 'd')) {
        printf("\t\tIdentity Division: ");
        fflush (stdout);
        for (i = 0; i < test_size; i++) {
            v = gf_random_val<gf_w>();
            v = (! v) ? 1 : v;
            m = gf_random_val<gf_w>();
            m = (! m) ? 1 : m;
            r1 = mul (v, m);
            r2 = div (r1, m);
            r3 = div (r1, v);
            if (r2 != v || r3 != m) {
                printf(" fail\n");
                rcode = -12;
                return (TEST_MUL_DIV);
            }
        }
        printf(" pass\n");
    }

    // Check multiplication of region by constant
    if (strchr (which_tests, 'C')) {
        printf("\t\tMul by constant: ");
        fflush (stdout);
        for (i = 0; i < test_size; ++i) {
            if (i == 0){
              m = 0;
            } else if (i==1){
              m = 1;
            } else {
              m = gf_random_val<gf_w> ();
            }
            for (j = 0; j < bufsize / sizeof (gf_w); ++j) {
                buf0[j] = gf_random_val<gf_w> ();
                buf2[j] = mul (buf0[j], m);
                buf3[j] = gf_random_val<gf_w> ();
                buf4[j] = add (buf3[j],buf2[j]);
            }
            memset (buf1, 0, bufsize);
            mul_region (buf0, buf1, bufsize, m, false);
            if (!! memcmp (buf1, buf2, bufsize)) {
                printf(" FAIL\n");
                rcode = -13;
                return (TEST_MUL_REGION_CONST);
            }
            /*cant pass buf1, buf1 for ARM - segfault*/
            mul_region (buf0, buf3, bufsize, m, true);
            if (!! memcmp (buf3, buf4, bufsize)) {
                printf(" FAIL ACCUM\n");
                rcode = -14;
                return (TEST_MUL_REGION_CONST);
            }

        }
        printf(" pass\n");
    }

    // Check multiplication of region by region
    if (strchr (which_tests, 'R')) {
        printf("\t\tMul by region: ");
        fflush (stdout);
        for (i = 0; i < test_size; ++i) {
            m = gf_random_val<gf_w> ();
            for (j = 0; j < bufsize / sizeof (gf_w); ++j) {
                buf1[j] = gf_random_val<gf_w> ();
                buf2[j] = gf_random_val<gf_w> ();
                buf3[j] = (gf_w)0;
                buf4[j] = add (mul (buf1[j],buf2[j]), buf1[j]);
            }
            mul_region_region (buf1, buf2, buf3, bufsize,false);
            for (j = 0; j < bufsize / sizeof (gf_w); ++j) {
                if (buf3[j] != mul (buf1[j], buf2[j])) {
                    printf(" fail\n");
                    rcode = -15;
                    return (TEST_MUL_REGION_REGION);
                }
            }
            mul_region_region (buf1, buf2, buf1, bufsize,true);
            if (!! memcmp (buf4, buf1, bufsize)) {
                printf(" fail [ACCUM]\n");
                rcode = -16;
                return (TEST_MUL_REGION_REGION);
            }
        }
        printf(" pass\n");
    }

    // Check dot product
    if (strchr (which_tests, 'P')) {
        printf("\t\tDot-product: ");
        fflush (stdout);
        uint64_t    n_elements = bufsize / sizeof (gf_w);
        for (i = 0; i < n_elements; ++i) {
            buf1[i] = gf_random_val<gf_w> ();
        }
        for (i = 0; i < test_size; ++i) {
            r2 = (gf_w)0;
            for (j = 0; j < n_elements; ++j) {
                buf2[j] = gf_random_val<gf_w> ();
                r2 ^= mul (buf1[j], buf2[j]);
            }
            r1 = dotproduct (buf1, buf2, n_elements, 1);
            if (r1 != r2) {
                //printf("\n%08x(c) == %08x(mine):%d\n",r2,r1,n_elements);
                printf(" fail\n");
                rcode = -17;
                return (TEST_DOT_PRODUCT);
            }
        }
        printf(" pass\n");
    }

    // Check pow
    if (strchr (which_tests, 'e')) {
        printf ("\t\tPowers: ");
        // We can't check all powers.  Cap power to 1500.
        for (i = 0; i < test_size; ++i) {
            v = gf_random_val<gf_w>();
            m = gf_random_val<gf_w>();
            if (m > 1500) {
                m %= (gf_w)1500;
            }
            r1 = pow (v, m);
            for (r2 = (gf_w)1, j = 0; j < m; ++j) {
                r2 = mul (r2, v);
            }
            if (r1 != r2) {
                printf (" fail\n");
                rcode = -18;
                return (TEST_POW);
            }
        }
        printf(" pass\n");
    }

    return (0);
}

static gf_w_state<gf_8_t,  gf8_shiftadd_state>  gf8_shiftadd_test (gf8_default_primpoly);
static gf_w_state<gf_16_t, gf16_shiftadd_state> gf16_shiftadd_test (gf16_default_primpoly);
static gf_w_state<gf_32_t, gf32_shiftadd_state> gf32_shiftadd_test (gf32_default_primpoly);
static gf_w_state<gf_64_t, gf64_shiftadd_state> gf64_shiftadd_test (gf64_default_primpoly);
#ifdef  GF_LOG
static gf_w_state<gf_8_t,  gf8_log_state>       gf8_log_test (gf8_default_primpoly);
static gf_w_state<gf_16_t, gf16_log_state>      gf16_log_test (gf16_default_primpoly);
#endif

#ifdef  GF_SHIFT2
static gf_w_state<gf_8_t,  gf8_shift2_state>    gf8_shift2_test (gf8_default_primpoly);
static gf_w_state<gf_16_t, gf16_shift2_state>   gf16_shift2_test (gf16_default_primpoly);
#endif

#ifdef  GF_SSSE3
static gf_w_state<gf_8_t,  gf8_ssse3_state>     gf8_ssse3_test (gf8_default_primpoly);
static gf_w_state<gf_16_t, gf16_ssse3_state>    gf16_ssse3_test (gf16_default_primpoly);
#endif
#ifdef  GF_AVX2
static gf_w_state<gf_8_t,  gf8_avx2_state>      gf8_avx2_test (gf8_default_primpoly);
static gf_w_state<gf_16_t, gf16_avx2_state>     gf16_avx2_test (gf16_default_primpoly);
static gf_w_state<gf_8_t,  gf8_gather_state>    gf8_gather_test (gf8_default_primpoly);
static gf_w_state<gf_16_t, gf16_gather_state>   gf16_gather_test (gf16_default_primpoly);
#endif
#ifdef  GF_CLMUL
static gf_w_state<gf_8_t,  gf8_clmul_state>     gf8_clmul_test (gf8_default_primpoly);
static gf_w_state<gf_16_t, gf16_clmul_state>    gf16_clmul_test (gf16_default_primpoly);
static gf_w_state<gf_32_t, gf32_clmul_state>    gf32_clmul_test (gf32_default_primpoly);
static gf_w_state<gf_64_t, gf64_clmul_state>    gf64_clmul_test (gf64_default_primpoly);
#endif
#if defined(GF_NEON32) || defined(GF_NEON64)
static gf_w_state<gf_8_t,  gf8_vmull_state>     gf8_vmull_test  (gf8_default_primpoly);
static gf_w_state<gf_16_t, gf16_vmull_state>    gf16_vmull_test (gf16_default_primpoly);
static gf_w_state<gf_32_t, gf32_vmull_state>    gf32_vmull_test (gf32_default_primpoly);
static gf_w_state<gf_64_t, gf64_vmull_state>    gf64_vmull_test (gf64_default_primpoly);
static gf_w_state<gf_8_t,  gf8_neon_state>      gf8_neon_test   (gf8_default_primpoly);
static gf_w_state<gf_16_t, gf16_neon_state>     gf16_neon_test  (gf16_default_primpoly);
#endif

struct field_test_info {
    const char *        field_name;
    uint64_t            n_bytes_field_unittest;
    unsigned            n_tests_field_unittest;
    int (*field_unittest) (const char * tests, uint64_t test_size, uint64_t bufsize);
    unsigned (*ecc_unittest) (unsigned n_data, unsigned n_ecc, uint64_t n_bytes,
                              unsigned n_tests, unsigned ecc_mode);
};

static struct field_test_info test_fields[] = {
    {"gf8_shiftadd", 256, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf8_shiftadd_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_8_t, gf8_shiftadd_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf16_shiftadd", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf16_shiftadd_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_16_t, gf16_shiftadd_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf32_shiftadd", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf32_shiftadd_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_32_t, gf32_shiftadd_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf64_shiftadd", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf64_shiftadd_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_64_t, gf64_shiftadd_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf8_log", 256, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf8_log_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_8_t, gf8_log_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf16_log", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf16_log_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_16_t, gf16_log_state> (data, ecc, bytes, tests, ecc_mode);}
    },
#ifdef  GF_SHIFT2
    {"gf8_shift2", 256, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf8_shift2_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_8_t, gf8_shift2_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf16_shift2", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf16_shift2_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_16_t, gf16_shift2_state> (data, ecc, bytes, tests, ecc_mode);}
    },
#endif
#ifdef  GF_SSSE3
    {"gf8_ssse3", 256, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf8_ssse3_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_8_t, gf8_ssse3_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf16_ssse3", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf16_ssse3_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_16_t, gf16_ssse3_state> (data, ecc, bytes, tests, ecc_mode);}
    },
#endif
#ifdef  GF_AVX2
    {"gf8_avx2", 256, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf8_avx2_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_8_t, gf8_avx2_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf16_avx2", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf16_avx2_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_16_t, gf16_avx2_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf8_gather", 256, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf8_gather_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_8_t, gf8_gather_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf16_gather", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf16_gather_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_16_t, gf16_gather_state> (data, ecc, bytes, tests, ecc_mode);}
    },
#endif
#ifdef  GF_CLMUL
    {"gf8_clmul", 256, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf8_clmul_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_8_t, gf8_clmul_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf16_clmul", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf16_clmul_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_16_t, gf16_clmul_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf32_clmul", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf32_clmul_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_32_t, gf32_clmul_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf64_clmul", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf64_clmul_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_64_t, gf64_clmul_state> (data, ecc, bytes, tests, ecc_mode);}
    },
#endif
#if defined(GF_NEON32) || defined(GF_NEON64)
    {"gf8_vmull", 256, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf8_vmull_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_8_t, gf8_vmull_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf16_vmull", 256, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf16_vmull_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_16_t, gf16_vmull_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf32_vmull", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf32_vmull_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_32_t, gf32_vmull_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf64_vmull", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf64_vmull_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_64_t, gf64_vmull_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf8_neon", 256, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf8_neon_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_8_t, gf8_neon_state> (data, ecc, bytes, tests, ecc_mode);}
    },
    {"gf16_neon", 2048, 1024,
     [](const char * tests, uint64_t test_size, uint64_t bufsize)->int
     {return gf16_neon_test.unit_test (tests, test_size, bufsize);},
     [](unsigned data, unsigned ecc, uint64_t bytes, unsigned tests, unsigned ecc_mode)->unsigned
     {return erasure_code_test_one<gf_16_t, gf16_neon_state> (data, ecc, bytes, tests, ecc_mode);}    },
#endif

};

static unsigned n_test_fields = sizeof (test_fields) / sizeof (test_fields[0]);

void
error_and_exit ()
{
    fprintf (stderr, "Usage: %s [options] <field1> [field2] ...\n", prog_name);
    fprintf (stderr, "\t--unittests=<tests>\n");
    fprintf (stderr, "\t\tValid tests are (use single letter in <tests>:\n");
    fprintf (stderr, "\t\ti: Inverse\n");
    fprintf (stderr, "\t\t1: identity\n");
    fprintf (stderr, "\t\td: multiply-Divide\n");
    fprintf (stderr, "\t\tC: multiply Constant by region\n");
    fprintf (stderr, "\t\tR: multiply Region by region\n");
    fprintf (stderr, "\t\tP: dot Product\n");
    fprintf (stderr, "\t\te: power (Exponentiation)\n");
    fprintf (stderr, "\t--erasure=data,ecc : perform unit tests on erasure coding for this data / ecc combination\n");
    fprintf (stderr, "\t--coding=<c|s|v>\n");
    fprintf (stderr, "\t\tc: non-systematic Cauchy\n");
    fprintf (stderr, "\t\ts: systematic Cauchy\n");
    fprintf (stderr, "\t\tv: non-systematic Vandermonde\n");
    fprintf (stderr, "\t--ntests=n : perform n tests (0 < n < 10000)\n");
    fprintf (stderr, "\n\tValid fields are:\n");
    for (unsigned i = 0; i < n_test_fields; ++i) {
        fprintf (stderr, "\t\t%s\n", test_fields[i].field_name);
    }
    exit (-99);
}


int
main (int argc , char * const * argv)
{
    static struct option option_desc[] = {
        {"help",        no_argument,            NULL,           'h'},
        {"unittests",   required_argument,      NULL,           'u'},
        {"erasures",    required_argument,      NULL,           'e'},
        {"coding",      required_argument,      NULL,           'c'},
        {"ntests",      required_argument,      NULL,           'n'},
        {NULL,          0,                      NULL,           0 }
    };
    const char *        unit_tests = "";
    const char *        ecc_tests = "";
    const char          valid_unit_tests[] = "i1dCRPe";
    const char          valid_ecc_tests[] = "cvs";
    const char *        c;
    unsigned            n_data = 0U, n_ecc = 0U;
    uint64_t            n_bytes = 128;
    uint64_t            n_tests = 20;
    int                 ch;
    unsigned            n_passed;
    std::vector<unsigned> ecc_mode;
    std::vector<const char*> ecc_mode_name;
    rcode = 0;

    prog_name = argv[0];
    // Tests are:
    // i: inverse
    // 1: multiply by 1
    // d: multiply / divide
    // C: multiply region by constant
    // R: multiply region by region
    // P: dot product
    // e: power (exponent)

    while ((ch = getopt_long (argc, argv, "hu:e:c:n:", option_desc, NULL)) != -1) {
        switch (ch) {
        case 'u':
            for (c = optarg; *c != '\0'; ++c) {
                if (strchr (valid_unit_tests, *c) == NULL) {
                    fprintf (stderr, "%s: invalid unit test %c\n", argv[0], *c);
                    error_and_exit ();
                }
            }
            unit_tests = optarg;
            break;
        case 'e':
            if (sscanf (optarg, "%u,%u", &n_data, &n_ecc) != 2) {
                error_and_exit ();
            }
            printf ("n_data=%d n_ecc=%d\n", n_data, n_ecc);
            break;
        case 'n':
            n_tests = strtoll (optarg, NULL, 10);
            if (n_tests < 1 || n_tests > 10000) {
                error_and_exit ();
            }
            break;
        case 'c':
            for (c = optarg; *c != '\0'; ++c) {
                if (strchr (valid_ecc_tests, *c) == NULL) {
                    fprintf (stderr, "%s: invalid ecc test %c\n", argv[0], *c);
                    error_and_exit ();
                }
                if (c[0] == 'c') {
                    ecc_mode.push_back(ECC_CAUCHY);
                    ecc_mode_name.push_back("CAUCHY");
		} else if (c[0] == 'v'){
                    ecc_mode.push_back(ECC_VANDERMONDE);
                    ecc_mode_name.push_back("VANDERMONDE");
		} else if (c[0] == 's'){
                    ecc_mode.push_back(ECC_CAUCHY_SYSTEMATIC);
                    ecc_mode_name.push_back("SYSTEMATIC CAUCHY");
		}
            }
            ecc_tests = optarg;
            break;
        case 'h':
        default:
            error_and_exit ();
            break;
        }
    }

    if (argc < 2) {
        error_and_exit ();
    }

    for (int i = 2; i < argc; ++i) {
        for (unsigned j = 0; j < n_test_fields; ++j) {
            const struct field_test_info *  fld = &(test_fields[j]);
            if (!strcmp (argv[i], fld->field_name)) {
                printf ("-----------> %s:\n", fld->field_name);
                printf ("\tRunning field unit tests: %s\n", unit_tests);
                fld->field_unittest (unit_tests, fld->n_bytes_field_unittest,
                                     fld->n_tests_field_unittest);
                printf ("\tRunning ECC unit tests: %s\n",ecc_tests);
                if (n_data > 0 && n_ecc > 0) {
		    for (int g=ecc_mode.size(); g>0; g--){
			unsigned m = ecc_mode.at(g-1);
			    printf ("\t\tECC unit test (%s, n_data=%d n_ecc=%d): ", ecc_mode_name[g-1],
				    n_data, n_ecc);
			    fflush (stdout);
			    if ((n_passed = fld->ecc_unittest (n_data, n_ecc, n_bytes,
							       n_tests, m)) != n_tests) {
				printf (" FAIL (%d passed).\n", n_passed);
				rcode = m*-1;
			    } else {
				printf (" pass\n");
			    }
		    }
                }
                printf ("<-----------\n");
            }
        }
    }
    return(rcode);
}
