//
// gf8_clmul.h
//
// 8-bit Galois field using CLMUL.
//

#pragma     once

#include "gf_types.h"
#include "gfw_shiftadd.h"

#include "gf_random.h"

struct
alignas(16)
gf8_clmul_state {

    gf8_clmul_state (gf_8_t prim_poly_ = gf8_default_primpoly, gf_8_t alpha_ = 1)
        : prim_poly(prim_poly_)
        , shiftadd_state(prim_poly_, alpha_)
    {
    }


    gf_8_t  add (gf_8_t a, gf_8_t b)  const {return ((gf_8_t)(a ^ b));}
    gf_8_t  sub (gf_8_t a, gf_8_t b)  const {return ((gf_8_t)(a ^ b));}
    gf_8_t  inv (gf_8_t a)             const {return shiftadd_state.inv(a);}
    gf_8_t  pow (gf_8_t a, uint8_t b) const {return shiftadd_state.pow(a,b);}
    gf_8_t  mul (gf_8_t a, gf_8_t b)  const;
    gf_8_t  div (gf_8_t a, gf_8_t b)  const {return (mul (a,inv(b)));}

    void        mul_region (const gf_8_t * src, gf_8_t * dst,
                            uint64_t bytes, gf_8_t m, bool accum = false) const;

    void        mul_region_region (const gf_8_t * src1, const gf_8_t * src2, gf_8_t * dst,
                                 uint64_t bytes, bool accum = false) const;

    gf_8_t      dotproduct (const gf_8_t * src1, const gf_8_t * src2,
                            uint64_t n_elements, uint64_t stride = 1) const;

    uint64_t    field_size () const     {return GF8_FIELD_SIZE;}
    uint64_t    field_max () const      {return (field_size() - 1);}

 private:
    uint8_t             prim_poly;
    gf8_shiftadd_state  shiftadd_state;
};
