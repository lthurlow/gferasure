/*
 * gf16_clmul.cpp
 *
 * Routines for 16-bit Galois fields using SSSE3 tables.
 * Additional comments can be found in gf16_log.cpp for non-simd functions
 * 
 */

#include "gf16_clmul.h"

#include <stdio.h>
#include <string.h>
#include <x86intrin.h>

static
inline
__m128i 
mul_8(__m128i a, __m128i b,__m128i pp){
  __m128i  res0,res1;
  __m128i  v0, w0;
  //replaced 0xffff with -1 for overflow warning
  __m128i  mask = _mm_set_epi16(0,-1,0,-1,0,-1,0,-1);
  
  res0 = _mm_clmulepi64_si128 (_mm_and_si128(a,mask), b, 0);
  v0 = _mm_srli_epi32(res0, 16);
  w0 = _mm_clmulepi64_si128 (pp, v0, 0);
  res0 = _mm_xor_si128 (_mm_and_si128(mask,res0), w0);
  v0 = _mm_srli_epi32(res0, 16);
  w0 = _mm_clmulepi64_si128 (pp, v0, 0);
  res1 = _mm_and_si128(mask,_mm_xor_si128 (res0, w0));

  res0 = _mm_clmulepi64_si128 (_mm_and_si128(_mm_srli_epi64(a,16),mask), b, 0);
  v0 = _mm_srli_epi32(res0, 16);
  w0 = _mm_clmulepi64_si128 (pp, v0, 0);
  res0 = _mm_xor_si128 (_mm_and_si128(mask,res0), w0);
  v0 = _mm_srli_epi32(res0, 16);
  w0 = _mm_clmulepi64_si128 (pp, v0, 0);
  res1 = _mm_xor_si128(res1,_mm_slli_si128(_mm_xor_si128 (_mm_and_si128(mask,res0), w0),2));

  res0 = _mm_clmulepi64_si128 (_mm_and_si128(a,mask), b, 1);
  v0 = _mm_srli_epi32(res0, 16);
  w0 = _mm_clmulepi64_si128 (pp, v0, 0);
  res0 = _mm_xor_si128 (_mm_and_si128(mask,res0), w0);
  v0 = _mm_srli_epi32(res0, 16);
  w0 = _mm_clmulepi64_si128 (pp, v0, 0);
  res1 = _mm_xor_si128(res1,_mm_slli_si128(_mm_xor_si128 (_mm_and_si128(mask,res0), w0),8));

  res0 = _mm_clmulepi64_si128 (_mm_and_si128(_mm_srli_epi64(a,16),mask), b, 1);
  v0 = _mm_srli_epi32(res0, 16);
  w0 = _mm_clmulepi64_si128 (pp, v0, 0);
  res0 = _mm_xor_si128 (_mm_and_si128(mask,res0), w0);
  v0 = _mm_srli_epi32(res0, 16);
  w0 = _mm_clmulepi64_si128 (pp, v0, 0);
  res1 = _mm_xor_si128(res1,_mm_slli_si128(_mm_xor_si128 (_mm_and_si128(mask,res0), w0),10));

  return res1;
}

gf_16_t
gf16_clmul_state::mul(gf_16_t a16, gf_16_t b16) const {
  __m128i  a, b;
  __m128i  result;
  __m128i  pp;
  __m128i  v, w;

  a = _mm_insert_epi16(_mm_setzero_si128(), a16, 0);
  b = _mm_insert_epi16(a, b16, 0);
  pp = _mm_set_epi16(0, 0, 0, 0, 0, 0, 1, (uint16_t)(prim_poly & 0x00ffULL));
  
  result = _mm_clmulepi64_si128(a, b, 0);
  v = _mm_srli_epi64(result, 16);
  w = _mm_clmulepi64_si128(pp, v, 0);
  result = _mm_xor_si128(result, w);
  v = _mm_srli_epi64(result, 16);
  w = _mm_clmulepi64_si128(pp, v, 0);
  result = _mm_xor_si128(result, w);
  return ((gf_16_t)_mm_extract_epi16(result, 0));
}


/*
 * Custom 16 bit blend function (not present in intel).
 * takes the same 8 bit region twice to construct a 16 bit region.
 * these 16 bit regions, will then both choose yes or no in the blendv
 * to select the correct 16 bit element.
static
inline
__m128i 
gf16_blend_mask(__m128i l, __m128i mask){
  __m128i temp = _mm_srai_epi16(mask, 15);
  return(_mm_and_si128(temp,l));
}
*/


/*
 * Multiply Region function takes in two regions of memmory, the first region
 * is the multiplicand, to be be multiplied by the multiplier and stored in the 
 * second memmory region.  This function is to be used for multiplying multiple
 * variables by a single multiplier.  In particular this function uses the 0007 
 * htable and alternate mapping, to change to normal mapping, comment out the 
 * to_altmap function as well as the mult_alt, then uncomment out the mult_const
*/



void
gf16_clmul_state::mul_region (const gf_16_t * src, gf_16_t * dst, uint64_t bytes,
                              gf_16_t multiplier, bool accum) const {

  if (multiplier == (gf_16_t)0) {
    if (! accum) {
      memset (dst, 0, bytes);
    }
    return;
  }
  else if (multiplier == (gf_8_t)1) {
    if (! accum){
      memcpy (dst, src, bytes);
    }   
    else {
      uint64_t    chunks = bytes >> 4ULL;
      for (uint64_t i = 0; i < chunks; i++) {
        __m128i x = _mm_loadu_si128 (((__m128i *)src) + i);
        __m128i y = _mm_loadu_si128 (((__m128i *)dst) + i);
        _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128(x,y));
      }
    }
    return;
  }   

  uint32_t    i;
  uint32_t    chunks = bytes >> 4ULL;
  __m128i     x,y;
  __m128i     mm = _mm_set_epi16(0,0,0,0,0,0,0,multiplier);
  __m128i pp = _mm_set_epi16(0,0,0,0,
                             0,0,
                             0, (uint16_t)(prim_poly & 0xffULL));
  if (accum) {
    for (i = 0; i < chunks; i++) {
      x = _mm_loadu_si128 (((__m128i *)src) + i);
      y = _mm_loadu_si128 (((__m128i *)dst) + i);
      x = mul_8(x,mm,pp);
      _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128(x,y));
    }
  } else {
    for (i = 0; i < chunks; i++) {
      x = _mm_loadu_si128 (((__m128i *)src) + i);
      x = mul_8(x,mm,pp);
      _mm_storeu_si128 (((__m128i *)dst) + i, x);
    }
  }
}

void
gf16_clmul_state::mul_region_region (const gf_16_t * buf1, const gf_16_t * buf2, gf_16_t * dst,
                                    uint64_t bytes, bool accum) const
{
  uint32_t    i;
  uint32_t    chunks = bytes >> 1ULL;
  if (accum) {
    for (i = 0; i < chunks; i++) {
      dst[i] = add(dst[i],mul(buf1[i],buf2[i]));
    }
  } else {
    for (i = 0; i < chunks; i++) {
      dst[i] = mul(buf1[i],buf2[i]);
    }
  }
}

gf_16_t
gf16_clmul_state::dotproduct (const gf_16_t * src1, const gf_16_t * src2,
                              uint64_t n_elements, uint64_t stride) const
{

  gf_32_t  dp = (gf_32_t)0;

  for (uint32_t i = 0; i < n_elements; ++i) {
    dp = add (dp, mul (*src1, *src2));
    src1 += stride;
    src2 += stride;
  }
  return dp;
}
