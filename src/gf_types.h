//
// Definitions for Galois field element types

#pragma once

#include <stdint.h>

// 8 bits, field of 256
#define GF8_BITS        8ULL
#define GF8_FIELD_SIZE  (1ULL << GF8_BITS)
typedef uint8_t         gf_8_t;

// 16 bits, field of 65536
#define GF16_BITS       16ULL
#define GF16_FIELD_SIZE (1ULL << GF16_BITS)
typedef uint16_t        gf_16_t;

// 32 bits, field of 4294967296
#define GF32_BITS       32ULL
#define GF32_FIELD_SIZE (1ULL << GF32_BITS)
typedef uint32_t        gf_32_t;

// 64 bits,
#define GF64_BITS       64ULL
#define GF64_FIELD_MAX  0xffffffffffffffffULL
typedef uint64_t        gf_64_t;

// Primitive polynomials for various field widths from "Primitive Polynomials Over Finite Fields"
// [Hansen & Mullen, Mathematics of Computation 59(200), pages 639-643, October 1992].
// The value for 16 bits isn't in the table, but is primitive.
//
//   8: 0x1d
//  16: 0x100b
//  32: 0x000000c5
//  64: 0x000000000000001b

// This will be the gf_state passed into gf_w_state
static const gf_8_t     gf8_default_primpoly    = (gf_8_t)0x1d;
//static const gf_16_t    gf16_default_primpoly   = (gf_16_t)0x100b;
/*This irreducible polynomial is more convient for clmul gf16 as no upper bits*/
static const gf_16_t    gf16_default_primpoly   = (gf_16_t)0x002d;
static const gf_32_t    gf32_default_primpoly   = (gf_32_t)0x000000c5;
static const gf_64_t    gf64_default_primpoly   = (gf_64_t)0x000000000000001b;
