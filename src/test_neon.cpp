#include <stdio.h> // for printf and stdout
#include <stdlib.h> // for strtol and exit
#include <getopt.h> // included for user input
#include <sys/time.h> // for the gettimeofday
#include <string.h>
#include <ctype.h>
#include <arm_neon.h>

#include "gf_random.h"
#include "gf8_log.h"
#include "gf16_log.h"


typedef uint8x16_t v128u;
typedef uint8_t gf_8_t;
typedef uint16_t gf_16_t;
typedef uint16x8_t __v128u;

static
void
print_v128u(v128u v){
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,0));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,1));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,2));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,3));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,4));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,5));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,6));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,7));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,8));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,9));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,10));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,11));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,12));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,13));
  printf("%02x ",(uint8_t)vgetq_lane_u8(v,14));
  printf("%02x",(uint8_t)vgetq_lane_u8(v,15));
  printf("\n");
}

static
void
print_gf16(__v128u v){
  printf("%04x ",(uint16_t)vgetq_lane_u16(v,0));
  printf("%04x ",(uint16_t)vgetq_lane_u16(v,1));
  printf("%04x ",(uint16_t)vgetq_lane_u16(v,2));
  printf("%04x ",(uint16_t)vgetq_lane_u16(v,3));
  printf("%04x ",(uint16_t)vgetq_lane_u16(v,4));
  printf("%04x ",(uint16_t)vgetq_lane_u16(v,5));
  printf("%04x ",(uint16_t)vgetq_lane_u16(v,6));
  printf("%04x", (uint16_t)vgetq_lane_u16(v,7));
  printf("\n");
}

void print_var(char* a, v128u v){
  printf("%s: ", a);
  print_v128u(v);
}

__v128u gf16_blend_mask(__v128u a, __v128u b, __v128u mask){
  __v128u tmp = vdupq_n_u16(0);
  uint16_t t0, t1, t2, t3, t4, t5, t6, t7;
  // if mask[i] & f0 -> b, else mask[i] -> a
  t0  = vgetq_lane_u16(mask,0);
  t1  = vgetq_lane_u16(mask,1);
  t2  = vgetq_lane_u16(mask,2);
  t3  = vgetq_lane_u16(mask,3);
  t4  = vgetq_lane_u16(mask,4);
  t5  = vgetq_lane_u16(mask,5);
  t6  = vgetq_lane_u16(mask,6);
  t7  = vgetq_lane_u16(mask,7);
  //printf("** %04x %04x %04x %04x %04x %04x %04x %04x\n",t0,t1,t2,t3,t4,t5,t6,t7);
  tmp = t0  & 0x8000 ? vsetq_lane_u16(vgetq_lane_u16(b,0),tmp,0)   : vsetq_lane_u16(vgetq_lane_u16(a,0),tmp,0);
  tmp = t1  & 0x8000 ? vsetq_lane_u16(vgetq_lane_u16(b,1),tmp,1)   : vsetq_lane_u16(vgetq_lane_u16(a,1),tmp,1);
  tmp = t2  & 0x8000 ? vsetq_lane_u16(vgetq_lane_u16(b,2),tmp,2)   : vsetq_lane_u16(vgetq_lane_u16(a,2),tmp,2);
  tmp = t3  & 0x8000 ? vsetq_lane_u16(vgetq_lane_u16(b,3),tmp,3)   : vsetq_lane_u16(vgetq_lane_u16(a,3),tmp,3);
  tmp = t4  & 0x8000 ? vsetq_lane_u16(vgetq_lane_u16(b,4),tmp,4)   : vsetq_lane_u16(vgetq_lane_u16(a,4),tmp,4);
  tmp = t5  & 0x8000 ? vsetq_lane_u16(vgetq_lane_u16(b,5),tmp,5)   : vsetq_lane_u16(vgetq_lane_u16(a,5),tmp,5);
  tmp = t6  & 0x8000 ? vsetq_lane_u16(vgetq_lane_u16(b,6),tmp,6)   : vsetq_lane_u16(vgetq_lane_u16(a,6),tmp,6);
  tmp = t7  & 0x8000 ? vsetq_lane_u16(vgetq_lane_u16(b,7),tmp,7)   : vsetq_lane_u16(vgetq_lane_u16(a,7),tmp,7);
  return tmp;
}



v128u blendv(v128u a, v128u b, v128u mask){
  v128u tmp = vdupq_n_u8(0);
  uint8_t t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15;
  // if mask[i] & f0 -> b, else mask[i] -> a
  t0  = vgetq_lane_u8(mask,0);
  t1  = vgetq_lane_u8(mask,1);
  t2  = vgetq_lane_u8(mask,2);
  t3  = vgetq_lane_u8(mask,3);
  t4  = vgetq_lane_u8(mask,4);
  t5  = vgetq_lane_u8(mask,5);
  t6  = vgetq_lane_u8(mask,6);
  t7  = vgetq_lane_u8(mask,7);
  t8  = vgetq_lane_u8(mask,8);
  t9  = vgetq_lane_u8(mask,9);
  t10 = vgetq_lane_u8(mask,10);
  t11 = vgetq_lane_u8(mask,11);
  t12 = vgetq_lane_u8(mask,12);
  t13 = vgetq_lane_u8(mask,13);
  t14 = vgetq_lane_u8(mask,14);
  t15 = vgetq_lane_u8(mask,15);

  tmp = t0  & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,0),tmp,0)   : vsetq_lane_u8(vgetq_lane_u8(a,0),tmp,0);
  tmp = t1  & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,1),tmp,1)   : vsetq_lane_u8(vgetq_lane_u8(a,1),tmp,1);
  tmp = t2  & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,2),tmp,2)   : vsetq_lane_u8(vgetq_lane_u8(a,2),tmp,2);
  tmp = t3  & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,3),tmp,3)   : vsetq_lane_u8(vgetq_lane_u8(a,3),tmp,3);
  tmp = t4  & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,4),tmp,4)   : vsetq_lane_u8(vgetq_lane_u8(a,4),tmp,4);
  tmp = t5  & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,5),tmp,5)   : vsetq_lane_u8(vgetq_lane_u8(a,5),tmp,5);
  tmp = t6  & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,6),tmp,6)   : vsetq_lane_u8(vgetq_lane_u8(a,6),tmp,6);
  tmp = t7  & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,7),tmp,7)   : vsetq_lane_u8(vgetq_lane_u8(a,7),tmp,7);
  tmp = t8  & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,8),tmp,8)   : vsetq_lane_u8(vgetq_lane_u8(a,8),tmp,8);
  tmp = t9  & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,9),tmp,9)   : vsetq_lane_u8(vgetq_lane_u8(a,9),tmp,9);
  tmp = t10 & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,10),tmp,10) : vsetq_lane_u8(vgetq_lane_u8(a,10),tmp,10);
  tmp = t11 & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,11),tmp,11) : vsetq_lane_u8(vgetq_lane_u8(a,11),tmp,11);
  tmp = t12 & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,12),tmp,12) : vsetq_lane_u8(vgetq_lane_u8(a,12),tmp,12);
  tmp = t13 & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,13),tmp,13) : vsetq_lane_u8(vgetq_lane_u8(a,13),tmp,13);
  tmp = t14 & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,14),tmp,14) : vsetq_lane_u8(vgetq_lane_u8(a,14),tmp,14);
  tmp = t15 & 0x80 ? vsetq_lane_u8(vgetq_lane_u8(b,15),tmp,15) : vsetq_lane_u8(vgetq_lane_u8(a,15),tmp,15);

  return tmp;
}

static
inline
void 
setup_htable_gf16(gf_16_t m, uint32_t prim_poly, __v128u & htbl, __v128u & ltbl) {
  __v128u lv[4];
  __v128u hv[4];
  
  //could do this with shifts, but unroll faster?
  //have 4 in case processor wants to parrelize better possibly
  uint16x4_t tmp0 = vcreate_u16(0x0007000600050004);
  uint16x4_t tmp1 = vcreate_u16(0x0003000200010000);
  uint16x4_t tmp2 = vcreate_u16(0x000f000e000d000c);
  uint16x4_t tmp3 = vcreate_u16(0x000b000a00090008);
  lv[0] = vcombine_u16(tmp1,tmp0);
  lv[1] = vcombine_u16(tmp3,tmp2);
  tmp0 = vcreate_u16(0x0070006000500040);
  tmp1 = vcreate_u16(0x0030002000100000);
  tmp2 = vcreate_u16(0x00f000e000d000c0);
  tmp3 = vcreate_u16(0x00b000a000900080);
  lv[2] = vcombine_u16(tmp1,tmp0);
  lv[3] = vcombine_u16(tmp3,tmp2);
  tmp0 = vcreate_u16(0x0700060005000400);
  tmp1 = vcreate_u16(0x0300020001000000);
  tmp2 = vcreate_u16(0x0f000e000d000c00);
  tmp3 = vcreate_u16(0x0b000a0009000800);
  hv[0] = vcombine_u16(tmp1,tmp0);
  hv[1] = vcombine_u16(tmp3,tmp2);
  tmp0 = vcreate_u16(0x7000600050004000);
  tmp1 = vcreate_u16(0x3000200010000000);
  tmp2 = vcreate_u16(0xf000e000d000c000);
  tmp3 = vcreate_u16(0xb000a00090008000);
  hv[2] = vcombine_u16(tmp1,tmp0);
  hv[3] = vcombine_u16(tmp3,tmp2);
  
  /*
  printf("----------\n");
  print_gf16(lv[0]);
  print_gf16(lv[1]);
  print_gf16(lv[2]);
  print_gf16(lv[3]);
  print_gf16(hv[0]);
  print_gf16(hv[1]);
  print_gf16(hv[2]);
  print_gf16(hv[3]);
  printf("----------\n");
  */
  
  int left_bit = __builtin_clz ((uint32_t)m) - 16;
  int i;

  __v128u     poly = vdupq_n_u16((int32_t)(prim_poly & 0xffff));
  __v128u     zero = vdupq_n_u16(0);
  uint64_t    mul = (uint64_t)m * 0x0001000100010001ULL;
  __v128u     t1;

  for (i = 15; i >= left_bit; --i) {
    tmp0 = vcreate_u16(mul << (uint64_t)i);
    t1 = vcombine_u16(tmp0,tmp0);
    (&ltbl)[0] = veorq_u16((&ltbl)[0], gf16_blend_mask(zero, lv[0], t1));
    (&htbl)[0] = veorq_u16((&htbl)[0], gf16_blend_mask(zero, hv[0], t1));
    t1 = gf16_blend_mask(zero, poly, lv[0]);
    lv[0] = veorq_u16(vaddq_u16(lv[0], lv[0]), t1);
    t1 = gf16_blend_mask(zero, poly, hv[0]);
    hv[0] = veorq_u16(vaddq_u16(hv[0], hv[0]), t1);

    t1 = vcombine_u16(tmp0,tmp0);
    (&ltbl)[1] = veorq_u16((&ltbl)[1], gf16_blend_mask(zero, lv[1], t1));
    (&htbl)[1] = veorq_u16((&htbl)[1], gf16_blend_mask(zero, hv[1], t1));
    t1 = gf16_blend_mask(zero, poly, lv[1]);
    lv[1] = veorq_u16(vaddq_u16(lv[1], lv[1]), t1);
    t1 = gf16_blend_mask(zero, poly, hv[1]);
    hv[1] = veorq_u16(vaddq_u16(hv[1], hv[1]), t1);

    t1 = vcombine_u16(tmp0,tmp0);
    (&ltbl)[2] = veorq_u16((&ltbl)[2], gf16_blend_mask(zero, lv[2], t1));
    (&htbl)[2] = veorq_u16((&htbl)[2], gf16_blend_mask(zero, hv[2], t1));
    t1 = gf16_blend_mask(zero, poly, lv[2]);
    lv[2] = veorq_u16(vaddq_u16(lv[2], lv[2]), t1);
    t1 = gf16_blend_mask(zero, poly, hv[2]);
    hv[2] = veorq_u16(vaddq_u16(hv[2], hv[2]), t1);

    t1 = vcombine_u16(tmp0,tmp0);
    (&ltbl)[3] = veorq_u16((&ltbl)[3], gf16_blend_mask(zero, lv[3], t1));
    (&htbl)[3] = veorq_u16((&htbl)[3], gf16_blend_mask(zero, hv[3], t1));
    t1 = gf16_blend_mask(zero, poly, lv[3]);
    lv[3] = veorq_u16(vaddq_u16(lv[3], lv[3]), t1);
    t1 = gf16_blend_mask(zero, poly, hv[3]);
    hv[3] = veorq_u16(vaddq_u16(hv[3], hv[3]), t1);

    //printf("****** %d *******\n",i);
    //print_gf16((&ltbl)[0]);
    //print_gf16((&htbl)[0]);
    //printf("^^^^^^ %d ^^^^^^^\n",i);


  }
}



static
inline
void
setup_htable (gf_8_t m, uint32_t pp,  uint8x16_t & htbl, uint8x16_t & ltbl) {
  uint8x8_t hi_tmp = vcreate_u8(0x0f0e0d0c0b0a0908LL);
  uint8x8_t lo_tmp = vcreate_u8(0x0706050403020100LL);
  //v128u     lv = vcombine_u8(hi_tmp,lo_tmp);
  v128u     lv = vcombine_u8(lo_tmp,hi_tmp);
  v128u     hv = vshlq_n_u8(lv, 4);
  v128u     poly = vdupq_n_u8((uint8_t) (pp & 0xff));
  v128u     zero = vdupq_n_u8(0);
  uint64_t  mul = (uint64_t)m * 0x0101010101010101ULL;
  v128u     t1;
  int       left_bit = __builtin_clz ((uint32_t)m) - 24;
  int       i;
  ltbl = zero;
  htbl = zero;
  for (i = 7; i > left_bit; --i) {
    //re-using hi_tmp variable.
    hi_tmp = vcreate_u8(mul << (uint64_t)i);
    t1 = vcombine_u8(hi_tmp,hi_tmp);
    ltbl = veorq_u8(ltbl, blendv(zero,lv,t1));
    htbl = veorq_u8(htbl, blendv(zero,hv,t1));
    t1 = blendv(zero,poly,lv);
    lv = veorq_u8(vaddq_u8(lv,lv),t1);
    t1 = blendv(zero,poly,hv);
    hv = veorq_u8(vaddq_u8(hv,hv),t1);
  }
  hi_tmp = vcreate_u8(mul << (uint64_t)i);
  t1 = vcombine_u8(hi_tmp,hi_tmp);
  ltbl = veorq_u8(ltbl, blendv(zero,lv,t1));
  htbl = veorq_u8(htbl, blendv(zero,hv,t1));
}

static
inline
void print_64(uint16x4_t t, uint16x4_t tt){
  printf("%04x %04x %04x %04x ",   vget_lane_u16(t,0),vget_lane_u16(t,1),vget_lane_u16(t,2),vget_lane_u16(t,3));
  printf("%04x %04x %04x %04x\n",  vget_lane_u16(tt,0),vget_lane_u16(tt,1),vget_lane_u16(tt,2),vget_lane_u16(tt,3));
}

static
inline
void print_8(uint8x8_t t){
  printf("%02x %02x %02x %02x ",   vget_lane_u8(t,0),vget_lane_u8(t,1),vget_lane_u8(t,2),vget_lane_u8(t,3));
  printf("%02x %02x %02x %02x\n",  vget_lane_u8(t,4),vget_lane_u8(t,5),vget_lane_u8(t,6),vget_lane_u8(t,7));
}

static
inline
void trans_map(__v128u & a, __v128u & b){
  //need to do some inline asm for transpose which does not have any intrinsics
  //https://community.arm.com/groups/processors/blog/2012/03/13/coding-for-neon--part-5-rearranging-vectors
  register uint16x4_t v0 asm ("q0");
  register uint16x4_t v1 asm ("q1");
  register uint16x4_t v2 asm ("q2");
  register uint16x4_t v3 asm ("q3");
  v0 = vget_low_u16(a);
  v1 = vget_low_u16(b);
  __asm__("VTRN.8 q0,q1");
  v2 = vget_high_u16(a);
  v3 = vget_high_u16(b);
  __asm__("VTRN.8 q2,q3");
  a = vcombine_u16(v0,v2);
  b = vcombine_u16(v1,v3);
}

static
inline
void gf16_to_8(__v128u & a, __v128u & b){
  register uint8x8_t  t0 asm ("d0");
  register uint8x8_t  t1 asm ("d1");
  register uint8x8_t  t2 asm ("d2");
  register uint8x8_t  t3 asm ("d3");
  register uint8x8_t  lk asm ("d4");
  //our look arrays to statically pull low and hi 8's
  //uint64_t lookup_lo = 0x0c0e080a04060002; //0x0e0c0a0806040200; //0x00020406080a0c0e;
  //uint64_t lookup_hi = 0x0d0f090b05070103; //0x01030507090b0d0f;
  uint64_t lookup_lo = 0x0e0c0a0806040200; //0x00020406080a0c0e;
  uint64_t lookup_hi = 0x0f0d0b0907050301;//0x01030507090b0d0f;
  //need to store temp value
  uint16x8_t tmp = a;
  //set double word registers
  t0 = vget_low_u8(vreinterpretq_u8_u16(a));
  t1 = vget_high_u8(vreinterpretq_u8_u16(a));
  t2 = vget_low_u8(vreinterpretq_u8_u16(b));
  t3 = vget_high_u8(vreinterpretq_u8_u16(b));
  //create our lookup
  lk = vcreate_u8(lookup_lo);
  //double lookup to grab from both 128 bit vectors
  t0 = vtbx2_u8(t0,{t0,t1},lk);
  t1 = vtbx2_u8(t1,{t2,t3},lk);
  //save the result back to a.
  a = vreinterpretq_u16_u8(vcombine_u8(t0,t1));
  //rinse and repeat
  t0 = vget_low_u8(vreinterpretq_u8_u16(tmp));
  t1 = vget_high_u8(vreinterpretq_u8_u16(tmp));
  lk = vcreate_u8(lookup_hi);
  t2 = vtbx2_u8(t2,{t2,t3},lk);
  t3 = vtbx2_u8(t3,{t0,t1},lk);
  b = vreinterpretq_u16_u8(vcombine_u8(t3,t2));

}

//homomorphic operation - stdmap to altmap, altmap to stdmap
static
inline
void to_altmap(__v128u & l, __v128u & h){
  //trans_map((&l)[0],(&l)[1]);
  //trans_map((&l)[2],(&l)[3]);
  //trans_map((&h)[0],(&h)[1]);
  //trans_map((&h)[2],(&h)[3]);
  gf16_to_8((&l)[0],(&l)[1]);
  gf16_to_8((&l)[2],(&l)[3]);
  gf16_to_8((&h)[0],(&h)[1]);
  gf16_to_8((&h)[2],(&h)[3]);
  /*
  gf16_to_8((&l)[0],(&h)[0]);
  gf16_to_8((&l)[1],(&h)[1]);
  gf16_to_8((&l)[2],(&h)[2]);
  gf16_to_8((&l)[3],(&h)[3]);
  */
}


static
inline
uint8x8x2_t uint8x16_to_8x8x2(v128u v){
  return uint8x8x2_t({vget_low_u8(v), vget_high_u8(v)});
}

static
inline
v128u shuffle(v128u a, v128u b){
  return vcombine_u8(vtbl2_u8(uint8x16_to_8x8x2(a), vget_low_u8(b)),vtbl2_u8(uint8x16_to_8x8x2(a),
vget_high_u8(b)));
}

static
inline 
v128u gf8_mult_const_16(v128u v, v128u htbl, v128u ltbl, v128u loset) {

  v128u r_lo, r_hi, hi, lo;
  // compute the bitwise and between 2 128b values _mm_and_si128
  lo = vandq_u8(loset, v);
  // shift right _mm_srli_epi64, then and with loset
  hi = vandq_u8(loset, vshrq_n_u8(v, 4));
  // shuffle the contects of ltbl according to lo _mm_shuffle_epi8
  r_lo = shuffle(ltbl, lo);
  //print_var("lo",r_lo);
  r_hi = shuffle(htbl, hi);
  //print_var("hi",r_hi);
  //return xor of lo and hi bit vectors
  return (veorq_u8(r_lo, r_hi));
}

//vreinterpretq_u16_u8 seems to put my 8's as 103254...
static
inline
__v128u flip(v128u a){
  register uint16x4_t v2 asm ("q0");
  register uint16x4_t v3 asm ("q1");
  register uint8x8_t v0 asm ("q2");
  register uint8x8_t v1 asm ("q3");
  v0 = vget_low_u8(a);
  v1 = vget_high_u8(a);
  __asm__("vmov q0,q2");
  __asm__("vmov q1,q3");
  
  __asm__("VREV16.8 q0,q0");
  __asm__("VREV16.8 q1,q1");
  return vcombine_u16(v2,v3);
}

//rehash of gf8 version
static
inline
__v128u gf16_mult_const_8(__v128u v, __v128u *htbl, __v128u *ltbl, __v128u loset) {

  v128u res_l = vdupq_n_u8(0);
  v128u res_h = vdupq_n_u8(0);
  v128u bit_loc;

  bit_loc = vreinterpretq_u8_u16(vandq_u16(loset, v));
  //print_var("bit_l",bit_loc);
  res_l = shuffle(vreinterpretq_u8_u16(ltbl[0]),bit_loc);
  //print_var("res_l",res_l);
  res_h = shuffle(vreinterpretq_u8_u16(ltbl[1]),bit_loc);
  //print_var("res_h",res_h);
  
  bit_loc = vreinterpretq_u8_u16(vandq_u16(loset, vshrq_n_u16(v,4)));
  //print_var("bit_l",bit_loc);
  res_l = veorq_u8(res_l, shuffle(vreinterpretq_u8_u16(ltbl[2]),bit_loc));
  //print_var("res_l",res_l);
  res_h = veorq_u8(res_h, shuffle(vreinterpretq_u8_u16(ltbl[3]),bit_loc));
  //print_var("res_h",res_h);

  bit_loc = vreinterpretq_u8_u16(vandq_u16(loset, vshrq_n_u16(v,8)));
  //print_var("bit_l",bit_loc);
  res_l = veorq_u8(res_l, shuffle(vreinterpretq_u8_u16(htbl[0]),bit_loc));
  //print_var("res_l",res_l);
  res_h = veorq_u8(res_h, shuffle(vreinterpretq_u8_u16(htbl[1]),bit_loc));
  //print_var("res_h",res_h);

  bit_loc = vreinterpretq_u8_u16(vandq_u16(loset, vshrq_n_u16(v,12)));
  //print_var("bit_l",bit_loc);
  res_l = veorq_u8(res_l, shuffle(vreinterpretq_u8_u16(htbl[2]),bit_loc));
  //print_var("res_l",res_l);
  res_h = veorq_u8(res_h, shuffle(vreinterpretq_u8_u16(htbl[3]),bit_loc));
  //print_var("res_h",res_h);

  res_h = vreinterpretq_u8_u16(vshlq_n_u16(vreinterpretq_u16_u8(res_h),8));
  __v128u res16 = vreinterpretq_u16_u8(veorq_u8(res_l,res_h));

  return res16;
}

template<class gf_w, class gf_state>
uint32_t
mul_logs(uint32_t a, uint32_t b, const gf_state & state){
  return  state.mul ((gf_w)a,(gf_w)b);
}


int main (int argc, char * argv[]){
  v128u     htbl = vdupq_n_u8(0);
  v128u     ltbl = vdupq_n_u8(0);
  uint8_t poly = 0035;
  v128u loset = vdupq_n_u8(0x0f);
  v128u mul;
  uint8_t res1, res2;

  gf8_log_state gf8_log_field ((uint32_t)0035, (gf_8_t)1);

  for(int i=0; i <256;++i){
    setup_htable(i, poly, htbl, ltbl);
    for(int j=0; j <256;++j){
      mul = vdupq_n_u8(j);
      res1 = (uint8_t)vgetq_lane_u8(gf8_mult_const_16(mul,htbl,ltbl,loset),0);
      res2 = (uint8_t)mul_logs<gf_8_t,gf8_log_state>(i,j, gf8_log_field);
      if (res1 != res2){
        printf("i: %2x, j: %2x\n", i,j);
        printf("neon: %2x, log: %2x\n", res1,res2);
        print_var((char*)"ltbl",ltbl);
        print_var((char*)"htbl",htbl);
        exit(0);
      }
    }
  }

  __v128u     ghtbl[4];
  __v128u     gltbl[4];
  uint16_t    prim_poly = 0x100b;
  __v128u     mask = vdupq_n_u16(0x000f);
  //__v128u     zero = vdupq_n_u16(0);

  gf16_log_state gf16_log_field ((uint32_t)0x100b, (gf_16_t)1);
  __v128u mult;
  uint16_t res3, res4;
  int i_min = 0x0000;
  int i_max = 0xffff;
  int j_min = 0x0000;
  int j_max = 0xffff;

  for(int i=i_min; i <i_max;++i){
    ghtbl[0] = vdupq_n_u16(0);
    ghtbl[1] = vdupq_n_u16(0);
    ghtbl[2] = vdupq_n_u16(0);
    ghtbl[3] = vdupq_n_u16(0);
    gltbl[0] = vdupq_n_u16(0);
    gltbl[1] = vdupq_n_u16(0);
    gltbl[2] = vdupq_n_u16(0);
    gltbl[3] = vdupq_n_u16(0);

    setup_htable_gf16(i, prim_poly, *ghtbl, *gltbl);
    to_altmap(*gltbl,*ghtbl);
    //gf16_to_8(gltbl[0],gltbl[1]);
    //to_altmap(*gltbl,*ghtbl);
    /*
    printf("Before Mapping:\n");
    for(int i=0;i<4;++i){
      print_gf16(gltbl[i]);
    }
    for(int i=0;i<4;++i){
      print_gf16(ghtbl[i]);
    }
    */
    /*
    printf("After Mapping:\n");
    for(int i=0;i<4;++i){
      print_gf16(gltbl[i]);
    }
    for(int i=0;i<4;++i){
      print_gf16(ghtbl[i]);
    }
    */
    for(int j=j_min; j <j_max;++j){
      mult = vdupq_n_u16(j);
      res3 = (uint16_t)vgetq_lane_u16(gf16_mult_const_8(mult,ghtbl,gltbl,mask),0);
      res4 = (uint16_t)mul_logs<gf_16_t,gf16_log_state>(i,j, gf16_log_field);
      if (res3 != res4){
        printf("i: %04x, j: %04x\n", i,j);
        printf("neon: %04x, log: %04x\n", res3,res4);
        for(int i=0;i<4;++i){
          print_gf16(gltbl[i]);
        }
        for(int i=0;i<4;++i){
          print_gf16(ghtbl[i]);
        }
        exit(0);
      }
    }
  }
  printf("ALL CORRECT!\n"); 
}
