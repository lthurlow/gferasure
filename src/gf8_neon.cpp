#include <arm_neon.h>
#include <string.h>
#include <stdio.h>

#include "gf8_neon.h"

typedef uint8x16_t v128u;
typedef uint8x8x2_t _v128u;

v128u blendv2(v128u a, v128u mask){
  v128u tmp = vreinterpretq_u8_s8(vshrq_n_s8( vreinterpretq_s8_u8(mask), 8));
  return(vandq_u8(tmp,a));
}

//http://stackoverflow.com/questions/7962141/converting-between-sse-and-neon-intrinsics-shuffling
//modified for wpedentic on compound literals
//#define uint8x16_to_8x8x2(v) ((uint8x8x2_t) { vget_low_u8(v), vget_high_u8(v) })
static
inline
uint8x8x2_t uint8x16_to_8x8x2(v128u v){
  return uint8x8x2_t({vget_low_u8(v), vget_high_u8(v)});
}

static
inline
void
setup_htable (gf_8_t m, uint32_t pp,  _v128u & htbl, _v128u & ltbl) {
  uint8x8_t hi_tmp = vcreate_u8(0x0f0e0d0c0b0a0908LL);
  uint8x8_t lo_tmp = vcreate_u8(0x0706050403020100LL);
  v128u     lv = vcombine_u8(lo_tmp,hi_tmp);
  v128u     hv = vshlq_n_u8(lv, 4);
  v128u     poly = vdupq_n_u8((uint8_t) (pp & 0xff));
  v128u     zero = vdupq_n_u8(0);
  uint64_t  mul = (uint64_t)m * 0x0101010101010101ULL;
  v128u     t1;
  int       left_bit = __builtin_clz ((uint32_t)m) - 24;
  int       i;
  ltbl = uint8x16_to_8x8x2(zero);
  htbl = uint8x16_to_8x8x2(zero);
  for (i = 7; i > left_bit; --i) {
    //re-using hi_tmp variable.
    hi_tmp = vcreate_u8(mul << (uint64_t)i);
    t1 = vcombine_u8(hi_tmp,hi_tmp);
    ltbl = uint8x16_to_8x8x2(veorq_u8(vcombine_u8(ltbl.val[0],ltbl.val[1]), blendv2(lv,t1)));
    htbl = uint8x16_to_8x8x2(veorq_u8(vcombine_u8(htbl.val[0],htbl.val[1]), blendv2(hv,t1)));
    t1 = blendv2(poly,lv);
    lv = veorq_u8(vaddq_u8(lv,lv),t1);
    t1 = blendv2(poly,hv);
    hv = veorq_u8(vaddq_u8(hv,hv),t1);
  }
  hi_tmp = vcreate_u8(mul << (uint64_t)i);
  t1 = vcombine_u8(hi_tmp,hi_tmp);
  ltbl = uint8x16_to_8x8x2(veorq_u8(vcombine_u8(ltbl.val[0],ltbl.val[1]), blendv2(lv,t1)));
  htbl = uint8x16_to_8x8x2(veorq_u8(vcombine_u8(htbl.val[0],htbl.val[1]), blendv2(hv,t1)));
}

static
inline
v128u shuf(_v128u a, v128u b){
  return vcombine_u8(vtbl2_u8
                      (a, vget_low_u8(b)),
                     vtbl2_u8
                      (a, vget_high_u8(b)));
}

static
inline 
v128u gf8_mult_const(v128u v, _v128u htbl, _v128u ltbl, v128u loset) {

  v128u r_lo, r_hi, hi, lo;
  // compute the bitwise and between 2 128b values _mm_and_si128
  lo = vandq_u8(loset, v);
  // shift right _mm_srli_epi64, then and with loset
  hi = vandq_u8(loset, vshrq_n_u8(v, 4));
  // shuffle the contects of ltbl according to lo _mm_shuffle_epi8
  r_lo = shuf(ltbl, lo);
  r_hi = shuf(htbl, hi);

  //return xor of lo and hi bit vectors
  return (veorq_u8(r_lo, r_hi));
}

void
gf8_neon_state::mul_region(const gf_8_t * src, gf_8_t * dst, uint64_t bytes,
                             gf_8_t multiplier, bool accum) const {

  if (multiplier == (gf_8_t)0) {
    if (! accum) {
      memset (dst, 0, bytes);
    }
    return;
  } else if (multiplier == (gf_8_t)1){
    if (! accum) {
      memcpy (dst, src, bytes);
    } else {
      uint64_t    chunks = bytes >> 4ULL;
      for (uint64_t i = 0; i < chunks; i++) {
        v128u x = vld1q_u8(src+i*16);
        v128u y = vld1q_u8(dst+i*16);
        vst1q_u8(dst + i*16, veorq_u8(x,y));
      }
    }
    return;
  }

  //v128u       htbl, ltbl, v;
  v128u       v;
  _v128u      htbl1, ltbl1;
  uint64_t    i;
  uint64_t    chunks = bytes >> 4ULL;
  setup_htable (multiplier, prim_poly, htbl1, ltbl1);

  //htbl1 = uint8x16_to_8x8x2(htbl);
  //ltbl1 = uint8x16_to_8x8x2(ltbl);

  v128u       loset = vdupq_n_u8(0x0f);

  if (accum) {
    for (i = 0; i < chunks; ++i) {
      // load a 128 bit value, it does not need to be alinged _mm_loadu_si128
      v = vld1q_u8(src+i*16);
      v = veorq_u8(vld1q_u8(dst + i*16),
                         //gf8_mult_const_16 (v, htbl, ltbl, loset));
                         gf8_mult_const(v, htbl1, ltbl1, loset));
      //store the value back into dst
      vst1q_u8(dst + i*16, v);
    }
  } else {
    for (i = 0; i < chunks; ++i) {
      v = vld1q_u8(src + i*16);
      //v = gf8_mult_const_16(v, htbl, ltbl, loset);
      v = gf8_mult_const(v, htbl1, ltbl1, loset);
      vst1q_u8(dst + i*16, v);
    }
  }
  for (i = chunks << 4ULL; i < bytes; ++i) {
    dst[i] = mul (src[i], multiplier) ^ (accum ? dst[i] : (gf_8_t)0);
  }
}

static inline
void mul_vec_by_vec_rnd(v128u &r, v128u v1, v128u & v2, v128u poly, const int pos){
    v128u t1;
    t1=vshlq_n_u8(v1, pos);
    r=veorq_u8(r, blendv2(v2, t1));
    t1=blendv2(poly, v2);
    v2=veorq_u8(vaddq_u8(v2, v2), t1);
}

static
inline
v128u mul_vec_by_vec(v128u v1, v128u v2, v128u pp){
    v128u r;
    r=vdupq_n_u8((uint8_t)0);
    //must be loop unrolled because vshlq_n_u8 only accepts constants(neon's shift instructions don't take a register as an "argument"
    mul_vec_by_vec_rnd(r, v1, v2, pp, 7);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 6);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 5);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 4);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 3);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 2);
    mul_vec_by_vec_rnd(r, v1, v2, pp, 1);
    r=veorq_u8(r, blendv2(v2, v1));
    return r;
}

void
gf8_neon_state::mul_region_region (const gf_8_t * buf1, const gf_8_t * buf2, gf_8_t * dst,
                                    uint64_t bytes, bool accum) const {
    uint64_t    i;
    uint64_t    chunks = bytes >> 4ULL;
    v128u       v1, v2, r, pp;

    pp = vdupq_n_u8((uint8_t)prim_poly);
    if (accum) {
        for (i = 0; i < chunks; ++i) {
            v1 = vld1q_u8(buf1 + 16*i);
            v2 = vld1q_u8(buf2 + 16*i);
            r = veorq_u8 (vld1q_u8(dst + 16*i),
                                                mul_vec_by_vec (v1, v2, pp));
            vst1q_u8 (dst + 16*i, r);
        }
        for (i = (chunks * 16); i < bytes; ++i) {
            dst[i] = add (dst[i], mul (buf1[i], buf2[i]));
        }
    } else {
        for (i = 0; i < chunks; ++i) {
            v1 = vld1q_u8 (buf1 + 16*i);
            v2 = vld1q_u8 (buf2 + 16*i);
            r = mul_vec_by_vec (v1, v2, pp);
            vst1q_u8 (dst + 16*i, r);
        }
        for (i = (chunks * 16); i < bytes; ++i) {
            dst[i] = mul (buf1[i], buf2[i]);
        }
    }

}

gf_8_t
gf8_neon_state::dotproduct (const gf_8_t * src1, const gf_8_t * src2,
                             uint64_t n_elements, uint64_t stride) const{
    gf_8_t result=(gf_8_t)0;
    uint64_t i;
    uint64_t chunks;
    v128u r;
    v128u accumulator=vdupq_n_u8((uint8_t)0);
    v128u pp = vdupq_n_u8((uint8_t) (prim_poly&0xFF));
    if(stride==1){
      chunks=n_elements>>4ULL;
      for(i=0;i<chunks;++i){
          r=mul_vec_by_vec(vld1q_u8(src1+16*i), vld1q_u8(src2+16*i),pp);
          accumulator=veorq_u8(r, accumulator);
      }
      for(i=chunks*16; i<n_elements; ++i){
          result^=mul(src1[i],src2[i]);
      }
      result^=vgetq_lane_u8(accumulator,0);
      result^=vgetq_lane_u8(accumulator,1);
      result^=vgetq_lane_u8(accumulator,2);
      result^=vgetq_lane_u8(accumulator,3);
      result^=vgetq_lane_u8(accumulator,4);
      result^=vgetq_lane_u8(accumulator,5);
      result^=vgetq_lane_u8(accumulator,6);
      result^=vgetq_lane_u8(accumulator,7);
      result^=vgetq_lane_u8(accumulator,8);
      result^=vgetq_lane_u8(accumulator,9);
      result^=vgetq_lane_u8(accumulator,10);
      result^=vgetq_lane_u8(accumulator,11);
      result^=vgetq_lane_u8(accumulator,12);
      result^=vgetq_lane_u8(accumulator,13);
      result^=vgetq_lane_u8(accumulator,14);
      result^=vgetq_lane_u8(accumulator,15);
    }else if(stride==2){
    }

    return result;
}
