/*
 * gfw_shiftadd.cpp
 *
 * Routines for n-bit Galois fields using simple shift and add.  These should be
 * independent of field width, assuming that there's a function that returns element
 * size in bits.
 *
 */

#include <string.h>
#include "gfw_shiftadd.h"

#include <stdio.h>

template<class gf_w_t>
gf_w_t
gfw_shiftadd_state<gf_w_t>::mul (gf_w_t a, gf_w_t b) const
{
    gf_w_t      accum = (b & (gf_w_t)1) ? a : (gf_w_t)0;

    for (unsigned i = 1; i < bitwidth(); ++i) {
        a = mul_by_2 (a);
        b >>= 1;
        accum = add (accum, (b & (gf_w_t)1) ? a : 0);
    }
    return accum;
}

template<class gf_w_t>
gf_w_t
gfw_shiftadd_state<gf_w_t>::pow (gf_w_t a, uint64_t b) const
{
    gf_w_t      accum = (b & 1) ? a : (gf_w_t)1;
    gf_w_t      powers = a;

    b >>= 1;
    while (b != 0) {
        powers = mul (powers, powers);
        if (b & 1) {
            accum = mul (accum, powers);
        }
        b >>= 1;
    }
    return accum;
}

template<class gf_w_t>
gf_w_t
gfw_shiftadd_state<gf_w_t>::inv (gf_w_t a) const
{
    return (a < 2) ? a : pow (a, (0xffffffffffffffffULL >> (64ULL - bitwidth())) ^ 0x1);
}

template<class gf_w_t>
void
gfw_shiftadd_state<gf_w_t>::mul_region (const gf_w_t * src, gf_w_t * dst, uint64_t bytes,
                                        gf_w_t multiplier, bool accum) const
{
    /*Faster without the ton of branches slightly higher average, reduced std dev*/
    if (multiplier == (gf_w_t)0) {
        if (! accum) {
            memset (dst, 0, bytes);
        }
        return;
    }
    else if (multiplier == (gf_w_t)1) {
        if (! accum){
            memcpy (dst, src, bytes);
        }
        else {
            for (uint64_t i = 0; i < bytes / sizeof (gf_w_t); i++) {
                dst[i] = add (dst[i], src[i]);
            }
        }
        return;
    }

    if (accum) {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            dst[i] = add (dst[i], mul (multiplier, src[i]));
        }
    } else {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            dst[i] = mul (multiplier, src[i]);
        }
    }
}

template<class gf_w_t>
void
gfw_shiftadd_state<gf_w_t>::mul_region_region ( const gf_w_t * src1, const gf_w_t * src2,
                                          gf_w_t * dst, uint64_t bytes, bool accum) const {

    if (accum) {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            dst[i] = add (dst[i], mul (src1[i], src2[i]));
        }
    } else {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            dst[i] = mul (src1[i], src2[i]);
        }
    }
}

template<class gf_w_t>
gf_w_t
gfw_shiftadd_state<gf_w_t>::dotproduct (const gf_w_t * src1, const gf_w_t * src2,
                                  uint64_t n_elements, uint64_t stride) const
{
    gf_w_t             dp = (gf_w_t)0;

    for (uint64_t i = 0; i < n_elements; ++i) {
        dp = add (dp, mul (*src1, *src2));
        src1 += stride;
        src2 += stride;
    }
    return dp;
}

template struct gfw_shiftadd_state<gf_8_t>;
template struct gfw_shiftadd_state<gf_16_t>;
template struct gfw_shiftadd_state<gf_32_t>;
template struct gfw_shiftadd_state<gf_64_t>;

gf8_shiftadd_state      gf8_shiftadd_field  (gf8_default_primpoly, (gf_8_t)1);
gf16_shiftadd_state     gf16_shiftadd_field (gf16_default_primpoly, (gf_8_t)1);
gf32_shiftadd_state     gf32_shiftadd_field (gf32_default_primpoly, (gf_8_t)1);
gf64_shiftadd_state     gf64_shiftadd_field (gf64_default_primpoly, (gf_8_t)1);
