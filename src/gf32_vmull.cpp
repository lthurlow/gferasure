/*
 * gf32_vmull.cpp
 *
 * Routines for 64-bit Galois fields using NEON.
 *
 */

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <arm_neon.h>

#include "gf32_vmull.h"

typedef poly16x8_t __v16p;
typedef poly8x8_t  __v8x8p;
typedef uint8x8_t  __v8x8u;
typedef uint8x16_t __v8u;
typedef uint16x8_t __v16u;
typedef uint64x2_t __v64u;
typedef uint32x4_t __v32u;
typedef uint32x2_t __32u;

#ifdef GF_NEON32
static
inline
uint16x8_t
mul64(uint32_t a32, uint32_t b32){
  uint64x1_t a = vreinterpret_u64_u32(vdup_n_u32((uint32_t)a32));
  poly8x8_t  b = vcreate_p8(b32);
  poly16x8_t res0, res1, res2, res3, res4;
  uint64x2_t mask1 =vcombine_u64(vcreate_u64(0x000000000000ffff),vcreate_u64(0));
  uint64x2_t mask2 =vcombine_u64(vcreate_u64(0x00000000ffffffff),vcreate_u64(0));
  uint64x2_t mask3 =vcombine_u64(vcreate_u64(0xffff000000000000),vcreate_u64(0));
  
  //no shifting necessary
  res0 =  vmull_p8(vcreate_p8(vget_lane_u32(vreinterpret_u32_u64(a),0)), b);

  //shift a by 8 for next val to mul
  a = vshr_n_u64(a,8);
  res1 =  vmull_p8(vcreate_p8(vget_lane_u32(vreinterpret_u32_u64(a),0)), b);

  a = vshr_n_u64(a,8);
  res2 =  vmull_p8(vcreate_p8(vget_lane_u32(vreinterpret_u32_u64(a),0)), b);

  a = vshr_n_u64(a,8);
  res3 =  vmull_p8(vcreate_p8(vget_lane_u32(vreinterpret_u32_u64(a),0)), b);

  //mask3 = vcombine_u64(vcreate_u64(0xffff000000000000),vcreate_u64(0));
  //shift first 24 bits left by 8
  res4 = vreinterpretq_p16_u64(
            vshlq_n_u64(
              vandq_u64(vreinterpretq_u64_p16(res1),
              vreinterpretq_u64_u16(
                vmvnq_u16(vreinterpretq_u16_u64(mask3)))),8));
  //shift the last 8 bits right by 24
  res1 = vreinterpretq_p16_u64(vshrq_n_u64(
            vandq_u64(vreinterpretq_u64_p16(res1),mask3),24));
  res1 = vreinterpretq_p16_u64(veorq_u64(vreinterpretq_u64_p16(res1),vreinterpretq_u64_p16(res4)));

  //mask2 = vcombine_u64(vcreate_u64(0x00000000ffffffff),vcreate_u64(0));
  // shift the first 32 bits by 16 to left
  res4 = vreinterpretq_p16_u64(vshlq_n_u64(
            vandq_u64(mask2, vreinterpretq_u64_p16(res2)),16));
  // shift second 32 bits by 16 to right, xor together
  res2 = vreinterpretq_p16_u64(vshrq_n_u64(
            vandq_u64(vreinterpretq_u64_u16(
              vmvnq_u16(vreinterpretq_u16_u64(mask2))), 
              vreinterpretq_u64_p16(res2)),16));
  res2 = vreinterpretq_p16_u64(veorq_u64(vreinterpretq_u64_p16(res2),vreinterpretq_u64_p16(res4)));

  //mask1 = vcombine_u64(vcreate_u64(0x000000000000ffff),vcreate_u64(0));
  //shift last 24 bits right by 8
  res4 = vreinterpretq_p16_u64(
            vshrq_n_u64(
              vandq_u64(vreinterpretq_u64_p16(res3),
              vreinterpretq_u64_u16(
                vmvnq_u16(vreinterpretq_u16_u64(mask1)))),8));
  //shift first 8 bits left by 24
  res3 = vreinterpretq_p16_u64(vshlq_n_u64(
            vandq_u64(vreinterpretq_u64_p16(res3),mask1),24));
  res3 = vreinterpretq_p16_u64(veorq_u64(vreinterpretq_u64_p16(res3),vreinterpretq_u64_p16(res4)));

  res0 = vreinterpretq_p16_u64(veorq_u64(vreinterpretq_u64_p16(res0),
            veorq_u64(vreinterpretq_u64_p16(res1),
                      vreinterpretq_u64_p16(res3))));

  res0 = vreinterpretq_p16_u64(veorq_u64(vreinterpretq_u64_p16(res0),
  vreinterpretq_u64_p16(res2)));

  return vreinterpretq_u16_p16(res0);
}

#elif GF_NEON64
static
inline
uint16x8_t
mul64(uint32_t a32, uint32_t b32){
  return vmull.p32(a,b);
}
#endif
gf_32_t
gf32_vmull_state::mul(gf_32_t a32, gf_32_t b32) const {
  uint64x1_t a  = vcreate_u64(a32);
  uint64x1_t b  = vcreate_u64(b32);
  uint64x1_t pp = vcreate_u64(prim_poly & 0xffffULL);
  uint64x1_t mk = vcreate_u64(0x00000000ffff0000);
  uint64x1_t v;
  uint16x8_t w, result;

  result = mul64(a, b);
  v = vshr_n_u64(vget_low_u64(vreinterpretq_u64_u16(result)),32);
  v = vand_u64(v,mk);
  w = mul64(pp,v);

  result = veorq_u16(result, w);
  v = vshr_n_u64(vget_low_u64(vreinterpretq_u64_u16(result)),32);
  v = vand_u64(v,vshr_n_u64(mk,16));
  w = mul64(pp,v);

  result = veorq_u16(result, w);
  return (vgetq_lane_u32(vreinterpretq_u32_u16(result),0));
}


void
gf32_vmull_state::mul_region (const gf_32_t * src, gf_32_t * dst,
                     uint64_t bytes, gf_32_t m, bool accum) const
{
  if (m == (gf_32_t)0) {
    if (! accum) {
      memset (dst, 0, bytes);
    }
    return;
  }
  else if (m == (gf_32_t)1) {
    if (! accum){
      memcpy (dst, src, bytes);
    }   
    else {
      uint64_t    chunks = bytes >> 3ULL;
      for (uint64_t i = 0; i < chunks; i+=2) {
        __v64u x = vld1q_u64((uint64_t*)src+i);
        __v64u y = vld1q_u64((uint64_t*)dst+i);
        vst1q_u64((uint64_t*)dst+i, veorq_u64(x,y));
      }
    }
    return;
  }

  uint64_t    i;
  uint64_t    chunks = bytes >> 2ULL;
  if (accum) {
    for (i = 0; i < chunks; i++) {
      dst[i] ^= mul(src[i],m);
    }
  } else {
    for (i = 0; i < chunks; i++){
      dst[i] = mul(src[i],m);
    }
  }
}

void
gf32_vmull_state::mul_region_region (const gf_32_t * src1, 
                 const gf_32_t * src2, gf_32_t * dst,
                 uint64_t bytes, bool accum) const
{

  uint64_t    i;
  uint64_t    chunks = bytes >> 2ULL;
  if (accum) {
    for (i = 0; i < chunks; i++) {
      dst[i] ^= mul(src1[i],src2[i]);
    }
  } else {
    for (i = 0; i < chunks; i++){
      dst[i] = mul(src1[i],src2[i]);
    }
  }
}

gf_32_t
gf32_vmull_state::dotproduct (const gf_32_t * src1, const gf_32_t * src2,
            uint64_t n_elements, uint64_t stride) const
{
    gf_32_t dp = (gf_32_t)0;
    for (uint64_t i = 0; i < n_elements; ++i) {
      dp = add (dp, mul (*src1, *src2));
      src1 += stride;
      src2 += stride;
    }
    return dp;
}
