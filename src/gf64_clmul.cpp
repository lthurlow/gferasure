/*
 * gf64_clmul.cpp
 *
 * Routines for 64-bit Galois fields using SSSE3.
 *
 */

#include "gf64_clmul.h"

#include <string.h>
#include <x86intrin.h>
#ifdef __APPLE__
#include <wmmintrin.h>
#include <smmintrin.h>
#endif

inline
static
__m128i
mul_2(__m128i a, __m128i b, __m128i pp){
  __m128i  res0,res1;
  __m128i  v0, w0, v1, w1;
  __m128i  mask = _mm_set_epi64x(0,0xffffffffffffffff);

  res0 = _mm_clmulepi64_si128(a, b, 0);
  v0 = _mm_insert_epi32(_mm_srli_si128(res0, 8), 0, 0);
  w0 = _mm_clmulepi64_si128(pp, v0, 0);
  res0 = _mm_xor_si128(res0, w0);
  v0 = _mm_insert_epi32(_mm_srli_si128(res0, 8), 0, 1);
  w0 = _mm_clmulepi64_si128(pp, v0, 0);
  res0 = _mm_xor_si128(res0, w0);
  res0 = _mm_and_si128(res0,mask);

  res1 = _mm_clmulepi64_si128(a, b, 0x11);
  v1 = _mm_insert_epi32(_mm_srli_si128(res1, 8), 0, 0);
  w1 = _mm_clmulepi64_si128(pp, v1, 0);
  res1 = _mm_xor_si128(res1, w1);
  v1 = _mm_insert_epi32(_mm_srli_si128(res1, 8), 0, 1);
  w1 = _mm_clmulepi64_si128(pp, v1, 0);
  res1 = _mm_xor_si128(res1, w1);

  return _mm_xor_si128(res0,_mm_slli_si128(res1,8));
}


gf_64_t
gf64_clmul_state::mul(gf_64_t a64, gf_64_t b64) const {
  __m128i  a, b;
  __m128i  result;
  __m128i  pp;
  __m128i  v, w;

  a = _mm_insert_epi64(_mm_setzero_si128(), a64, 0);
  b = _mm_insert_epi64(a, b64, 0);
  pp = _mm_set_epi32(0, 0, 0, (uint32_t)(prim_poly & 0xffffffffULL));
  
  result = _mm_clmulepi64_si128(a, b, 0);
  v = _mm_insert_epi32(_mm_srli_si128(result, 8), 0, 0);
  w = _mm_clmulepi64_si128(pp, v, 0);
  result = _mm_xor_si128(result, w);
  v = _mm_insert_epi32(_mm_srli_si128(result, 8), 0, 1);
  w = _mm_clmulepi64_si128(pp, v, 0);
  result = _mm_xor_si128(result, w);

  return _mm_extract_epi64(result, 0);
}

void
gf64_clmul_state::mul_region (const gf_64_t * src, gf_64_t * dst,
                     uint64_t bytes, gf_64_t m, bool accum) const
{
  if (m == (gf_64_t)0) {
    if (! accum) {
      memset (dst, 0, bytes);
    }
    return;
  }
  else if (m == (gf_64_t)1) {
    if (! accum){
      memcpy (dst, src, bytes);
    }   
    else {
      uint64_t    chunks = bytes >> 4ULL;
      for (uint64_t i = 0; i < chunks; i++) {
        __m128i x = _mm_loadu_si128 (((__m128i *)src) + i);
        __m128i y = _mm_loadu_si128 (((__m128i *)dst) + i);
        _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128(x,y));
      }
    }
    return;
  }

  uint64_t    i;
  uint64_t    chunks = bytes >> 4ULL;
  __m128i     pp = _mm_set_epi32( 0, (uint32_t)(prim_poly & 0xffffffffULL), 
                                  0, (uint32_t)(prim_poly & 0xffffffffULL));
  __m128i     mult = _mm_set1_epi64x(m);

  if (accum) {
    __m128i x,y;
    for (i = 0; i < chunks; i++) {
      x = _mm_loadu_si128 (((__m128i *)src) + i);    
      y = _mm_loadu_si128 (((__m128i *)dst) + i);
      _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128(mul_2(x,mult,pp),y));
    }
  } else {
    __m128i x;
    for (i = 0; i < chunks; i++) {
      x = _mm_loadu_si128 (((__m128i *)src) + i);    
      _mm_storeu_si128 (((__m128i *)dst) + i, mul_2(x,mult,pp));
    }
  }
}

void
gf64_clmul_state::mul_region_region (const gf_64_t * src1, 
                 const gf_64_t * src2, gf_64_t * dst,
                 uint64_t bytes, bool accum) const
{

  uint64_t    i;
  uint64_t    chunks = bytes >> 4ULL;
  __m128i     pp = _mm_set_epi32( 0, (uint32_t)(prim_poly & 0xffffffffULL), 
                                  0, (uint32_t)(prim_poly & 0xffffffffULL));

  if (accum) {
    __m128i x,y,z;
    for (i = 0; i < chunks; i++) {
      x = _mm_loadu_si128 (((__m128i *)src1) + i);
      y = _mm_loadu_si128 (((__m128i *)src2) + i);
      z = _mm_loadu_si128 (((__m128i *)dst) + i);
      _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128(mul_2(x,y,pp),z));
    }
  } else {
    __m128i x,y;
    for (i = 0; i < chunks; i++) {
      x = _mm_loadu_si128 (((__m128i *)src1) + i);
      y = _mm_loadu_si128 (((__m128i *)src2) + i);
      _mm_storeu_si128 (((__m128i *)dst) + i, mul_2(x,y,pp));
    }
  }
}

gf_64_t
gf64_clmul_state::dotproduct (const gf_64_t * src1, const gf_64_t * src2,
            uint64_t n_elements, uint64_t stride) const
{

  /*
  if (stride == 1 && n_elements > 1 && !(n_elements&1)) {
    __m64 dp = _m_from_int64(0);
    __m128i x,y;
    __m128i pp = _mm_set_epi32( 0, (uint32_t)(prim_poly & 0xffffffffULL), 
                                0, (uint32_t)(prim_poly & 0xffffffffULL));

    for (uint64_t i = 0; i < n_elements; i+=2) {
      x = _mm_loadu_si128 (((__m128i *)src1) + i);
      y = _mm_loadu_si128 (((__m128i *)src2) + i);
      x = mul_2(x,y,pp);
      y = _mm_srli_si128(x,8);
      x = _mm_xor_si128(x,y);
      dp = _mm_xor_si64(dp,_m_from_int64(_mm_extract_epi64(x,0)));
    }
    return _m_to_int64(dp);
  } else {
  }
  */
    gf_64_t dp = (gf_64_t)0;
    for (uint64_t i = 0; i < n_elements; ++i) {
      dp = add (dp, mul (*src1, *src2));
      src1 += stride;
      src2 += stride;
    }
    return dp;

}
