//
// gf16_vmull.h
//
// 16-bit Galois field using SSSE3.
//

#pragma     once

#include "gf_types.h"
#include "gfw_log.h"

#include "gf_random.h"

struct
alignas(16)
gf16_vmull_state {

    gf16_vmull_state (gf_16_t prim_poly_ = gf16_default_primpoly, gf_16_t alpha_ = 1)
        : prim_poly(prim_poly_)
        , log_state(prim_poly_, alpha_)
    {
    }


    gf_16_t  add (gf_16_t a, gf_16_t b)    const {return ((gf_16_t)(a ^ b));}
    gf_16_t  sub (gf_16_t a, gf_16_t b)    const {return ((gf_16_t)(a ^ b));}
    gf_16_t  inv (gf_16_t a)               const {return (log_state.inv (a));}
    gf_16_t  pow (gf_16_t a, gf_16_t b)    const {return (log_state.pow (a, b));}
    gf_16_t  mul (gf_16_t a, gf_16_t b)    const;
    gf_16_t  div (gf_16_t a, gf_16_t b)    const {return (mul (a,inv(b)));}

    void        mul_region (const gf_16_t * src, gf_16_t * dst,
                            uint64_t bytes, gf_16_t m, bool accum = false) const;

    void        mul_region_region (const gf_16_t * src1, const gf_16_t * src2, gf_16_t * dst,
                                 uint64_t bytes, bool accum = false) const;

    gf_16_t      dotproduct (const gf_16_t * src1, const gf_16_t * src2,
                            uint64_t n_elements, uint64_t stride = 1) const;

    uint64_t    field_size () const     {return GF16_FIELD_SIZE;}
    uint64_t    field_max () const      {return (field_size() - 1);}

 private:
    gf_16_t             prim_poly;
    gf16_log_state log_state;
};
