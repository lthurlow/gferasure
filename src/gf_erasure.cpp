//
// gf_erasure.cpp
//
// Routines for doing erasure coding.

#include <stdio.h>
#include <string.h>
#include "gf_erasure.h"
#include "gf_matrix.h"

template <class gf_w, class gf_state>
void
gf_erasure<gf_w, gf_state>::setup ()
{
    gf_w        share_ids[n_data + n_ecc];
    //otherwise warning of unused variable
    memset(share_ids, 0, (n_data+n_ecc)*sizeof(gf_w));
    switch (ecc_type) {
    case ECC_CAUCHY_SYSTEMATIC:
        generator.set_cauchy (true);
        break;
    case ECC_CAUCHY:
        generator.set_cauchy (false);
        break;
    case ECC_VANDERMONDE:
        for (unsigned i = 0; i < n_data + n_ecc; ++i){
            share_ids[i] = (gf_w)i;
        }
        generator.set_vandermonde ();
        break;
    case ECC_PQ:
        break;
    }
#ifdef  DEBUG
    generator.print ();
#endif
}

//
// Generate coded result by applying the generator matrix to the input data.
//
template <class gf_w, class gf_state>
void
gf_erasure<gf_w, gf_state>::generate (const gf_w * data[], gf_w * result[], uint64_t n_bytes) const
{
    generator.apply (data, result, n_bytes);
}

//
// Generate a rebuild matrix by copying relevant rows from the generator into the partial,
// and then inverting it into the recovery matrix.
//
template <class gf_w, class gf_state>
bool
gf_erasure<gf_w, gf_state>::config_rebuild (const gf_w in_ids[],
                                            gf_matrix<gf_w, gf_state> * recover)
{
    int         res;

    if (! recover) {
        recover = &internal_recover;
    }
    for (unsigned r = 0; r < n_data; ++r) {
        partial.copy_row (r, generator, in_ids[r]);
    }
    res = partial.invert (recover);
    return (res == 0);
}

template <class gf_w, class gf_state>
bool
gf_erasure<gf_w, gf_state>::rebuild (const gf_w * in[], gf_w * out[], uint64_t n_bytes,
                                     const gf_matrix<gf_w, gf_state> * recover) const
{
    if (! recover) {
        recover = &internal_recover;
    }
    recover->apply (in, out, n_bytes);
    return true;
}

#include "gf_erasure_fields.h"
