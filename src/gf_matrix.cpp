//
// gf_matrix.cpp
//
// Routines for doing matrix operations.

#include <string.h>
#include <stdio.h>
#include <algorithm>
#include "gf_matrix.h"

template <class gf_w, class gf_state>
gf_matrix<gf_w, gf_state>::gf_matrix( unsigned rows_, unsigned columns_,
                                      const gf_w_state<gf_w, gf_state> & field_)

    : n_rows(rows_)
    , n_cols(columns_)
    , field (field_)
{
    values = new gf_w[n_rows * n_cols];
    set_zero ();
}

template <class gf_w, class gf_state>
gf_matrix<gf_w, gf_state>::gf_matrix (const gf_matrix & m)

    : field (m.field)
{
    n_rows = m.n_rows;
    n_cols = m.n_cols;
    values = new gf_w[n_rows * n_cols];
    memcpy (values, m.values, n_rows * n_cols * sizeof (gf_w));
}

template <class gf_w, class gf_state> gf_matrix<gf_w, gf_state>::~gf_matrix(){
    delete values;
}

template <class gf_w, class gf_state> 
void 
gf_matrix<gf_w, gf_state>::copy_column(unsigned dst_col, 
                                       const gf_matrix<gf_w, gf_state> & src_matrix, unsigned src_col){

    if (src_matrix.n_rows != n_rows) {
        return;
    }
    for (unsigned i = 0; i < n_rows; ++i) {
        e (i, dst_col) = src_matrix.e (i, src_col);
    }
}

template <class gf_w, class gf_state>
void
gf_matrix<gf_w, gf_state>::copy_row( unsigned dst_row, 
                                     const gf_matrix<gf_w, gf_state> & src_matrix, unsigned src_row){

    if (src_matrix.n_cols != n_cols) {
        return;
    }
    for (unsigned i = 0; i < n_cols; ++i) {
        e (dst_row, i) = src_matrix.e (src_row, i);
    }
}

template<class gf_w, class gf_state>
void
gf_matrix<gf_w, gf_state>::exchange_rows (unsigned a, unsigned b)
{
    gf_w        tmp;

    for (unsigned c = 0; c < n_cols; ++c) {
        tmp = e(a,c);
        e(a,c) = e(b,c);
        e(b,c) = tmp;
    }
}

template <class gf_w, class gf_state> int gf_matrix<gf_w, gf_state>::invert(gf_matrix<gf_w, gf_state> * result) {

    if (n_rows != n_cols || result->n_rows != result->n_cols || n_rows != result->n_rows ||
        n_rows > 64) {
        return -1;
    }

    // Save the original matrix and initialize the dual to identity
    gf_matrix<gf_w, gf_state>     tm = *this;
    gf_w                factor;
    result->set_identity ();

#ifdef  DEBUG
    tm.print ("Invert: input");
#endif

#if 0
    uint64_t            non_zeros[n_rows];
    unsigned            identity_rows[n_rows];
    const unsigned      unassigned = 0xffffffU;
    // Count zeros in each row.  NOTE: only works if 64 or fewer elements, since we're using
    // a uint64_t to hold the bits.
    for (unsigned r = 0; r < n_rows; ++r) {
        identity_rows[n_rows] = unassigned;
    }
    for (unsigned r = 0; r < n_rows; ++r) {
        for (unsigned c = 0; c < n_cols; ++c) {
            non_zeros[r] |= (tm(r,c) != (gf_w)0) ? (1ULL << (uint64_t)c) : 0;
        }
        unsigned nz = __builtin_popcountll (non_zeros[r]);
        if (nz == 0) {
        } else if (nz == 1) {
            identity_rows[__builtin_ctzll (non_zeros[r])] = r;
        }
    }
#endif

    // Convert primary to upper-triangular with 1s on the diagonal
    for (unsigned r = 0; r < n_rows - 1; ++r) {
        if (tm(r,r) == (gf_w)0) {
            unsigned r_a;
            for (r_a = r+1; r_a < n_rows; ++r_a) {
                if (tm(r_a,r) != (gf_w)0) {
                    break;
                }
            }
#ifdef  DEBUG
            printf ("Exchanging rows %d and %d\n", r, r_a);
#endif
            tm.exchange_rows (r, r_a);
            result->exchange_rows (r, r_a);
#ifdef  DEBUG
            tm.print ("Original");
            result->print ("Result");
#endif
        }
        for (unsigned r_a = r+1; r_a < n_rows; ++r_a) {
            // Element is already zero, so no need to fix it
            if (tm(r_a, r) == (gf_w)0) {
                continue;
            }
            factor = field.div (tm(r_a, r), tm(r, r));
            // Update the original matrix
            tm(r_a, r) = (gf_w)0;
            for (unsigned c = r+1; c < n_cols; c++) {
                tm(r_a, c) = field.add (tm(r_a, c), field.mul (tm(r, c), factor));
            }
            // Update the dual
            for (unsigned c = 0; c < n_cols; c++) {
                result->e(r_a, c) = field.add (result->e(r_a, c), field.mul (factor,
                                                                             result->e(r, c)));
            }

#ifdef  DEBUG
            tm.print ("Original");
            result->print ("Result");
#endif

        }
    }
#ifdef  DEBUG
    printf ("++++ Invert: upper triangular reached\n");
    tm.print ("Original Original Original Original");
    result->print ("Result");
    printf ("++++^^^^\n");
#endif
    // Back-substitute.  However, we only need to do operations on the dual, since the operations
    // on the original matrix only impact the current column (the one being zeroed), and result
    // in the column containing a single 1 on the diagonal.
    // We normalize the row first, and then add it into the other rows because this is a bit
    // quicker for the dual: no need to calculate inverses for each row, only for the bottom
    // column.
    for (int r = n_rows - 1; r >= 0; --r) {
        factor = field.inv (tm((unsigned)r, (unsigned)r));

        for (unsigned c = 0; c < n_cols; ++c) {
            result->e((unsigned)r, c) = field.mul (result->e((unsigned)r, c), factor);
        }
        for (unsigned r_a = 0; r_a < (unsigned)r; ++r_a) {
            factor = tm(r_a, (unsigned)r);
            if (factor == (gf_w)0) {
                continue;
            }

#ifdef  DEBUG
            printf ("Adding to row %d, factor %x\n", r_a, factor);
#endif

            for (unsigned c = 0; c < n_cols; ++c) {
                result->e(r_a, c) = field.add (result->e(r_a, c), field.mul (result->e(r, c),
                                                                             factor));
            }
#ifdef  DEBUG
            result->print ("Result");
#endif
        }
    }
#ifdef  DEBUG
    result->print ("Final result");
    printf ("+++++++++++++++++++++++++++ Invert: end\n");
#endif
    return 0;
}

template <class gf_w, class gf_state>
void
gf_matrix<gf_w, gf_state>::set_zero () {
    memset (values, 0, n_rows * n_cols * sizeof (gf_w));
}

template <class gf_w, class gf_state>
void
gf_matrix<gf_w, gf_state>::set_identity (){

    if (n_rows != n_cols) {
        return;
    }
    set_zero ();
    for (unsigned i = 0; i < n_rows; i++) {
        e(i, i) = (gf_w)1;
    }
}

//
// This routine sets up a Vandermonde matrix.  If a row basis vector is passed, the
// values in the vector are used as the first entry for each row.  Otherwise, the
// numbers 1..r are used for an r-row matrix.
//
template <class gf_w, class gf_state>
bool
gf_matrix<gf_w, gf_state>::set_vandermonde (const gf_w * row_basis)
{
    for (unsigned r = 0; r < n_rows; ++r) {
        e(r, 0) = (gf_w)1;
        gf_w    v = (gf_w)1;
        gf_w    m = (row_basis == (gf_w *)0) ? (gf_w)(r + 1) : row_basis[r];
        for (unsigned c = 1; c < n_cols; ++c) {
            v = field.mul (v, m);
            e(r, c) = v;
        }
    }
    return true;
}

template <class gf_w, class gf_state>
bool
gf_matrix<gf_w, gf_state>::set_cauchy (bool systematic, const gf_w * row_ids, const gf_w * col_ids)
{
    gf_w                r_space[n_rows], c_space[n_cols];
    const gf_w *        r_id;
    const gf_w *        c_id;
    unsigned            start_row = systematic ? n_cols : 0;

    if (n_rows < n_cols) {
        return false;
    }

    if (row_ids == (gf_w *)0 && col_ids == (gf_w *)0) {
        // We use odd values for rows and even for columns.  That way, we know we
        // don't have conflicting values.  It also means that many entries are going to
        // be 1, since they'll be 2*r+2 ^ 2*c+3, where r == c
        for (unsigned r = start_row; r < n_rows; ++r) {
            r_space[r] = (gf_w)(2 * r + 2);
        }
        for (unsigned c = 0; c < n_cols; ++c) {
            c_space[c] = (gf_w)(2 * c + 3);
        }
        r_id = r_space;
        c_id = c_space;
    } else if (row_ids != (gf_w *)0 && col_ids != (gf_w *)0) {
        r_id = row_ids;
        c_id = col_ids;
    } else {
        // Must specify both row and column IDs or neither of them.
        return false;
    }

    for (unsigned r = 0; r < start_row; ++r) {
        for (unsigned c = 0; c < n_cols; ++c) {
            e(r, c) = 0;
        }
        e(r, r) = 1;
    }
    for (unsigned r = start_row; r < n_rows; ++r) {
        for (unsigned c = 0; c < n_cols; ++c) {
            if (r_id[r] == c_id[c]) {
                return false;
            }
            e(r, c) = field.inv (field.add (r_id[r], c_id[c]));
        }
    }
    return true;
}

template <class gf_w, class gf_state>
void
gf_matrix<gf_w, gf_state>::set_cauchy(){

    for (unsigned r = 0; r < n_rows; ++r) {
        for (unsigned c = 0; c < n_cols; ++c) {
            e(r, c) = field.inv (field.add(r, 1) + field.add(c, n_rows));
        }
    }
}


template <class gf_w, class gf_state>
void
gf_matrix<gf_w, gf_state>::apply (const gf_w * in, gf_w * out) const {

    for (unsigned r = 0; r < n_rows; ++r) {
        gf_w    accum = (gf_w)0;

        for (unsigned c = 0; c < n_cols; ++c) {
            accum ^= field.mul (in[c], e(r, c));
        }
        out[r] = accum;
    }
}

template <class gf_w, class gf_state>
void
gf_matrix<gf_w, gf_state>::apply (const gf_w * const * in, gf_w * out[], uint64_t sz,
                                  uint64_t stripe_size) const {

    for (uint64_t off = 0; off < (uint64_t)sz; off += stripe_size) {
        uint64_t        tsz = std::min ((uint64_t)sz - off, stripe_size);
        for (unsigned r =
                 0; r < n_rows; ++r) {
            if (! out[r]) {
                continue;
            }
            field.mul_region (in[0] + off / sizeof (gf_w), out[r] + off / sizeof (gf_w),
                              tsz, e(r, 0));
            for (unsigned c = 1; c < n_cols; ++c) {
                field.mul_region (in[c] + off / sizeof (gf_w), out[r] + off / sizeof (gf_w),
                                  tsz, e(r, c), true);
            }
        }
    }
}


template <class gf_w, class gf_state>
void
gf_matrix<gf_w, gf_state>::print (const char * name) const
{
    if (name != NULL) {
        printf ("%s\n", name);
    }
    for (unsigned r = 0; r < n_rows; ++r) {
        for (unsigned c = 0; c < n_cols; ++c) {
            printf ("%04x ", (unsigned)e(r, c));
        }
        printf ("\n");
    }
}

void
print_array (const gf_16_t * a, unsigned n, const char * nm = NULL){

    if (nm != NULL) {
        printf ("%s: ", nm);
    }

    for (unsigned i = 0; i < n; ++i) {
        printf ("%04x ", a[i]);
    }
    putchar ('\n');
}

#include "gf_matrix_fields.h"
