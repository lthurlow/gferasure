//
// gf32_vmull.h
//
// 32-bit Galois field using SSSE3.
//

#pragma     once

#include "gf_types.h"
#include "gfw_shiftadd.h"

#include "gf_random.h"

struct
alignas(16)
gf32_vmull_state {

    gf32_vmull_state (gf_32_t prim_poly_ = gf32_default_primpoly, gf_32_t alpha_ = 1)
        : prim_poly(prim_poly_)
        , shiftadd_state(prim_poly_, alpha_)
    {
    }


    gf_32_t  add (gf_32_t a, gf_32_t b)    const {return ((gf_32_t)(a ^ b));}
    gf_32_t  sub (gf_32_t a, gf_32_t b)    const {return ((gf_32_t)(a ^ b));}
    gf_32_t  inv (gf_32_t a)               const {return (shiftadd_state.inv (a));}
    gf_32_t  pow (gf_32_t a, gf_32_t b)    const {return (shiftadd_state.pow (a, b));}
    gf_32_t  mul (gf_32_t a, gf_32_t b)    const;
    gf_32_t  div (gf_32_t a, gf_32_t b)    const {return (mul (a,inv(b)));}

    void        mul_region (const gf_32_t * src, gf_32_t * dst,
                            uint64_t bytes, gf_32_t m, bool accum = false) const;

    void        mul_region_region (const gf_32_t * src1, const gf_32_t * src2, gf_32_t * dst,
                                 uint64_t bytes, bool accum = false) const;

    gf_32_t      dotproduct (const gf_32_t * src1, const gf_32_t * src2,
                            uint64_t n_elements, uint64_t stride = 1) const;

    uint64_t    field_size () const     {return GF32_FIELD_SIZE;}
    uint64_t    field_max () const      {return (field_size() - 1);}

 private:
    gf_32_t             prim_poly;
    gf32_shiftadd_state shiftadd_state;
};
