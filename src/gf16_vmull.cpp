/*
 * gf16_vmull.cpp
 *
 * Routines for 64-bit Galois fields using NEON.
 *
 */

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <arm_neon.h>

#include "gf16_vmull.h"

typedef poly16x8_t __v16p;
typedef poly8x8_t  __v8x8p;
typedef uint8x8_t  __v8x8u;
typedef uint8x16_t __v8u;
typedef uint16x8_t __v16u;
typedef uint64x2_t __v64u;


#ifdef GF_NEON32
static
inline
uint16x8_t
mul_4(uint64x1_t a16, uint64x1_t b16){
  uint16x4_t a = vreinterpret_u16_u64(a16);
  uint16x4_t b = vreinterpret_u16_u64(b16);
  poly16x8_t res0, res1;
  res0 = vmull_p8(vreinterpret_p8_u16(a),vreinterpret_p8_u16(b));
  a = veor_u16(vshr_n_u16(a,8),vshl_n_u16(a,8));
  res1 = vmull_p8(vreinterpret_p8_u16(a),vreinterpret_p8_u16(b));
  b = veor_u16(vmovn_u32(vreinterpretq_u32_p16(res1)),vmovn_u32(vshrq_n_u32(vreinterpretq_u32_p16(res1),16)));
  res1 = vreinterpretq_p16_u32(vshlq_n_u32(vmovl_u16(b),8));
  res0 = vreinterpretq_p16_u16(veorq_u16(vreinterpretq_u16_p16(res0),vreinterpretq_u16_p16(res1)));
  return vreinterpretq_u16_p16(res0);
}

#elif GF_NEON64
static
inline
uint16x8_t
mul_4(uint64x1_t a16, uint64x1_t b16){
  return vmull.p16(a,b);
}
#endif

static
inline
uint64_t
mul16x4(uint64x1_t a, uint64x1_t b, uint16_t ip){
  uint64x1_t pp = vreinterpret_u64_u16(vdup_n_u16(ip&0xff));
  uint16x8_t mk = vdupq_n_u16(0xff00);
  uint16x8_t v,w,result;

  result = mul_4(a, b);
  v = vreinterpretq_u16_u32(vshrq_n_u32(vreinterpretq_u32_u16(result), 16));
  v = vandq_u16(v,mk);
  w = mul_4(pp,vreinterpret_u64_u16(vmovn_u32(vreinterpretq_u32_u16(v))));

  result = veorq_u16(result, w);
  v = vreinterpretq_u16_u32(vshrq_n_u32(vreinterpretq_u32_u16(result), 16));
  v = vandq_u16(v,vshrq_n_u16(mk,8));
  w = mul_4(pp,vreinterpret_u64_u16(vmovn_u32(vreinterpretq_u32_u16(v))));

  result = veorq_u16(result, w);

  return (uint64_t)vmovn_u32(vreinterpretq_u32_u16(result));
}

gf_16_t
gf16_vmull_state::mul(gf_16_t a16, gf_16_t b16) const {
  uint64x1_t a = vcreate_u64(a16);
  uint64x1_t b = vcreate_u64(b16);
  uint64x1_t pp = vcreate_u64(prim_poly&0xff);
  uint16x8_t mk = vdupq_n_u16(0xff00);
  uint16x8_t v,w,result;

  result = mul_4(a, b);
  v = vreinterpretq_u16_u32(vshrq_n_u32(vreinterpretq_u32_u16(result), 16));
  v = vandq_u16(v,mk);
  w = mul_4(pp, vcreate_u64(vgetq_lane_u16(v,0)));
  result = veorq_u16(result, w);
  v = vreinterpretq_u16_u32(vshrq_n_u32(vreinterpretq_u32_u16(result), 16));
  v = vandq_u16(v,vshrq_n_u16(mk,8));
  w = mul_4(pp, vcreate_u64(vgetq_lane_u16(v,0)));
  result = veorq_u16(result, w);

  return vgetq_lane_u16(result,0);
}


void
gf16_vmull_state::mul_region (const gf_16_t * src, gf_16_t * dst,
                     uint64_t bytes, gf_16_t m, bool accum) const
{
  if (m == (gf_16_t)0) {
    if (! accum) {
      memset (dst, 0, bytes);
    }
    return;
  }
  else if (m == (gf_16_t)1) {
    if (! accum){
      memcpy (dst, src, bytes);
    }   
    else {
      uint64_t    chunks = bytes >> 3ULL;
      for (uint64_t i = 0; i < chunks; i+=2) {
        __v64u x = vld1q_u64((uint64_t*)src+i);
        __v64u y = vld1q_u64((uint64_t*)dst+i);
        vst1q_u64((uint64_t*)dst+i, veorq_u64(x,y));
      }
    }
    return;
  }

  uint64_t    i;
  uint64_t    chunks = bytes >> 3ULL;
  uint64x1_t  mult = vreinterpret_u64_u16(vdup_n_u16(m));

  if (accum) {
    for (i = 0; i < chunks; i++) {
      uint64x1_t x = vld1_u64((uint64_t*)src+i);
      uint64x1_t y = vld1_u64((uint64_t*)dst+i);
      vst1_u64((uint64_t*)dst+i,
          veor_u64(y,vcreate_u64(mul16x4(x,mult,prim_poly))));
    }
  } else {
    for (i = 0; i < chunks; i++){
      uint64x1_t x = vld1_u64((uint64_t*)src+i);
      vst1_u64((uint64_t*)dst+i,mul16x4(x,mult,prim_poly));
    }
  }
}

void
gf16_vmull_state::mul_region_region (const gf_16_t * src1, 
                 const gf_16_t * src2, gf_16_t * dst,
                 uint64_t bytes, bool accum) const
{

  uint64_t    i;
  uint64_t    chunks = bytes >> 1ULL;
  if (accum) {
    for (i = 0; i < chunks; i++) {
      dst[i] ^= mul(src1[i],src2[i]);
    }
  } else {
    for (i = 0; i < chunks; i++){
      dst[i] = mul(src1[i],src2[i]);
    }
  }
}

gf_16_t
gf16_vmull_state::dotproduct (const gf_16_t * src1, const gf_16_t * src2,
            uint64_t n_elements, uint64_t stride) const
{
    gf_16_t dp = (gf_16_t)0;
    for (uint64_t i = 0; i < n_elements; ++i) {
      dp = add (dp, mul (*src1, *src2));
      src1 += stride;
      src2 += stride;
    }
    return dp;
}
