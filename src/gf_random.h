//compile file only once, equivalent to using include guards
#pragma once

#include "gf_types.h"

// gf_random_u64 is going to generate a 64 bit rand
extern uint64_t gf_random_u64(void);
extern void     gf_random_fill (void * buf, uint64_t len);

template <typename gf_w> gf_w gf_random_val();
