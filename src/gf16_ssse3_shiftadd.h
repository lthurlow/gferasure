//
// gf8_log.h
//
// 8-bit Galois field using log tables.
//

#pragma     once

#include "gf_types.h"
#include "gf_random.h"

struct
alignas(16)
gf16_ssse3_shiftadd_state {

  gf16_ssse3_shiftadd_state (gf_16_t prim_poly = gf16_default_primpoly, gf_16_t alpha = 1);
  gf_16_t    add (gf_16_t a, gf_16_t b) const {return ((gf_16_t)(a ^ b));}
  gf_16_t    sub (gf_16_t a, gf_16_t b) const {return ((gf_16_t)(a ^ b));}
  gf_16_t    log (gf_16_t a) const {return (this->log_tbl[a]);}
  gf_16_t    antilog (gf_16_t a) const {return (this->antilog_tbl[a]);}
  gf_16_t    mul (gf_16_t a, gf_16_t b) const {
    return ((a == 0 || b == 0) ? 0 :
      antilog_tbl[(uint32_t)log (a) + (uint32_t)log(b)]);
  }

  gf_16_t    div (gf_16_t a, gf_16_t b) const {
    return ((a == 0 || b == 0) ? 0 :
      antilog ((gf_16_t)(
        ((GF8_FIELD_SIZE-1)+(int)log(a)-(int)log(b))
        %(GF8_FIELD_SIZE-1))));
  }

  gf_16_t    inv (gf_16_t a) const;
  gf_16_t    pow (gf_16_t a, uint32_t b) const;
  void      mul_region (gf_16_t * const src, gf_16_t * dst,
                        uint64_t bytes, gf_16_t m, bool accum = false) const;
  void      mul_region_region (gf_16_t * const src1, gf_16_t * const src2, gf_16_t * dst,
                               uint64_t bytes, bool accum = false) const;
  uint64_t field_size () const {return GF16_FIELD_SIZE;}
  int      sanity_check (int max_stage = 45) const;

private:
  gf_16_t   log_tbl[GF16_FIELD_SIZE];
  gf_16_t   antilog_tbl[GF16_FIELD_SIZE * 2];
  gf_16_t   alpha;
  uint32_t prim_poly;

};
