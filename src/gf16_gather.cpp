/*
 * gf16_log.cpp
 *
 * Routines for 16-bit Galois fields using gathered log tables.
 *
 */

#include "gf16_gather.h"
#include <stdio.h>
#include <string.h>
#include <immintrin.h>
#include <nmmintrin.h> //for _mm_blendv_epi8


/*
 * Basic Galois field routines
 */

gf_16_t 
gf16_gather_state::pow (gf_16_t a, uint32_t b) const {

    if (a == 0) {
        return (a);
    } else if (b == 0) {
        return (gf_16_t)1;
    } else {
        return (antilog ((gf_16_t)(((uint64_t)log(a) * ((uint64_t)b & 0xfffffULL))
                                  % (uint64_t)(GF16_FIELD_SIZE-1))));
    }
}

gf_16_t
gf16_gather_state::inv (gf_16_t a) const {

    if (a == 0) {
        return (0);
    } else if (a == 1) {
        return (1);
    } else {
        return (antilog_tbl[GF16_FIELD_SIZE - 1 - log_tbl[a]]);
    }
}

void
gf16_gather_state::mul_region (const gf_16_t * src, gf_16_t * dst, uint64_t bytes,
                           gf_16_t multiplier, bool accum) const {

    if (multiplier == (gf_16_t)0) {
        if (! accum) {
            memset (dst, 0, bytes);
        }
        return;
    }

    __m256i m1,m2,m3;
    __m256i zero = _mm256_setzero_si256();
    //256 bit vector with constant value seperated every 32 bits
    __m256i log_m = _mm256_set1_epi32(log(multiplier));

    //mask for only the low 16 bits of each 32 bits
    __m256i gmask = _mm256_set_epi32(0x0000ffff,0x0000ffff,0x0000ffff,0x0000ffff,0x0000ffff,0x0000ffff,0x0000ffff,0x0000ffff);

    uint64_t    i;
    // we will load/operate over 256 bits at a time
    uint64_t    chunks = bytes >> 5ULL;


    if (accum) {
      for (i = 0; i < chunks; i++) {
        //load values from region into vector (8 values into 128 bit vector)
        m3 = _mm256_loadu_si256 (((__m256i *)src) + i); // load 128 bits into a vector
        //convert / expand from 16 bits to 32 bits
        m1 = _mm256_unpacklo_epi16(m3, zero); //+1 lat
        //gather // lookup into log table
        m1 = _mm256_i32gather_epi32((int32_t*)log_tbl,m1,4);
        m1 = _mm256_and_si256(gmask,m1);
        //add our multiplier to our looked up value
        m1 = _mm256_add_epi32(log_m,m1);
        //gather // lookup into alog table 
        m1 = _mm256_i32gather_epi32((int32_t*)antilog_tbl,m1,4);
        m1 = _mm256_and_si256(gmask,m1);
        //convert back
        m1 = _mm256_packus_epi32(m1,zero);

        m2 = _mm256_unpackhi_epi16(m3, zero); //+1 lat
        m2 = _mm256_i32gather_epi32((int32_t*)log_tbl,m2,4);
        m2 = _mm256_and_si256(gmask,m2);
        m2 = _mm256_add_epi32(log_m,m2);
        m2 = _mm256_i32gather_epi32((int32_t*)antilog_tbl,m2,4);
        m2 = _mm256_and_si256(gmask,m2);
        m2 = _mm256_slli_si256(_mm256_packus_epi32(m2,zero),8);

        m2 = _mm256_xor_si256(m1,m2);

        //check and replace original 0 values in array
        m3 = _mm256_cmpeq_epi16(m3,zero);
        m2 = _mm256_andnot_si256(m3,m2);

        //store value back out
        _mm256_storeu_si256 (((__m256i *)dst) + i,
              _mm256_xor_si256(m2,
              _mm256_loadu_si256 (((__m256i *)dst) + i)));
      }
    } else {
      for (i = 0; i < chunks; i++) {

        //load values from region into vector (8 values into 128 bit vector)
        m3 = _mm256_loadu_si256 (((__m256i *)src) + i); // load 128 bits into a vector
        //convert / expand from 16 bits to 32 bits
        m1 = _mm256_unpacklo_epi16(m3, zero); //lat = 1, thp  = 1

        //gather // lookup into log table
        m1 = _mm256_i32gather_epi32((int32_t*)log_tbl,m1,4); //lat = 6, thp  = -
        m1 = _mm256_and_si256(gmask,m1);
        //add our multiplier to our looked up value
        m1 = _mm256_add_epi32(log_m,m1); //lat = 1, thp  = .5
        //gather // lookup into alog table 
        m1 = _mm256_i32gather_epi32((int32_t*)antilog_tbl,m1,4); //lat = 6, thp  = -
        m1 = _mm256_and_si256(gmask,m1);
        //convert back
        m1 = _mm256_packus_epi32(m1,zero); //lat = 1, thp  = 1

        m2 = _mm256_unpackhi_epi16(m3, zero); //    = 1,      = 1
        m2 = _mm256_i32gather_epi32((int32_t*)log_tbl,m2,4); //    = 6,      = -
        m2 = _mm256_and_si256(gmask,m2);
        m2 = _mm256_add_epi32(log_m,m2); //    = 1,      = .5
        m2 = _mm256_i32gather_epi32((int32_t*)antilog_tbl,m2,4); //    = 6,      = -
        m2 = _mm256_and_si256(gmask,m2);
        m2 = _mm256_slli_si256(_mm256_packus_epi32(m2,zero),8);  //lat = 2, thp  = 1
        m2 = _mm256_xor_si256(m1,m2); //lat = 1, thp  = -

        //check and replace original 0 values in array
        m3 = _mm256_cmpeq_epi16(m3,zero); //lat = 1, thp  = .5
        m2 = _mm256_andnot_si256(m3,m2); //lat = 1, thp  = .5


        //store value back out
        _mm256_storeu_si256 (((__m256i *)dst) + i, m2);
      }
    }
    for (i = chunks << 5ULL; i < bytes; ++i) {
      dst[i] = mul (src[i], multiplier) ^ (accum ? dst[i] : (gf_16_t)0);
    }
}


void
gf16_gather_state::mul_region_region (const gf_16_t * src1, const gf_16_t * src2,
                                  gf_16_t * dst, uint64_t bytes, bool accum) const
{

    if (accum) {
        for (unsigned i = 0; i < bytes / sizeof (gf_16_t); ++i) {
            dst[i] ^= mul (src1[i], src2[i]);
        }
    } else {
        for (unsigned i = 0; i < bytes / sizeof (gf_16_t); ++i) {
            dst[i] = mul (src1[i], src2[i]);
        }
    }
}

gf_16_t
gf16_gather_state::dotproduct (const gf_16_t * src1, const gf_16_t * src2,
                           uint64_t n_elements, uint64_t stride) const
{
    gf_16_t             dp = (gf_16_t)0;
    gf_16_t const *     s1 = src1;
    gf_16_t const *     s2 = src2;

    for (uint64_t i = 0; i < n_elements; ++i) {
        dp = add (dp, mul (*s1, *s2));
        s1 += stride;
        s2 += stride;
    }
    return dp;
}

//
// Since we're not using this for much, we want to make it more memory-efficient rather than
// speed-efficient.  We don't precompute the inverse table; we can just look up inverses
// using the log / antilog tables instead.
//
gf16_gather_state::gf16_gather_state (gf_16_t prim_poly, gf_16_t alpha){

    uint32_t    b, i;

    this->alpha = alpha;
    this->prim_poly = 0x10000 | (uint32_t)prim_poly;

    for (b = this->alpha, i = 0; i < GF16_FIELD_SIZE - 1; i++) {
        log_tbl[b] = (gf_32_t)i;
        antilog_tbl[i] = (gf_32_t)b;
        antilog_tbl[i + GF16_FIELD_SIZE - 1] = (gf_32_t)b;
        b <<= 1;
        if (b & GF16_FIELD_SIZE) {
            b ^= this->prim_poly;
        }
    }
}
