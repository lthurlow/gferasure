#include "gf_random.h"

#ifdef ARCH_RDRAND_X86
#else
#include <random>
#endif

uint64_t
gf_random_u64(void)
{

    uint64_t    v;

#ifdef  ARCH_RDRAND_X86
      //int _rdrand_u64_step(unsigned __int64 *);
      //Bull Mountain, intrinsic to generate rand store in v
      __asm__ volatile ("rdrand %0" : "=r" (v));
#else
      //For devices without hardware support for rand gen.
      std::mt19937 rng;
      rng.seed(std::random_device()());
      std::uniform_int_distribution<uint64_t> uint_dist;
      v = uint_dist(rng);
#endif
    return (v);
}

// returns random value based on passed in GF's field size [0, 2^n-1]
//returns a random unsigned interger from 0-255
template<>
gf_8_t
gf_random_val<gf_8_t>(){
    return (gf_8_t)(gf_random_u64() % GF8_FIELD_SIZE);
}

//returns a random unsigned integer from 0-65535
template<>
gf_16_t
gf_random_val<gf_16_t>()
{
    return (gf_16_t)(gf_random_u64() % GF16_FIELD_SIZE);
}

template<>
gf_32_t
gf_random_val<gf_32_t>(){
    return (gf_32_t)(gf_random_u64() % GF32_FIELD_SIZE);
}

//returns a random unsigned integer from 0-65535
template<>
gf_64_t
gf_random_val<gf_64_t>(){
    return (gf_64_t)(gf_random_u64());
}

void
gf_random_fill (void * buf, uint64_t len)
{
    uint64_t *  b = (uint64_t *)buf;
    uint8_t *   b8;

    for (uint64_t i = 0; i < len; i += sizeof (uint64_t), b++) {
        *b = gf_random_u64();
    }
    for (b8 = (uint8_t *)b; b8 < (uint8_t *)buf + len; b++) {
        *b8 = gf_random_val<gf_8_t> ();
    }
}

