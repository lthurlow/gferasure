Building Gferasure  
===================

Prequisites
-------------

Before building Gferasure we will need to check what CPU instructions are currently supported by your machine.  To check what capabilities your machine has we will need to check the CPU Flags.  
On Linux run **cat /proc/cpuinfo** if there is output, your machine is capable of running avx2 instructions.  
On FreeBSD or Mac OSX **sysctl machdep.cpu.leaf7_features** for avx2 and  **sysctl machdep.cpu.features** for other flags (ssse3, sse4, rdrand, clmul). 
On arm devices confirm wether neon exists for 32 bit ARM or for 64 bit arm that asimd and pmull instructions exist.


Modifying the Make file
-------------

 13 SUPPORT = avx2 ssse3 clmul rdrand


If your machine does not support avx2 instructions, for example the output from a Mac with SSSE3 will show:

*machdep.cpu.features: FPU VME DE PSE TSC MSR PAE MCE CX8 APIC SEP MTRR PGE MCA CMOV PAT PSE36 CLFSH DS ACPI MMX FXSR SSE SSE2 SS HTT TM PBE SSE3 **PCLMULQDQ** DTES64 MON DSCPL VMX SMX EST TM2 **SSSE3** CX16 TPR PDCM SSE4.1 SSE4.2 x2APIC POPCNT AES PCID XSAVE OSXSAVE TSCTMR AVX1.0*

The above output indicates that this machine does not support AVX2 or RDRAND, but does support SSSE3 and PCLMULQDQ and therefore we will need to set the **SUPPORT** value in the Makefile to **ssse3 clmul**.

If there is no output from **cat** or **sysctl** for your CPU flags, we will need to set the value of **SUPPORT** on line 13 to *none* or *leave blank*.  Therefore the default build for the Makefile is a generic, non-simd build.  This will cause the make file to build only Log/Antilog and ShiftAdd methods.

6  # Set this to one of generic, ssse3, avx2, neon
7  SUPPORT = ssse3

Once the Makefile's **SUPPORT** value has been set to the correct value run make or gmake from FreeBSD. Running **make** will build the executables **gf_test** and **gf_unittest** as well as the shared object library **gferasure.so** (this is the default name.  If you wish to change it you can modify **LIBNAME** as long as the name chosen has the suffix ``.so").

 15 # This is the name of the library that's produced
 16 LIBPREF = lib
 17 LIBNAME = gferasure.so

The linker is given an absolute path value to the shared object library, so if the library is moved, you will need to modify your **LD_LIBRARY_PATH** environment variable and export it: **export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/lib** or alternatively, re-compile the code using **gcc's** **-L** and **-l** option to tell the linker which directory to look for the library and what the library's name is.

The **rdrand** flag is not a SIMD instruction, however on newer intel processors it allows hardware support for generating random numbers.

*flags   : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant\_tsc arch\_perfmon pebs bts rep\_good nopl xtopology nonstop\_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds\_cpl vmx smx est tm2 ssse3 fma cx16 xtpr pdcm pcid sse4\_1 sse4\_2 x2apic movbe popcnt tsc\_deadline\_timer aes xsave avx f16c **rdrand** lahf\_lm abm 3dnowprefetch ida arat epb pln pts dtherm hwp hwp\_noitfy hwp\_act\_window hwp\_epp intel\_pt tpr\_shadow vnmi flexpriority ept vpid fsgsbase tsc\_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm mpx rdseed adx smap clflushopt xsaveopt xsavec xgetbv1*

If **rdrand** does not show up such as if you have an Apple Macbook Pro prior to Mid, you will need to remove the **rdrand** flag from the makefile or be poised with a crypted error: "Illegal Instruction: 4"
